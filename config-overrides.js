/* eslint-disable */
const rewireLess = require('react-app-rewire-less');
const themeConfig = require('./src/configs/theme');

const {
  override,
  fixBabelImports,
  addLessLoader,
  addDecoratorsLegacy,
  useBabelRc,
} = require('customize-cra');

/* eslint-disable */
module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      '@component-background': themeConfig.background.content,
      '@primary-color': themeConfig.palette.primary,
      '@table-header-bg': themeConfig.background.headerTable,
      '@table-header-color': themeConfig.text.headerTable,
      '@text-color': themeConfig.text.text,
      '@border-color-base': themeConfig.border.default,
      '@popover-bg': themeConfig.background.container,
    },
  }),
  addDecoratorsLegacy(),
  useBabelRc()
);
