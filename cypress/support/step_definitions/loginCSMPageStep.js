import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import loginCSMPage from '../../integration/pages/loginCSMPage'


Given(`I open CSM page`, () => {
  cy.visit('/');
});

Then(`I input {string} in gmail`, gmail => {
   loginCSMPage.inputGmail(gmail)
});

Then(`I input {string} in password`, password => {
   loginCSMPage.inputPassword(password)
});

Then(`I input space in gmail`, () => {
  loginCSMPage.inputSpaceGmail()
});

Then(`I input space in password`, () => {
  loginCSMPage.inputSpacePassword()
});

Then(`I click sign in button`, () => {
  loginCSMPage.clickSubmitBtn()
});

Then(`I click register button`, () => {
  loginCSMPage.clickRegisterBtn()
});

Then(`I click forgot Password button`, () => {
  loginCSMPage.clickForgotPasswordBtn()
});

Then(`I verify login page`, () => {
  loginCSMPage.verifyLoginPage()
});

Then(`I verify null for gmail`, () => {
  loginCSMPage.verifyNullGmail()
});

Then(`I verify null for password`, () => {
  loginCSMPage.verifyNullPassword()
});

Then(`I verify gmail is not registered yet`, () => {
  loginCSMPage.verifyGmaiNotRegister()
});

Then(`I verify gmail is invalid`, () => {
  loginCSMPage.verifyGmaiInvalid()
});


