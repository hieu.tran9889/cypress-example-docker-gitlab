import { Given } from "cypress-cucumber-preprocessor/steps";
import settingCSMPage from '../../integration/pages/settingCSMPage'

Given(`I open setting page`, () => {
  cy.fixture('csmAccount.json').as('csmAccount')
  cy.get('@csmAccount').then(user => {
    cy.loginCSM(user.email, user.password);
  });
  cy.visit('/');
  // cy.eyesHieutran()
  cy.xpath('//li[@title="Settings"]').click()
});

Then(`I click packages tab`, () => {
  return settingCSMPage.clickPackagesTabBtn()
});

Then(`I click package types tab`, () => {
  return settingCSMPage.clickPackageTypesTabBtn()
});

Then(`I click transaction types tab`, () => {
  return settingCSMPage.clickTransactionTypesTabBtn()
});

Then(`I verify home page`, () => {
  settingCSMPage.verifyLogoCSM()
});

Then(`I verify setting page and rooms tab`, () => {
  settingCSMPage.verifySettingRoomTabPageCSM()
});

Then(`I verify packages tab`, () => {
  settingCSMPage.verifySettingPackagesTabPageCSM()
});

Then(`I verify package types tab`, () => {
  settingCSMPage.verifySettingPackagesTypesTabPageCSM()
});

Then(`I verify transaction types tab`, () => {
  settingCSMPage.verifySettingTransactionTypesTabPageCSM()
});

// Then(`I eyes close`, () => {
//   cy.eyesCloseHieutran();
// });

