import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import forgotPasswordCSMPage from '../../integration/pages/forgotPasswordCSMPage'

Given(`I open forgot password page`, () => {
  cy.forgotPasswordCSM();
});

Then(`I click reset password in forgot password`, () => {
  forgotPasswordCSMPage.clickResetPassword()
});

Then(`I click login button in forgot password`, () => {
  forgotPasswordCSMPage.clickLoginForgotPassword()
});

Then(`I input {string} in gmail of forgot password page`, gmail => {
  forgotPasswordCSMPage.inputGmailForgotPasswordPage(gmail)
});

Then(`I input gmail has space in gmail of forgot password page`, () => {
  forgotPasswordCSMPage.inputGmailSpaceForgotPasswordPage()
});

Then(`I verify forgot Password page`, () => {
  forgotPasswordCSMPage.verifyForgotPasswoPage();
});

Then(`I verify null gmail in forgot password`, () => {
  forgotPasswordCSMPage.verifyNullGmailForgotPasswoPage();
});

Then(`I verify gmail is invalid in forgot password`, () => {
  forgotPasswordCSMPage.verifyGmailInvalidForgotPasswoPage();
});


Then(`I verify reset password is successful`, () => {
  forgotPasswordCSMPage.verifyGmailSuccessForgotPassword();
});

Then(`I verify gmail is not found in forgot password`, () => {
  forgotPasswordCSMPage.verifyGmailIsNotFoundForgotPassword();
});




