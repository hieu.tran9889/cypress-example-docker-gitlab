import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import registerCSMPage from '../../integration/pages/registerCSMPage'

Given(`I open Register page`, () => {
  cy.registerCSM();
});

Then(`I click submit button in register page`, () => {
  registerCSMPage.clickSubmitBtnRegisterPage()
});

Then(`I click login button in register page`, () => {
  registerCSMPage.clickLoginBtnRegisterPage()
});

Then(`I input {string} in username of register page`, username => {
  registerCSMPage.inputUsernameRegisterPage(username)
});

Then(`I input space in username`, () => {
  registerCSMPage.inputSpaceUsernameRegisterPage()
});

Then(`I input {string} in gmail of register page`, gmail => {
  registerCSMPage.inputGmailRegisterPage(gmail)
});

Then(`I input {string} in random username of register page`, randomUsername => {
  registerCSMPage.inputRandomUsernamelRegisterPage(randomUsername)
});

Then(`I input {string} in random gmail of register page`, randomGmail => {
  registerCSMPage.inputRandomGmailRegisterPage(randomGmail)
});

Then(`I input {string} in password of register page`, password => {
  registerCSMPage.inputPasswordlRegisterPage(password)
});

Then(`I input {string} in confirmPassword of register page`, confirmPassword => {
  registerCSMPage.inputConfirmPasswordRegisterPage(confirmPassword)
});

Then(`I verify register page`, () => {
  registerCSMPage.verifyRegisterPage()
});

Then(`I verify null for username in register page`, () => {
  registerCSMPage.verifyNullUsernameRegisterPage()
});

Then(`I verify null for gmail in register page`, () => {
  registerCSMPage.verifyNullGmailRegisterPage()
});

Then(`I verify null for password in register page`, () => {
  registerCSMPage.verifyNullPasswordRegisterPage()
});

Then(`I verify null for confirm password in register page`, () => {
  registerCSMPage.verifyNullConfirmPasswordRegisterPage()
});


Then(`I verify username is invalid`, () => {
  registerCSMPage.verifyUsernameIsInValid()
});

Then(`I verify gmail is invalid`, () => {
  registerCSMPage.verifyGmailIsInValid()
});

Then(`I verify gmail is existed`, () => {
  registerCSMPage.verifyGmailIsExisted()
});

Then(`I verify confirm password don't match`, () => {
  registerCSMPage.verifyPasswordDontMatch()
});

Then(`I verify username is existed in register page`, () => {
  registerCSMPage.verifyUsernameIsExisted()
});

Then(`I verify password is invalid`, () => {
  registerCSMPage.verifyPasswordIsInValid()
});



