
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import 'cypress-file-upload';

// snapshot: take a picture page
const compareSnapshotCommand = require('cypress-visual-regression/dist/command');
compareSnapshotCommand();

Cypress.Commands.add('forgotPasswordCSM', () => {
  cy.visit('/')
  cy.xpath('//a[@href="/forgot-password"]').click()
})

Cypress.Commands.add('registerCSM', () => {
  cy.visit('/')
  cy.xpath('//a[contains("Register", text())]').click()
})

Cypress.Commands.add('loginCSM', (email, password) => {
  cy.request({
    method: 'POST',
    url: 'https://csm-api-staging.enouvo.com/api/v1/auth/login',
    body: {
      email: email,
      password: password
    }
  })
    .then((resp) => {
      window.localStorage.setItem('sessionToken', resp.body.token)
      return resp.body.token
    })

})

// login is successful
//  Cypress.Commands.add('login', () => {
//     cy.visit('/')
//     cy.xpath('//input[@id="username"]').type('enouvospace@enouvo.com{enter}')
//     cy.xpath('//input[@id="password"]').type('admin123{enter}')
//     cy.get('.ant-btn').click()
//  })

Cypress.Commands.add('eyesHieutran', () => {
  cy.eyesOpen({
    appName: 'Hello Cypress, This is Applitools!',
    testName: 'My first Cypress Test',
    browser: [
      { width: 800, height: 600, name: 'firefox' },
      { width: 1024, height: 768, name: 'chrome' },
      {
        deviceName: 'iPhone X',
        screenOrientation: 'landscape'
      }
      //Add more variations
    ],
  });
  cy.eyesCheckWindow('Home Page');
})


Cypress.Commands.add('eyesCloseHieutran', () => {
  cy.eyesClose();
})



// Cypress.Commands.add('loginLambdaTest', (username, password) => {
//   cy.contains("Login").click();
//   cy.get("#loginLink").invoke('text').as("linkText");
//   cy.get("@linkText").then( ($x) => {
//     expect($x).is.eql('Login');
//   });

// })