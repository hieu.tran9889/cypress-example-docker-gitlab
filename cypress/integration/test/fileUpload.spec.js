describe('Testing upload file', () => {
    it('Testing upload file', () => {
        cy.visit('https://fineuploader.com/demos.html');
        // upload
        cy.fixture('1.jpg').then(fileContent => {
            cy.xpath("(//input[@title='file input'])[1]").upload(
                {
                    fileContent,
                    fileName: '1.jpg',
                    mimeType: 'image/jpg'
                },
                { 
                    uploadType: 'input' 
                },
            );
        });

    });

});