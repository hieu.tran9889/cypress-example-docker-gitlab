describe('Testing LambdaTest website xhr', () => {
    beforeEach("Navigatie to website", () => {
        cy.visit('https://accounts.lambdatest.com/login');
    });
    it('Testing login and verify XHR', () => {
        //start the server
        cy.server();

        cy.route({
            method:'GET',
            url: '/api/user/organization/team'
        }).as('team');

        cy.route({
            method:'GET',
            url: '/api/user/organization/automation-test-summary'
        }).as('apiCheck');
        // loggin account
        cy.get("@team").then((xhr) => {
            expect(xhr.status).to.eq(200);
            expect(xhr.response.body.data[0]).to.have.property("name","hieu");
            expect(xhr.response.body.data[0]).to.have.property("email","hieutrandn123@gmail.com");
        });

    });
;
});