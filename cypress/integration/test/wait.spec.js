describe('Testing EA site for assertion', () => {
    beforeEach("Navigatie to website", () => {
        cy.visit('http://www.executeautomation.com/site/');
    });
    it('Testing implicit wait', () => {
        // implicit wait tat ca phan tu
        cy.get("[aria-label='jump to slide 2']", {timeout:60000}).should('have.class', 'ls-nav-active');

    });

    it('Testing Explicit wait', () => {
        // Explicit wait dung phan tu
        cy.get("[aria-label='jump to slide 2']", {timeout:60000}).should(($x) => {
            expect($x).to.have.class("ls-nav-active");
        });
    });
});