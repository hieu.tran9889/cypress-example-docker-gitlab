
export default class loginCSMPageUI {
    
    static inputGmail                = '//input[@id="email"]';
    static inputPassword             = '//input[@id="password"]';
    static submitBtn                 = '//button[@type="submit"]';
    static registerBtn               = '//a[contains("Register", text())]';
    static verifyLoginPage           = '//span[contains("Login", text())]';
    static forgotPasswordBtn         = '//a[@href="/forgot-password"]'
    static verifyNullGmail           = '//div[@class="ant-form-explain" and contains("Please input your email!", text())]';
    static verifyNullPassword        = '//div[@class="ant-form-explain" and contains("Please input your password!", text())]';
    static verifyGmaiInvalid         = '//div[@class="ant-form-explain" and contains("Email is invalid ", text())]';
    static verifyGmaiNotRegister     = '//div[@class="ant-notification-notice-description" and contains("Email or password is not correct", text())]';
   
    
    
  };


 