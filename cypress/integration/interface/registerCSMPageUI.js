
export default class registerCSMPageUI {
    
    static inputEmailRegister                       = '//input[@id="email"]';
    static inputPasswordRegister                    = '//input[@id="password"]';
    static inputConfirmPasswordRegister             = '//input[@id="comfirm"]';
    static inputUsernameRegister                    = '//input[@id="username"]';
    static submitBtnRegister                        = '//button[@type="button"]';
    static submitBtnLoginInRegister                 = '//a[contains("Log in", text())]';
    static verifyNullUsernameRegisterPage           = '//div[@class="ant-form-explain" and contains("Please input your username!", text())]';
    static verifyNullGmailRegisterPage              = '//div[@class="ant-form-explain" and contains("Please input your email!", text())]';
    static verifyNullPasswordRegisterPage           = '//div[@class="ant-form-explain" and contains("Please input your password!", text())]';
    static verifyNullConfirmPasswordRegisterPage    = '//div[@class="ant-form-explain" and contains("Please input your confirm password!", text())]';
    static verifyNotMatchConfirmPasswordRegister    = '//div[@class="ant-form-explain" and contains("Two passwords that you enter is inconsistent!", text())]';
    static verifyTitleRegister                      = '//div[@class="title" and contains("Register", text())]';
    static verifyUsernameIsInValid                  = '//div[@class="ant-form-explain" and contains("Username must be 3-100 characters", text())]';
    static verifConfirmPasswordDontMatch            = '//div[@class="ant-form-explain" and contains("Those passwords didn\'t match", text())]'
    static verifyGmailIsExisted                     = '//div[@class="ant-notification-notice-description" and contains("Email is exist", text())]';
    static verifyGmailIsInValid                     = '//div[@class="ant-form-explain" and contains("Email is invalid", text())]';
    static verifyUsernameIsExisted                  = '//div[@class="ant-notification-notice-description" and contains("Username is exist", text())]';
    static verifyPasswordIsInValid                  = '//div[@class="ant-form-explain" and contains("Password must be at least 6 characters (0-9 a-z)", text())]';


  };


 