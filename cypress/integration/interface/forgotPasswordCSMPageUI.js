
export default class forgotPasswordCSMPageUI {
    
    static inputGmailForgotPassword            = '//input[@id="email"]';
    static resetPasswordBtn                    = '//button[@type="submit"]';
    static loginForgotPasswordBtn              = '//a[@href="/login"]';
    static verifyTitleForgotPassword           = '//div[@class="h3 regular center  " and contains("Forgot password", text())]';
    static verifyNullGmailForgotPassword       = '//div[@class="ant-form-explain" and contains("Please input your email!", text())]';
    static verifyGmailInvalidForgotPassword    = '//div[@class="ant-form-explain" and contains("Email is invalid", text())]';
    static verifyGmailSuccessForgotPassword    = '//div[@class="h3 regular center  " and contains("Reset Password Success!", text())]';
    static verifyGmailIsNotFoundForgotPassword = '//div[@class="ant-notification-notice-description" and contains("Email is not found", text())]';


  };


 