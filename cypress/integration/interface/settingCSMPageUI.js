
export default class settingCSMPageUI {

  static adminBtn                           = '//div[contains(text(),"Admin")]';
  static logoCSM                            = '//div[@class="logo"]';
  static settingBtn                         = '//li[@title="Settings"]';
  static clickRoomTabInSetting              = '//div[@class=" ant-tabs-tab" and contains("Rooms", text())]';
  static clickPackagesTabInSetting          = '//div[@class=" ant-tabs-tab" and contains("Packages", text())]';
  static clickPackagesTypesTabInSetting     = '//div[@class=" ant-tabs-tab" and contains("Package Types", text())]';
  static clickTransactionTypesTabInSetting  = '//div[@class=" ant-tabs-tab" and contains("Transaction Types", text())]';
  static verifyRoomTabInSetting             = '//div[@class="ant-tabs-tab-active ant-tabs-tab" and contains("Rooms", text())]'
  static verifyPackagesTabInSetting         = '//div[@class="ant-tabs-tab-active ant-tabs-tab" and contains("Packages", text())]'
  static verifyPackagesTypesTabInSetting    = '//div[@class="ant-tabs-tab-active ant-tabs-tab" and contains("Package Types", text())]'
  static verifyTransactionTypesTabInSetting = '//div[@class="ant-tabs-tab-active ant-tabs-tab" and contains("Transaction Types", text())]'



};


