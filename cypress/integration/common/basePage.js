

export default class basePage  {
  constructor() {
    this.loginPageUI = new loginPageUI();

  }

  // verifyElements() {
  //   return cy.get(this.mainPageUI.bodyUI).should('be.visible').then(() => {
  //     return this.navMenu.verifyElements();
  //   });
  // }

  static clickElements(elementFinder){
    cy.xpath(elementFinder).click()  
  }

  static typeElements(elementFinder, value){
    // cy.xpath(elementFinder).type(value +'{enter}')
    cy.xpath(elementFinder).type(value)
  }

  static typeSpaceElements(elementFinder){
    cy.xpath(elementFinder).type("  ")
  }

  static verifyElements(elementFinder) {
    return cy.xpath(elementFinder).should('be.visible');
  }

  static verifyElementsLenght(elementFinder, value) {
    return cy.xpath(elementFinder).should('have.lenght', value);
  }

  static typeRandomGmailElements(elementFinder, value){
    cy.xpath(elementFinder).type(value + this.randomStringNumber(2) + '@gmail.com')

  }

  static typeRandomUsernameElements(elementFinder, value){
    cy.xpath(elementFinder).type(value + this.randomStringNumber(2))

  }

  static randomStringNumber(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

};
