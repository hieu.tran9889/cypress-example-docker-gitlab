import registerCSMPageUI from '../interface/registerCSMPageUI'
import basePage from '../common/basePage';

export default class registerCSMPage extends basePage {
  
  static clickSubmitBtnRegisterPage() {
    this.clickElements(registerCSMPageUI.submitBtnRegister)
  }

  static clickLoginBtnRegisterPage() {
    this.clickElements(registerCSMPageUI.submitBtnLoginInRegister)
  }

  static inputUsernameRegisterPage(username) {
    this.typeElements(registerCSMPageUI.inputUsernameRegister, username)
  }

  static inputSpaceUsernameRegisterPage() {
    this.typeSpaceElements(registerCSMPageUI.inputUsernameRegister)
  }
  
  static inputGmailRegisterPage(gmail) {
    this.typeElements(registerCSMPageUI.inputEmailRegister, gmail)
  }

  static inputRandomGmailRegisterPage(randomGmail) {
    this.typeRandomGmailElements(registerCSMPageUI.inputEmailRegister, randomGmail)
  }

  static inputRandomUsernamelRegisterPage(randomUsername) {
    this.typeRandomUsernameElements(registerCSMPageUI.inputUsernameRegister, randomUsername)
  }


  static inputPasswordlRegisterPage(password) {
    this.typeElements(registerCSMPageUI.inputPasswordRegister, password)
  }

  static inputConfirmPasswordRegisterPage(confirmPassword) {
    this.typeElements(registerCSMPageUI.inputConfirmPasswordRegister, confirmPassword)
  }

  static verifyRegisterPage() {
    this.verifyElements(registerCSMPageUI.verifyTitleRegister)
  }

  static verifyNullUsernameRegisterPage() {
    this.verifyElements(registerCSMPageUI.verifyNullUsernameRegisterPage)
  }

  static verifyNullGmailRegisterPage() {
    this.verifyElements(registerCSMPageUI.verifyNullGmailRegisterPage)
  }

  static verifyNullPasswordRegisterPage() {
    this.verifyElements(registerCSMPageUI.verifyNullPasswordRegisterPage)
  }

  static verifyNullConfirmPasswordRegisterPage() {
    this.verifyElements(registerCSMPageUI.verifyNullConfirmPasswordRegisterPage)
  }
  
  static verifyUsernameIsInValid() {
    this.verifyElements(registerCSMPageUI.verifyUsernameIsInValid)
  }

  static verifyGmailIsInValid() {
    this.verifyElements(registerCSMPageUI.verifyGmailIsInValid)
  }

  static verifyGmailIsExisted() {
    this.verifyElements(registerCSMPageUI.verifyGmailIsExisted)
  }
  
  static verifyPasswordDontMatch() {
    this.verifyElements(registerCSMPageUI.verifConfirmPasswordDontMatch)
  }

  static verifyUsernameIsExisted() {
    this.verifyElements(registerCSMPageUI.verifyUsernameIsExisted)
  }

  static verifyPasswordIsInValid() {
    this.verifyElements(registerCSMPageUI.verifyPasswordIsInValid)
  }
  

  
};


