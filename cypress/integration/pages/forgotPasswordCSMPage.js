import forgotPasswordCSMPageUI from '../interface/forgotPasswordCSMPageUI'
import basePage from '../common/basePage';

export default class forgotPasswordCSMPage extends basePage {

  static clickResetPassword() {
    this.clickElements(forgotPasswordCSMPageUI.resetPasswordBtn)
  }
  
  static clickLoginForgotPassword() {
    this.clickElements(forgotPasswordCSMPageUI.loginForgotPasswordBtn)
  }

  static inputGmailForgotPasswordPage(gmail) {
    this.typeElements(forgotPasswordCSMPageUI.inputGmailForgotPassword, gmail)
  }

  static inputGmailSpaceForgotPasswordPage() {
    this.typeSpaceElements(forgotPasswordCSMPageUI.inputGmailForgotPassword)
  }

  static verifyForgotPasswoPage() {
    this.verifyElements(forgotPasswordCSMPageUI.verifyTitleForgotPassword)
  }

  static verifyNullGmailForgotPasswoPage() {
    this.verifyElements(forgotPasswordCSMPageUI.verifyNullGmailForgotPassword)
  }

  static verifyGmailInvalidForgotPasswoPage() {
    this.verifyElements(forgotPasswordCSMPageUI.verifyGmailInvalidForgotPassword)
  }

  static verifyGmailSuccessForgotPassword() {
    this.verifyElements(forgotPasswordCSMPageUI.verifyGmailSuccessForgotPassword)
  }
  
  static verifyGmailIsNotFoundForgotPassword() {
    this.verifyElements(forgotPasswordCSMPageUI.verifyGmailIsNotFoundForgotPassword)
  }


  
  

  

  

};


