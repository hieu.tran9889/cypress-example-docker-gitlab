import loginCSMPageUI from '../interface/loginCSMPageUI'
import basePage from '../common/basePage';

export default class loginCSMPage extends basePage {

  static inputGmail(gmail) {
    this.typeElements(loginCSMPageUI.inputGmail, gmail)
  }
  static inputPassword(password) {
    this.typeElements(loginCSMPageUI.inputPassword, password)
  }

  static inputSpaceGmail() {
    this.typeSpaceElements(loginCSMPageUI.inputGmail)
  }

  static inputSpacePassword() {
    this.typeSpaceElements(loginCSMPageUI.inputPassword)
  }

  static clickSubmitBtn() {
    this.clickElements(loginCSMPageUI.submitBtn)
  }

  static clickRegisterBtn() {
    this.clickElements(loginCSMPageUI.registerBtn)
  }

  static clickForgotPasswordBtn() {
    this.clickElements(loginCSMPageUI.forgotPasswordBtn)
  }

  static verifyNullGmail() {
    this.verifyElements(loginCSMPageUI.verifyNullGmail)
  }

  static verifyNullPassword() {
    this.verifyElements(loginCSMPageUI.verifyNullPassword)
  }

  static verifySpacePassword() {
    this.verifyElements(loginCSMPageUI.verifySpaceGmail)
  }

  static verifyGmaiNotRegister() {
    this.verifyElements(loginCSMPageUI.verifyGmaiNotRegister)
  }

  static verifyGmaiInvalid() {
    this.verifyElements(loginCSMPageUI.verifyGmaiInvalid)
  }

  static verifyLoginPage() {
    this.verifyElements(loginCSMPageUI.verifyLoginPage)
  }
  

  
};


