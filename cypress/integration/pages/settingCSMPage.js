import settingCSMPageUI from '../interface/settingCSMPageUI'
import basePage from '../common/basePage';

export default class settingCSMPage extends basePage {

  static clickPackagesTabBtn() {
    this.clickElements(settingCSMPageUI.clickPackagesTabInSetting)
  }

  static clickPackageTypesTabBtn() {
    this.clickElements(settingCSMPageUI.clickPackagesTypesTabInSetting)
  }

  static clickTransactionTypesTabBtn() {
    this.clickElements(settingCSMPageUI.clickTransactionTypesTabInSetting)
  }

  static verifyLogoCSM() {
    this.verifyElements(settingCSMPageUI.logoCSM)
  }

  static verifySettingRoomTabPageCSM() {
    this.verifyElements(settingCSMPageUI.verifyRoomTabInSetting)
  }

  static verifySettingPackagesTabPageCSM() {
    this.verifyElements(settingCSMPageUI.verifyPackagesTabInSetting)
  }

  static verifySettingPackagesTypesTabPageCSM() {
    this.verifyElements(settingCSMPageUI.verifyPackagesTypesTabInSetting)
  }

  static verifySettingTransactionTypesTabPageCSM() {
    this.verifyElements(settingCSMPageUI.verifyTransactionTypesTabInSetting)
  }

  


};


