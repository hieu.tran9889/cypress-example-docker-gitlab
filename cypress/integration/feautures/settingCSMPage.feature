@feature-tag
Feature: The CSM setting page

  I want to open CSM setting page

  Background:
    Given I open setting page

  Scenario: Verify a CSM setting page
    Then I verify home page
    Then I verify setting page and rooms tab
  # Then I eyes close

  Scenario:  Verify clicking on "Rooms" tab ==> Display "Rooms" tab
    Then I verify home page
    Then I verify setting page and rooms tab

  Scenario:  Verify clicking on "Packages" tab ==> Display "Packages" tab
    Then I click packages tab
    Then I verify packages tab

  Scenario:  Verify clicking on "Package Types" tab ==> Display "Package Types" tab
    Then I click package types tab
    Then I verify package types tab

  Scenario:  Verify clicking on "Transaction Types" tab ==> Display "Transaction Types" tab
    Then I click transaction types tab
    Then I verify transaction types tab