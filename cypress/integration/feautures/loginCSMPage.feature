Feature: The CSM login page

  I want to open CSM login page

  Background:
    Given I open CSM page

  Scenario: Verify all fields are null/blank
    Then I verify login page
    Then I click sign in button
    Then I verify null for gmail
    Then I verify null for password

  Scenario Outline: Verify inputting "Email" field without inputting "Password"
    Then I input "<gmail>" in gmail
    Then I click sign in button
    Then I verify null for password
    Examples:
      | gmail                  |
      | enouvospace@enouvo.com |

  Scenario Outline: Verify inputting "Password" field without inputting "Email"
    Then I input "<password>" in password
    Then I click sign in button
    Then I verify null for gmail
    Examples:
      | password |
      | admin123 |

  Scenario Outline: Verify when inputing Email and Password are not registered yet
    Then I input "<gmail>" in gmail
    Then I input "<password>" in password
    Then I click sign in button
    Then I verify gmail is not registered yet
    Examples:
      | gmail         | password |
      | abc@gmail.com | admin123 |

  Scenario Outline: Verify when inputing Email and Password value in database
    Then I input "<gmail>" in gmail
    Then I input "<password>" in password
    Then I click sign in button
    Then I verify home page
    Examples:
      | gmail                  | password |
      | enouvospace@enouvo.com | admin123 |

  Scenario: Verify inputting space begin and end into "Email" and "Password" field
    Then I input space in gmail
    Then I input space in password
    Then I click sign in button
    Then I verify gmail is invalid

  Scenario Outline: Verify inputting value Email is invalid (wrong format: string@string.string)
    Then I input "<gmail>" in gmail
    Then I input "<password>" in password
    Then I click sign in button
    Then I verify gmail is invalid
    Examples:
      | gmail    | password |
      | 123@.com | 123456   |

  Scenario Outline: Verify inputting value Email is valid (right format: string@string.string)
    Then I input "<gmail>" in gmail
    Then I input "<password>" in password
    Then I click sign in button
    Then I verify gmail is not registered yet
    Examples:
      | gmail       | password |
      | 123@123.com | 123456   |

  Scenario: Verify clicking on "Register" button
    Then I click register button
    Then I verify register page

  Scenario: Verify clicking on "Forgot Password" link
    Then I click forgot Password button
    Then I verify forgot Password page