Feature: The CSM register page

  I want to open CSM register page

  Background:
    Given I open Register page

  Scenario: Verify all fields are null/blank
    Then I click submit button in register page
    Then I verify register page
    Then I verify null for username in register page
    Then I verify null for gmail in register page
    Then I verify null for password in register page
    Then I verify null for confirm password in register page

  Scenario: Verify clicking on "Login" button
    Then I click login button in register page
    Then I verify login page

  Scenario Outline: Verify inputting valid data into Username field
    Then I input "<username>" in random username of register page
    Examples:
      | username |
      | 123      |

  Scenario Outline: Verify inputting a exist "Username"
    Then I input "<username>" in username of register page
    Then I input "<gmail>" in gmail of register page
    Then I input "<password>" in password of register page
    Then I input "<confirmPassword>" in confirmPassword of register page
    Then I click submit button in register page
    Then I verify username is existed in register page
    Examples:
      | username | gmail                   | password | confirmPassword |
      | 123      | enouvospace2@enouvo.com | Abc123   | Abc123          |


  Scenario Outline: Verify inputting uppercase letters into Username
    Then I input "<username>" in username of register page
    Examples:
      | username |
      | ABC      |

  Scenario Outline: Verify username inputting lowercase letters into Username
    Then I input "<username>" in username of register page
    Examples:
      | username |
      | abc      |

  Scenario Outline: Verify inputting min length letters into Username
    Then I input "<username>" in username of register page
    Then I verify username is invalid
    Examples:
      | username |
      | ab       |

  Scenario Outline: Verify inputting max length letters into Username
    Then I input "<username>" in username of register page
    Then I verify username is invalid
    Examples:
      | username                                                                                              |
      | 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 |

  Scenario Outline: Verify inputting special letters into Username
    Then I input "<username>" in username of register page
    Then I verify username is invalid
    Examples:
      | username |
      | @%#      |

  Scenario: Verify inputting space into Username
    Then I input space in username
    Then I verify username is invalid

  Scenario Outline: Verify inputting uppercase letters into gmail
    Then I input "<gmail>" in gmail of register page
    Examples:
      | gmail         |
      | ABC@gmail.com |

  Scenario Outline: Verify inputting lowercase letters into gmail
    Then I input "<gmail>" in gmail of register page
    Examples:
      | gmail         |
      | abc@gmail.com |

  Scenario Outline: Verify inputting min length letters into gmail
    Then I input "<gmail>" in gmail of register page
    Examples:
      | gmail       |
      | 1@gmail.com |

  Scenario Outline: Verify inputting max length letters into gmail
    Then I input "<gmail>" in gmail of register page
    Examples:
      | gmail                                                                                                           |
      | 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@gmail.com |

  Scenario Outline: Verify inputting special letters into gmail
    Then I input "<gmail>" in gmail of register page
    Examples:
      | gmail        |
      | %#@gmail.com |

  Scenario: Verify inputting space into gmail
    Then I input space in gmail
    Then I verify gmail is invalid

  Scenario Outline: Verify inputting invalid data into "Email" field (wrong format: string@string)
    Then I input "<gmail>" in gmail of register page
    Then I verify gmail is invalid
    Examples:
      | gmail      |
      | @gmail.com |

  Scenario Outline: Verify inputting valid data into "Email" field (right format: string@string.string)
    Then I input "<gmail>" in gmail of register page
    Examples:
      | gmail              |
      | hieutran@gmail.com |

  Scenario Outline: Verify inputting a exist "Email"
    Then I input "<username>" in username of register page
    Then I input "<gmail>" in gmail of register page
    Then I input "<password>" in password of register page
    Then I input "<confirmPassword>" in confirmPassword of register page
    Then I click submit button in register page
    Then I verify gmail is existed
    Examples:
      | username | gmail                  | password | confirmPassword |
      | 123      | enouvospace@enouvo.com | Abc123   | Abc123          |


  Scenario Outline: Verify inputting invalid data into "Password field (< 6 characters, Only number or alphabet character)
    Then I input "<password>" in password of register page
    Then I verify password is invalid
    Examples:
      | password |
      | Abc      |

  Scenario Outline: Verify inputting invalid data into "Password field (< 6 characters, Only number or alphabet character)
    Then I input "<password>" in password of register page
    Then I verify password is invalid
    Examples:
      | password |
      | 123456   |

  Scenario Outline: Verify inputting invalid data into "Password field (< 6 characters, Only number or alphabet character)
    Then I input "<password>" in password of register page
    Then I verify password is invalid
    Examples:
      | password |
      | abcHello |

  Scenario Outline: Verify inputting valid data into "Password field ( at least 6 characters contains: number and alphabet character)
    Then I input "<password>" in password of register page
    Examples:
      | password |
      | abc123   |

  Scenario Outline: Verify inputting Confirm Password match with Password
    Then I input "<password>" in password of register page
    Then I input "<confirmPassword>" in confirmPassword of register page
    Examples:
      | password | confirmPassword |
      | Abc123   | Abc123          |

  Scenario Outline: Verify inputting Confirm Password not match with Password
    Then I input "<username>" in username of register page
    Then I input "<gmail>" in gmail of register page
    Then I input "<password>" in password of register page
    Then I input "<confirmPassword>" in confirmPassword of register page
    Then I click submit button in register page
    Then I verify confirm password don't match
    Examples:
      | username | gmail                  | password | confirmPassword |
      | 123      | enouvospace@enouvo.com | Abc123   | Abc1231         |


  Scenario Outline: Verify click on "Submit" button
    Then I input "<username>" in random username of register page
    Then I input "<randomGmail>" in random gmail of register page
    Then I input "<password>" in password of register page
    Then I input "<confirmPassword>" in confirmPassword of register page
    Then I click submit button in register page
    Then I verify login page
    Examples:
      | username | randomGmail | password | confirmPassword |
      | 123      | enouvospace | Abc123   | Abc123          |

  Scenario Outline: Verify inputting a exist "Email"
    Then I input "<username>" in random username of register page
    Then I input "<gmail>" in gmail of register page
    Then I input "<password>" in password of register page
    Then I input "<confirmPassword>" in confirmPassword of register page
    Then I click submit button in register page
    Then I verify gmail is existed
    Examples:
      | username | gmail                  | password | confirmPassword |
      | 123      | ENOUVOSPACE@enouvo.com | Abc123   | Abc123          |