Feature: The CSM forgot password page

  I want to open CSM forgot password page

  Background:
    Given I open forgot password page

  Scenario: Verify all fields are null/blank
    Then I click reset password in forgot password
    Then I verify null gmail in forgot password
    Then I verify forgot Password page

  Scenario Outline: Verify inputting invalid data into Email (wrong format: string@string)
    Then I input "<gmail>" in gmail of forgot password page
    Then I verify gmail is invalid in forgot password
    Examples:
      | gmail |
      | 1234   |

  Scenario: Verify inputting invalid data into Email (wrong format: string@string, space)
    Then I input gmail has space in gmail of forgot password page
    Then I verify gmail is invalid in forgot password

  Scenario Outline: Verify inputting valid data into "Email" field (right format: string@string.string)
    Then I input "<gmail>" in gmail of forgot password page
    Examples:
      | gmail         |
      | 123@gmail.com |

  Scenario Outline: Verify inputting Email in database and Verify clicking "Reset Password" button
    Then I input "<gmail>" in gmail of forgot password page
    Then I click reset password in forgot password
    Then I verify reset password is successful
    Examples:
      | gmail                  |
      | enouvospace@enouvo.com |

  Scenario Outline: Verify inputting Email in database and Verify clicking "Reset Password" button then clicking login button
    Then I input "<gmail>" in gmail of forgot password page
    Then I click reset password in forgot password
    Then I verify reset password is successful
    Then I click login button in forgot password
    Then I verify login page
    Examples:
      | gmail                  |
      | enouvospace@enouvo.com |

  Scenario Outline: Verify inputting Email are not registered yet
    Then I input "<gmail>" in gmail of forgot password page
    Then I click reset password in forgot password
    Then I verify gmail is not found in forgot password
    Examples:
      | gmail          |
      | 4343@gmail.com |

  Scenario: Verify clicking on "Log In" button
    Then I click login button in forgot password
    Then I verify login page

