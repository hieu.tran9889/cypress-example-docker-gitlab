import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
.ant-carousel .slick-dots li {
  button {
    background: ${({ theme }) => theme.palette.primary} !important;
  }
  &.slick-active button {
  background: ${({ theme }) => theme.palette.primary} !important;
}
}
`;

const AppWrapper = styled.div`
  .anticon:before {
    display: block;
    font-family: 'anticon', 'csm-web' !important;
  }
  .anticon:after {
    display: block;
    font-family: 'anticon', 'csm-web' !important;
  }
  .gradientBackground {
    background-image: ${({ theme }) =>
      `linear-gradient(90deg, ${theme.palette.lightPrimary}, ${theme.palette.primary})`};
  }
  .ant-tabs {
    color: ${({ theme }) => theme.text.text};
  }
  .ant-tabs-bar {
    border-bottom-color: ${({ theme }) => theme.border.default};
  }
  .ant-table-thead > tr > th {
    border-bottom-color: ${({ theme }) => theme.border.default};
  }
  .ant-table-fixed-left table,
  .ant-table-fixed-right table {
  }
  .ant-table-tbody > tr > td {
    border-bottom-color: ${({ theme }) => theme.border.default};
  }
`;

export default AppWrapper;
