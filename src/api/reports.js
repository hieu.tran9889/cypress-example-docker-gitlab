import { get } from './utils';

export async function getPaymentReports(data) {
  return get('/transactions/summary', data);
}

export async function getCustomerReports() {
  return get('/customers/summary');
}

export async function getCustomerSummaries(userId) {
  return get(`/customers/${userId}/summary`);
}