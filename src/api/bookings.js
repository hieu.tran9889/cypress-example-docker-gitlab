import { get, post, put } from './utils';

export async function getBookings() {
  return get('/bookings/working');
}

export async function getBooking(id) {
  return get(`/bookings/${id}`);
}

export async function createBooking(data) {
  return post('/bookings', data);
}

export async function editBooking(id, data) {
  return put(`/bookings/${id}`, data);
}

export async function checkPricing(data) {
  return get('/calculateBookingPrice', data);
}

export async function checkinBooking(id, payload) {
  return post(`/bookings/${id}/checkin`, payload);
}

export async function checkoutBooking(id, payload) {
  return post(`/bookings/${id}/checkout`, payload);
}

export async function paymentBooking(id, data) {
  return post(`/bookings/${id}/paid`, data);
}

export async function payBooking(id, data) {
  return post(`/bookings/${id}/paid`, data);
}

export async function updateBookingStatus(id, status) {
  return put(`/bookings/${id}/${status.toLowerCase()}`);
}

export async function addServiceForBooking(data) {
  return post('/services', data);
}

export async function addPaymentForBooking(data) {
  return post('/transactions', data);
}

export async function checkServicePricing(data) {
  return get(`/calculateServicePrice?services=${encodeURIComponent(JSON.stringify(data))}`);
}
