import openSocket from 'socket.io-client';

let socket = null;
export const initSocket = cbFunc => {
  socket = openSocket(`${process.env.REACT_APP_SERVER_URL}/dashboard`, {
    transports: ['websocket'],
  });
  socket.on('connected', data => cbFunc(data));
  socket.on('changed', data => {
    cbFunc(data);
  });
  return socket;
};

export const disconnectSocket = () => {
  socket && socket.disconnect();
};
