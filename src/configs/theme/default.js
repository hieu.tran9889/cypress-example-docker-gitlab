const theme = {
  palette: {
    primary: '#5d4ec2',
    lightPrimary: '#4cb1e8',
    secondary: '#52cfa4',
    loadingBackgroundColor: '#2c3e51cc',
    color: ['#788195', '#E4E6E9'],
  },
  fonts: {
    primary: 'Georgia, serif',
  },
  fontWeight: {
    thin: 100, // Thin
    utraLight: 200, // Ultra Light
    light: 300, // Light
    regular: 400, // Regular
    medium: 500, // Medium
    semibold: 600, // Semibold
    bold: 700, // Bold
    heavy: 800, // Heavy
    black: 900, // Black
  },
  background: {
    container: '#f5f7fa',
    content: '#fff',
    input: '#efeff0',
    disabled: '#969696',
    headerTable: '#d4d2f450',
  },
  text: {
    primary: '#1f2933',
    text: '#1f2933',
    lightPrimary: '#3e4c59',
    secondary: '#7b8794',
    tabTitle: '#262626',
    empty: '#969696',
    highlight: '#5d4ec2',
    disabled: '#969696',
    formLabel: '#4a4a4a',
    headerTable: '#1f2933',
  },
  border: {
    default: '#d4d2f450',
    light: '#e4e7eb',
  },
  scrollbar: {
    thumb: '#b7b6c2',
    track: '#f0f3fa',
  },
  color: {
    gray: '#a3a3a3',
    green: '#4fcea2',
    red: '#e64c38',
    blue: '#0992d0',
    orange: '#f5a623',
    pink: '#F75D81',
    limeGreen: '#4fcea2',
    lightGreen: '#3ebac2',
    blueShade: '#2d7fd3',
    yellow: '#FFCA28',
    violet: '#665ca7',
  },
  alert: {
    error: 'red',
  },
  card: {
    header: '#d4d2f450',
  },
};
module.exports = theme;
