import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import I18n from 'i18next';
import { push } from 'connected-react-router';
import { Layout, Menu, Icon } from 'antd';
import { Redirect } from 'react-router-dom';
import { history } from '../../redux/store';
import PrivateLayoutWrapper from './styles';
import { logout as logoutAction } from '../../redux/auth/actions';
import Header from '../../containers/Header';
import Logo from '../../assets/images/logo.svg';
import FullLogo from '../../assets/images/fullLogo.svg';
import { PUBLIC_ROUTES } from '../../routes/PublicRoutes/index';
import SVGIcon from '../../components/common/SVGIcon';

const getCurrentTab = str => {
  const paths = str && str.split('/');
  return paths && paths[1];
};
const { Sider, Content, Footer } = Layout;
const sidebarMenu = [
  {
    key: 'dashboard',
    text: 'sideBar.dashboard',
    icon: 'ic-home',
    url: '/',
  },
  {
    key: 'reservations',
    text: 'sideBar.reservations',
    icon: 'ic-date',
    url: '/reservations',
  },
  {
    key: 'customers',
    text: 'sideBar.customers',
    icon: 'team',
    url: '/customers',
  },
  {
    key: 'bookings',
    text: 'sideBar.bookings',
    icon: 'ic-seat',
    url: '/bookings',
  },
  {
    key: 'checkins',
    text: 'sideBar.checkins',
    icon: 'check',
    url: '/checkins',
  },
  {
    key: 'transactions',
    text: 'sideBar.transactions',
    icon: 'ic-payment',
    url: '/transactions',
  },
  {
    key: 'users',
    text: 'sideBar.users',
    icon: 'user',
    url: '/users',
  },
  {
    key: 'settings',
    text: 'sideBar.settings',
    icon: 'ic-setting',
    url: '/settings/rooms',
  },
];

const mobileTabs = [
  {
    key: 'home',
    text: 'Profile',
    url: '/',
    icon: 'home',
  },
  {
    key: 'user',
    text: 'Profile',
    url: '#',
    icon: 'user',
  },
];

class PrivateLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: true,
    };
  }

  componentDidMount() {
    this.props.isAuthenticated && push('/login');
  }

  toggle = () => {
    this.setState(prevState => ({
      collapsed: !prevState.collapsed,
    }));
  };

  render() {
    const { children, isAuthenticated, location } = this.props;
    const url = getCurrentTab(location.pathname);
    if (!isAuthenticated) {
      return !PUBLIC_ROUTES.find(e => e.path === location.pathname) ? (
        <Redirect to="/login" />
      ) : null;
    }
    return (
      <PrivateLayoutWrapper>
        <Layout className="windowView">
          <input
            onChange={() => { }}
            id="collapsedTracker"
            type="checkbox"
            checked={!this.state.collapsed}
          />
          <label htmlFor="collapsedTracker" className="overlay" onClick={this.toggle} />
          <Sider trigger={null} collapsible collapsed={this.state.collapsed} className="sidebar">
            <div className="logo">
              <img alt="" src={Logo} />
              <img alt="" src={FullLogo} className="fullLogo" />
            </div>
            <Menu
              mode="inline"
              selectedKeys={[url || 'dashboard']}
              defaultSelectedKeys={[url || 'dashboard']}
            >
              {sidebarMenu.map(menu => (

                <Menu.Item key={menu.key} title={I18n.t(menu.text)} onClick={() => history.push(menu.url)}>
                  <SVGIcon type={menu.icon} />
                  {!this.state.collapsed && <span>{I18n.t(menu.text)}</span>}
                </Menu.Item>
              ))}
            </Menu>
          </Sider>
          <Layout className="mainView">
            <Header onToggle={this.toggle} collapsed={this.state.collapsed} />
            <Content className="container">
              <div className="content">{children}</div>
              <Footer className="footer">{I18n.t('appInfo.footer')}</Footer>
              <Footer className="footerMobile">
                {mobileTabs.map(tab => (
                  <a href={tab.url} key={tab.key}>
                    <Icon type={tab.icon} className="tabIcon" />
                  </a>
                ))}
              </Footer>
            </Content>
          </Layout>
        </Layout>
      </PrivateLayoutWrapper>
    );
  }
}

PrivateLayout.propTypes = {
  children: PropTypes.any,
  isAuthenticated: PropTypes.bool,
  location: PropTypes.object,
};

export default connect(
  state => ({
    isAuthenticated: state.auth.isAuthenticated,
    location: state.router.location,
  }),
  {
    logout: logoutAction,
  }
)(PrivateLayout);
