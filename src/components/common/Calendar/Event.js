import React from 'react';
import PropTypes from 'prop-types';

const Event = ({ event }) => (
  <div className="row">
    <strong>{event.title}</strong>
  </div>
  );
Event.propTypes = {
  event: PropTypes.object,
};

export default Event;
