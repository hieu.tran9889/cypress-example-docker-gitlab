import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { keyBy } from 'lodash';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import { CalendarWrapper } from './styles';
import Event from './Event';
import Header from './Header';

const localizer = BigCalendar.momentLocalizer(moment);

const DragAndDropCalendar = withDragAndDrop(BigCalendar);

export default class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.moveEvent = this.moveEvent.bind(this);
    this.newEvent = this.newEvent.bind(this);
  }

  onView = e => {
    this.props.onView(e);
  };

  onChangeEvent = (events, event) => {
    const { changeReservationsEvent } = this.props;
    changeReservationsEvent(keyBy(events, 'id'), events.map(data => data.id), {
      ...event,
      start: moment(event.start).toISOString(),
      end: moment(event.end).toISOString(),
    });
  };

  resizeEvent = ({ event, start, end }) => {
    const { events } = this.props;

    const nextEvents = events.map(existingEvent =>
      existingEvent.id === event.id ? { ...existingEvent, start, end } : existingEvent
    );
    this.onChangeEvent(nextEvents, { ...event, start, end });
    // alert(`${event.title} was resized to ${start}-${end}`)
  };

  moveEvent({ event, start, end, isAllDay: droppedOnAllDaySlot }) {
    const { events } = this.props;
    const idx = events.indexOf(event);
    let isAllDay = event.allDay;

    if (!event.allDay && droppedOnAllDaySlot) {
      isAllDay = true;
    } else if (event.allDay && !droppedOnAllDaySlot) {
      isAllDay = false;
    }

    const updatedEvent = { ...event, start, end, allDay: isAllDay };

    const nextEvents = [...events];
    nextEvents.splice(idx, 1, updatedEvent);
    this.onChangeEvent(nextEvents, updatedEvent);

    // alert(`${event.title} was dropped onto ${updatedEvent.start}`)
  }

  newEvent(event) {
    const idList = this.props.events.map(a => a.id);
    const newId = Math.max(...idList) + 1;
    const hour = {
      id: newId,
      title: 'New Event',
      allDay: event.slots.length === 1,
      start: moment(event.start).toISOString(),
      end: moment(event.end).toISOString(),
    };
    this.props.newEvent(hour);
  }

  render() {
    return (
      <CalendarWrapper>
        <DragAndDropCalendar
          selectable
          localizer={localizer}
          events={this.props.events}
          onEventDrop={this.moveEvent}
          resizable
          onEventResize={this.resizeEvent}
          onSelectSlot={this.newEvent}
          onSelectEvent={this.props.onSelectEvent}
          defaultView={this.props.defaultView}
          defaultDate={new Date()}
          onView={this.onView}
          onRangeChange={this.props.onRangeChange}
          components={{
            event: Event,
            day: { header: Header },
            week: { header: Header },
            month: { header: Header },
          }}
        />
      </CalendarWrapper>
    );
  }
}

Calendar.propTypes = {
  changeReservationsEvent: PropTypes.func,
  events: PropTypes.array,
  newEvent: PropTypes.func,
  onSelectEvent: PropTypes.func,
  defaultView: PropTypes.string,
  onView: PropTypes.func,
  onRangeChange: PropTypes.func,
};

Calendar.defaultProps = {
  changeReservationsEvent: () => {},
  events: [],
  defaultView: BigCalendar.Views.MONTH,
};
