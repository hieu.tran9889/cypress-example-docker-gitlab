import styled from 'styled-components';

export const CalendarWrapper = styled.div`
  height: 100vh;
  .rbc-agenda-date-cell,
  .header,
  .rbc-agenda-time-cell,
  .rbc-agenda-event-cell {
    padding: 10px;
    color: ${({ theme }) => theme.text.highlight};
  }
  .rbc-btn-group {
    button {
      border: 1px solid ${({ theme }) => theme.background.container};
      background: ${({ theme }) => theme.background.content};
      color: ${({ theme }) => theme.text.primary};
      :hover,
      :active,
      :focus {
        background: ${({ theme }) => theme.palette.primary};
        color: ${({ theme }) => theme.palette.secondary};
      }
    }
    .rbc-active {
      background: ${({ theme }) => theme.palette.primary};
      color: ${({ theme }) => theme.palette.secondary};
    }
  }
  .rbc-toolbar {
    .rbc-toolbar-label {
      color: ${({ theme }) => theme.palette.primary};
    }
  }
  .rbc-month-view,
  .rbc-header,
  .rbc-month-row,
  .rbc-time-view,
  .rbc-time-header,
  .rbc-time-content,
  .rbc-agenda-view table.rbc-agenda-table {
    background: ${({ theme }) => theme.background.container};
    border: 1px solid ${({ theme }) => theme.background.content};
    color: ${({ theme }) => theme.text.highlight};
  }
  .rbc-day-bg,
  .rbc-time-header-content,
  .rbc-time-content > * + * > *,
  .rbc-agenda-event-cell,
  .rbc-agenda-view table.rbc-agenda-table tbody > tr > td + td {
    border-left: 1px solid ${({ theme }) => theme.background.content};
  }
  .rbc-timeslot-group,
  .rbc-agenda-view table.rbc-agenda-table thead > tr > th {
    border-bottom: 1px solid ${({ theme }) => theme.background.content};
  }
  .rbc-day-slot .rbc-time-slot,
  .rbc-agenda-view table.rbc-agenda-table tbody > tr + tr {
    border-top: 1px solid ${({ theme }) => theme.background.content};
    background: ${({ theme }) => theme.background.container};
  }
  .rbc-label {
    color: ${({ theme }) => theme.text.highlight};
  }
  .rbc-agenda-empty,
  .rbc-event {
    background: ${({ theme }) => theme.text.highlight};
    color: ${({ theme }) => theme.palette.secondary};
  }
  .rbc-today {
    background: ${({ theme }) => theme.palette.primary};
  }
  .iconClose {
    color: ${({ theme }) => theme.text.primary};
    margin-right: 10px;
  }
  .row {
    display: flex;
    align-items: center;
  }
  .title {
    flex: 1;
  }
`;
