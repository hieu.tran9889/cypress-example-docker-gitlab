import List from '../../containers/Rooms/List';
import Edit from '../../containers/Rooms/Edit';
import Create from '../../containers/Rooms/Create';
import Show from '../../containers/Rooms/Show';

export default { List, Edit, Create, Show };
