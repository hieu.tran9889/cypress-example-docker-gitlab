import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Modal } from 'antd';
import { push } from 'connected-react-router';
import Box from '../../components/common/Box';
import Calendar from '../../components/common/Calendar';
import reservationsActions, {
  changeReservationsEvent as changeReservationsEventAction,
  changeDefaultView as saveDefaultViewAction,
} from '../../redux/reservations/actions';
import { reservationsData } from '../../redux/reservations/selectors';
import { getFilterFromUrl } from '../../utils/tools';
import { showModal as showModalAction } from '../../redux/modal/actions';

class Reservations extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    const filter = getFilterFromUrl(props.location.search);
    if (props.location.hash === '') {
      this.props.getReservations(
        filter
          ? {
              limit: 50,
              offset: 0,
              filter,
            }
          : {},
        true
      );
    }
  }

  onChangeEvent = (data, ids, event) => {
    const that = this;
    Modal.confirm({
      title: 'Bạn có chắc chắn thay đổi thời gian booking này?',
      onOk() {
        that.props.updateBooking(event.bookingId, {
          startTime: event.start,
          actualEndTime: event.end,
        });
        that.props.changeReservationsEvent(data, ids);
      },
      onCancel() {},
    });
  };

  onView = e => {
    this.props.saveDefaultView(e);
  };

  onRangeChange = data => {
    let startTime = '';
    let endTime = '';
    if (Array.isArray(data)) {
      // eslint-disable-next-line
      startTime = data[0];
      endTime = data[data.length - 1];
    } else {
      startTime = data.start;
      endTime = data.end;
    }
    this.retrieveList({
      limit: 50,
      offset: 0,
      filter: {
        checkinTime: { $gte: moment(startTime).startOf('d') },
        checkoutTime: { $lte: moment(endTime).endOf('d') },
      },
    });
  };

  onSelectEvent = data => {
    const { pushRoute } = this.props;
    pushRoute(`#bookings/${data.bookingId}/edit`);
    this.props.editBooking(data.bookingId);
  };

  newEvent = () => {
    const route = '#bookings/create';
    const { pushRoute } = this.props;
    pushRoute(route);
  };

  retrieveList = (filter = {}) => {
    this.props.getReservations(filter);
  };

  render() {
    const { reservations, defaultView } = this.props;
    return (
      <Box style={{ height: '100%' }}>
        <Calendar
          defaultView={defaultView}
          newEvent={this.newEvent}
          onSelectEvent={this.onSelectEvent}
          events={reservations}
          changeReservationsEvent={this.onChangeEvent}
          onView={this.onView}
          onRangeChange={this.onRangeChange}
        />
      </Box>
    );
  }
}

Reservations.propTypes = {
  getReservations: PropTypes.func,
  reservations: PropTypes.array,
  editBooking: PropTypes.func,
  defaultView: PropTypes.string,
  saveDefaultView: PropTypes.func,
  location: PropTypes.object,
  pushRoute: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    reservations: reservationsData(state),
    defaultView: state.reservations.defaultView,
  };
}

const mapDispatchToProps = dispatch => ({
  saveDefaultView: data => dispatch(saveDefaultViewAction(data)),
  getReservations: data => dispatch(reservationsActions.getAllReservations(data)),
  // createBooking: data =>
  //   dispatch(showModalAction(`/bookings/create?startTime=${data.start}&actualEndTime=${data.end}`)),
  editBooking: id => dispatch(showModalAction(`/bookings/${id}/edit`)),
  changeReservationsEvent: (data, ids) => dispatch(changeReservationsEventAction(data, ids)),
  updateBooking: (id, data) => dispatch(reservationsActions.editReservations({ id, ...data })),
  pushRoute: data => dispatch(push(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reservations);
