import List from '../../containers/Locations/List';
import Edit from '../../containers/Locations/Edit';
import Create from '../../containers/Locations/Create';
import Show from '../../containers/Locations/Show';

export default { List, Edit, Create, Show };
