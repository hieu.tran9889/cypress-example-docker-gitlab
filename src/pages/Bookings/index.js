import List from '../../containers/Bookings/List';
import Edit from '../../containers/Bookings/Edit';
import Create from '../../containers/Bookings/Create';
import Show from '../../containers/Bookings/Show';

export default { List, Edit, Create, Show };
