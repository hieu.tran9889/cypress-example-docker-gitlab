import React from 'react';
import PropTypes from 'prop-types';
import { Tabs } from 'antd';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { push } from 'connected-react-router';
import Rooms from '../../containers/Rooms/List';
import Packages from '../../containers/Packages/List';
import PackageTypes from '../../containers/PackageTypes/List';
import TransactionTypes from '../../containers/TransactionTypes/List';

const { TabPane } = Tabs;

const TABS = [
  {
    key: 'rooms',
    text: 'settings.tabs.rooms',
    url: '/rooms',
    component: Rooms,
  },
  {
    key: 'packages',
    text: 'settings.tabs.packages',
    url: '/packages',
    component: Packages,
  },
  {
    key: 'packageTypes',
    text: 'settings.tabs.packageTypes',
    url: '/packageTypes',
    component: PackageTypes,
  },
  {
    key: 'transactionTypes',
    text: 'settings.tabs.transactionTypes',
    url: '/transactionTypes',
    component: TransactionTypes,
  },
];

const Settings = ({ match, pushRoute, ...props }) => {
  const onChange = key => {
    pushRoute(`/settings/${key}`);
  };
  return (
    <div>
      <Tabs defaultActiveKey={match.params.model || 'rooms'} onChange={onChange}>
        {TABS.map(tab => (
          <TabPane tab={i18next.t(tab.text)} key={tab.key}>
            <tab.component
              rootPath="/settings"
              noCardWrapper
              layoutButtonCreate="no-inline"
              {...props}
            />
          </TabPane>
        ))}
      </Tabs>
    </div>
  );
};

Settings.propTypes = {
  match: PropTypes.object,
  pushRoute: PropTypes.func,
};

export default connect(
  null,
  dispatch => ({
    pushRoute: data => dispatch(push(data)),
  })
)(Settings);
