import List from '../../containers/Packages/List';
import Edit from '../../containers/Packages/Edit';
import Create from '../../containers/Packages/Create';
import Show from '../../containers/Packages/Show';

export default { List, Edit, Create, Show };
