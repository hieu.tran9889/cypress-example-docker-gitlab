import React from 'react';
import i18next from 'i18next';
// import PropTypes from 'prop-types';
import HomeWrapper from './style';
import PageTitle from '../../components/common/PageTitle';
import ListBookingWithSocket from '../../containers/Bookings/List/ListWithSocket';
import Summaries from '../../containers/Dashboard/Summaries';

const Home = props => (
  <HomeWrapper>
    <PageTitle>{i18next.t('home.title')}</PageTitle>
    <Summaries {...props} />
    <ListBookingWithSocket {...props} />
  </HomeWrapper>
);

Home.propTypes = {};

export default Home;
