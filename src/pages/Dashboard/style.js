import styled from 'styled-components';

const HomeWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  .viewSummary {
    width: 100%;
    margin-bottom: 20px;
  }
  .vBox {
    background: transparent;
    border: none;
    padding: 0 0px;
  }
`;

export default HomeWrapper;
