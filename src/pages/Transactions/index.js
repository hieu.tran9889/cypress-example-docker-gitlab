import List from '../../containers/Transactions/List';
import Edit from '../../containers/Transactions/Edit';
import Create from '../../containers/Transactions/Create';
import Show from '../../containers/Transactions/Show';

export default { List, Edit, Create, Show };
