import React from 'react';
// import PropTypes from 'prop-types';
import List from '../../../containers/Transactions/List';
import Summaries from '../../../containers/Transactions/components/Summaries';

const TransactionList = props => (
  <div>
    <Summaries {...props} />
    <List {...props} />
  </div>
);
TransactionList.propTypes = {};

export default TransactionList;
