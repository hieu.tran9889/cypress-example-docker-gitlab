import List from '../../containers/TransactionTypes/List';
import Edit from '../../containers/TransactionTypes/Edit';
import Create from '../../containers/TransactionTypes/Create';
import Show from '../../containers/TransactionTypes/Show';

export default { List, Edit, Create, Show };
