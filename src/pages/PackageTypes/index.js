import List from '../../containers/PackageTypes/List';
import Edit from '../../containers/PackageTypes/Edit';
import Create from '../../containers/PackageTypes/Create';
import Show from '../../containers/PackageTypes/Show';

export default { List, Edit, Create, Show };
