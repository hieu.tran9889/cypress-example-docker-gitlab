import List from '../../containers/Customers/List';
import Edit from '../../containers/Customers/Edit';
import Create from '../../containers/Customers/Create';
import Show from '../../containers/Customers/Show';

export default { List, Edit, Create, Show };
