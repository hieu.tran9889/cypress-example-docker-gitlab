import React from 'react';
// import PropTypes from 'prop-types';
import Edit from '../../../containers/Customers/Edit';
import Summaries from '../../../containers/Customers/components/Summaries';

const EditCustomers = props => (
  <div>
    <Summaries {...props} />
    <Edit {...props} />
  </div>
);
EditCustomers.propTypes = {};

export default EditCustomers;
