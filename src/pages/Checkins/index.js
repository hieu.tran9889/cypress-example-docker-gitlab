import List from '../../containers/Checkins/List';
import Edit from '../../containers/Checkins/Edit';
import Create from '../../containers/Checkins/Create';
import Show from '../../containers/Checkins/Show';

export default { List, Edit, Create, Show };
