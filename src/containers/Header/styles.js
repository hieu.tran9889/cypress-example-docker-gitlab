import styled from 'styled-components';
import { Layout } from 'antd';

const HeaderWrapper = styled(Layout.Header)`
  border-bottom: 1.5px solid ${({ theme }) => theme.background.container};
  background: ${({ theme }) => theme.background.content};
  .btn {
    margin-right: 10px;
  }
  .trigger {
    color: ${({ theme }) => theme.text.primary};
  }
`;

export default HeaderWrapper;
