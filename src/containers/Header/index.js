import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import I18n from 'i18next';
import { Menu, Icon, Dropdown, Avatar, Button } from 'antd';
import { push } from 'connected-react-router';
import { logout as logoutAction } from '../../redux/auth/actions';
import HeaderWrapper from './styles';

const Header = ({ logout, onToggle, collapsed, pushRoute }) => {
  const [locale, setLocale] = useState(localStorage.getItem('locale') || I18n.language);
  const profileMenu = [
    {
      key: 'profile',
      text: 'header.profile',
      url: '#',
    },
  ];
  const gotoCreateIncome = () => {
    const route = '#transactions/createForIncome';
    pushRoute(route);
  };
  const gotoCreateExpense = () => {
    const route = '#transactions/createForExpense';
    pushRoute(route);
  };

  const gotoCreateBooking = () => {
    const route = '#bookings/create';
    pushRoute(route);
  };

  const changeLocale = e => () => {
    setLocale(e);
    I18n.changeLanguage(e);
    localStorage.setItem('locale', e);
  };

  useEffect(() => {
    I18n.changeLanguage(locale);
  }, []);

  return (
    <HeaderWrapper className="header">
      <div className="leftHeader">
        <Icon
          className="trigger"
          type={collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={onToggle}
        />
        <div
          className={`localeSelect${locale === 'vi' ? ' active' : ''}`}
          role="presentation"
          onClick={changeLocale('vi')}
        >
          VI
        </div>
        <div
          className={`localeSelect${locale === 'en' ? ' active' : ''}`}
          role="presentation"
          onClick={changeLocale('en')}
        >
          EN
        </div>
        <div className="title">{I18n.t('appInfo.name')}</div>
      </div>
      <div className="rightHeader">
        <Button onClick={gotoCreateIncome} className="btn" type="primary">
          <Icon type="plus" />
          {I18n.t('button.income')}
        </Button>
        <Button onClick={gotoCreateExpense} className="btn" type="primary">
          <Icon type="minus" />
          {I18n.t('button.expense')}
        </Button>
        <Button onClick={gotoCreateBooking} className="btn" type="primary">
          <Icon type="ic-seat" />
          {I18n.t('bookings.createPage')}
        </Button>
        <Dropdown
          overlay={(
            <Menu style={{ minWidth: '120px' }}>
              {profileMenu.map(menu => (
                <Menu.Item key={menu.key}>
                  <a href={menu.url}>{I18n.t(menu.text)}</a>
                </Menu.Item>
              ))}
              <Menu.Divider />
              <Menu.Item onClick={logout} key="logout">
                {I18n.t('header.logout')}
              </Menu.Item>
            </Menu>
)}
          trigger={['click']}
        >
          <Avatar size="large" icon="user" />
        </Dropdown>
      </div>
    </HeaderWrapper>
  );
};
Header.propTypes = {
  logout: PropTypes.func,
  collapsed: PropTypes.bool,
  onToggle: PropTypes.func,
  pushRoute: PropTypes.func,
};

export default connect(
  state => ({
    current: state.modal.current,
  }),
  dispatch => ({
    logout: () => dispatch(logoutAction()),
    pushRoute: data => dispatch(push(data)),
  })
)(Header);
