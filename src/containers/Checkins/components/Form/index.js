import React from 'react';
import { Row, Col } from 'antd';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import RestDateTimePicker from '../../../../components/RestInput/RestDateTimePicker';
import Reference from '../../../rest/Reference';
import UserInfo from '../../../../components/RestField/UserInfo';

const CheckinsForm = props => (
  <div {...props}>
    <RestInputItem isReference>
      <Reference source="userId" resource="users">
        <UserInfo prefixLink="customers" />
      </Reference>
    </RestInputItem>
    <br />
    <RestInputItem disabled source="bookingId" header="checkins.bookingId" />
    <Row gutter={16}>
      <Col span={12}>
        <RestDateTimePicker source="startTime" header="checkins.startTime" />
      </Col>
      <Col span={12}>
        <RestDateTimePicker source="endTime" header="checkins.endTime" />
      </Col>
    </Row>
  </div>
);

CheckinsForm.propTypes = {};

export default CheckinsForm;
