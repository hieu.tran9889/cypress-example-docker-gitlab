import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="bookingId" placeholder="checkins.bookingId" />
    <RestInputItem source="userId" placeholder="checkins.user" />
    <RestInputItem source="startTime" placeholder="checkins.startTime" />
    <RestInputItem source="endTime" placeholder="checkins.endTime" />
  </div>
);

Filter.propTypes = {};

export default Filter;
