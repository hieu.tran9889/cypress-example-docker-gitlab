import React from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import EditButton from '../../../components/RestActions/EditButton';
import { formatDateTime } from '../../../utils/textUtils';
import Reference from '../../rest/Reference';
import UserInfo from '../../../components/RestField/UserInfo';

const CheckinsList = ({ isCustomerPage, ...props }) => (
  <List {...props} hasCreate={false} hasExport={false} resource="checkins">
    <RestFieldItem
      header="checkins.bookingId"
      format={data => (
        <Link href={`#bookings/${data}/edit`} to={`#bookings/${data}/edit`}>
          #{data}
        </Link>
      )}
      source="bookingId"
      width={100}
    />
    {!isCustomerPage && (
      <Reference source="userId" header="checkins.user" resource="users">
        <RestFieldItem
          format={(data, record) => <UserInfo prefixLink="customers" record={record} />}
          source="email"
        />
      </Reference>
    )}
    <RestFieldItem
      sorter
      format={data => formatDateTime(data)}
      source="startTime"
      header="checkins.startTime"
    />
    <RestFieldItem
      sorter
      format={data => formatDateTime(data)}
      source="endTime"
      header="checkins.endTime"
    />
    <EditButton />
  </List>
);

CheckinsList.propTypes = {
  isCustomerPage: propTypes.bool,
};

export default CheckinsList;
