import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const CheckinsShow = props => (
  <RestShow {...props} hasEdit resource="checkins">
    <RestFieldItem source="bookingId" header="checkins.bookingId" />
    <RestFieldItem source="userId" header="checkins.user" />
    <RestFieldItem source="startTime" header="checkins.startTime" />
    <RestFieldItem source="endTime" header="checkins.endTime" />
  </RestShow>
);

export default CheckinsShow;
