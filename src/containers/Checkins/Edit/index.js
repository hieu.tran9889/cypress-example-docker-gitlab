import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form';

const CheckinsEdit = props => (
  <Edit {...props} resource="checkins">
    <Form />
  </Edit>
);

CheckinsEdit.propTypes = {};

export default CheckinsEdit;
