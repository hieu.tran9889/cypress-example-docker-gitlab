import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form';

const CheckinsCreate = props => (
  <Create {...props} resource="checkins">
    <Form />
  </Create>
);

CheckinsCreate.propTypes = {};

export default CheckinsCreate;
