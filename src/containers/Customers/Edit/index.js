import React from 'react';
import { Row, Col, Tabs } from 'antd';
import i18next from 'i18next';
import Edit from '../../rest/Edit';
import CustomerDetail from '../components/CustomerDetail';
import ListBookings from '../../Bookings/List';
import ListCheckins from '../../Checkins/List';
import ListTransactions from '../../Transactions/List';
import CustomerContact from '../components/CustomerContact';

const CustomersEdit = props => (
  <Edit {...props} customEditButton={<span />} noCardWrapper resource="customers">
    <Row gutter={16}>
      <Col md={8}>
        <CustomerContact />
        <CustomerDetail />
      </Col>
      <Col md={16}>
        <Tabs>
          <Tabs.TabPane tab={i18next.t('bookings.title')} key="bookings">
            <ListBookings
              {...props}
              hasSearch={false}
              noCardWrapper
              initialFilter={{ filter: { userId: props.match.params.id } }}
              isUpdateRoute={false}
              isCustomerPage
            />
          </Tabs.TabPane>
          <Tabs.TabPane tab={i18next.t('checkins.title')} key="checkins">
            <ListCheckins
              {...props}
              noCardWrapper
              hasSearch={false}
              hasCreate={false}
              initialFilter={{ filter: { userId: props.match.params.id } }}
              isUpdateRoute={false}
              isScroll={false}
              isCustomerPage
            />
          </Tabs.TabPane>
          <Tabs.TabPane tab={i18next.t('transactions.header')} key="transactions">
            <ListTransactions
              {...props}
              noCardWrapper
              initialFilter={{ filter: { userId: props.match.params.id } }}
              isUpdateRoute={false}
              isCustomerPage
            />
          </Tabs.TabPane>
        </Tabs>
      </Col>
    </Row>
  </Edit>
);

CustomersEdit.propTypes = {};

export default CustomersEdit;
