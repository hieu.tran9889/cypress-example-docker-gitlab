import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';
import { Row, Col } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import SummaryCard from '../../../../components/common/SummaryCard';
import { formatMoney } from '../../../../utils/textUtils';
import { getCustomersSummaries } from '../../../../redux/reports/actions';

const Summaries = ({ theme, match }) => {
  const dispatch = useDispatch();
  const customerSummaries = useSelector(state => state.reports.customerSummaries);
  useEffect(() => {
    dispatch(getCustomersSummaries(match.params.id));
  }, []);
  const SUMMARIES = [
    {
      icon: 'ic-payment',
      header: 'customers.summaries.revenue',
      value: formatMoney(customerSummaries.totalIncome) || 0,
      color: theme.color.violet,
    },
    {
      icon: 'ic-seat',
      header: 'customers.summaries.totalBookings',
      value: customerSummaries.bookingCount || 0,
      color: theme.color.blueShade,
    },
  ];
  return (
    <Row gutter={24} type="flex">
      {SUMMARIES.map((data, index) => (
        <Col key={String(index)} lg={12} md={12} xs={24}>
          <SummaryCard {...data} />
        </Col>
      ))}
    </Row>
  );
};
Summaries.propTypes = {
  theme: PropTypes.object,
  match: PropTypes.object,
};

export default withTheme(Summaries);
