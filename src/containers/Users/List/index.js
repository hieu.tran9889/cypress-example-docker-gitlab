import React from 'react';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';
import UserInfo from '../../../components/RestField/UserInfo';
import { formatDateTime } from '../../../utils/textUtils';

const UsersList = props => (
  <List {...props} resource="users">
    <UserInfo hasSearch prefixLink="customers" source="name" header="customers.name" />
    <RestFieldItem hasSearch source="email" header="customers.email" />
    <RestFieldItem hasSearch source="phoneNumber" header="customers.phoneNumber" />
    <RestFieldItem source="job" header="customers.job" />
    <RestFieldItem source="nationality" header="customers.nationality" />
    <RestFieldItem
      format={data => formatDateTime(data)}
      source="departDate"
      header="customers.departDate"
    />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

UsersList.propTypes = {};

export default UsersList;
