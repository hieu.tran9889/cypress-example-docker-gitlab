import styled from 'styled-components';

export const CheckoutModalWrapper = styled.div`
  .ant-calendar-picker,
  .ant-time-picker {
    width: 100% !important;
    height: 40px;
    border: 1px solid ${({ theme }) => theme.border.default};
    border-radius: 2px;
    background: ${({ theme }) => theme.background.content};
    &:hover {
      border: 1px solid ${({ theme }) => theme.palette.primary};
    }
    .anticon {
      color: ${({ theme }) => theme.palette.primary};
    }
    .ant-calendar-picker-input,
    .ant-input,
    .ant-time-picker-input {
      background: transparent;
      border-bottom: none !important;
      padding-left: 10px;
      height: 40px;
      color: ${({ theme }) => theme.palette.primary};
      &:hover {
        border: none;
      }
    }
  }
  .modal-footer {
    text-align: center;
    align-items: center;
    margin-top: 20px;
    display: flex;
    .ant-btn {
      flex: 1;
      margin-right: 20px;
      &:last-child {
        margin-right: 0px;
      }
    }
  }
`;
