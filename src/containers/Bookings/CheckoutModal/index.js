import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import i18next from 'i18next';
import { connect } from 'react-redux';
import { Button, Popconfirm, TimePicker } from 'antd';
import { replace } from 'connected-react-router';
import { checkoutBooking } from '../../../redux/realtimeBookings/actions';
import InfoRow from '../components/InfoRow';
import Text from '../../../components/common/Text';
import {
  formatMoney,
  formatDate,
  formatTime,
  formatDisCount,
  formatDuration,
} from '../../../utils/textUtils';
import { CheckoutModalWrapper } from './styles';
import { checkPricing } from '../../../api/bookings';
import { currentBooking } from '../../../redux/realtimeBookings/selectors';

class CheckoutModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalAmountChanged: null,
      checkoutTime: moment(),
    };
  }

  onChangeDate = date => {
    this.setState({ checkoutTime: date });
    const { booking } = this.props;
    const {
      numberOfCustomer,
      checkins,
      discountUnit,
      discountValue,
      startTime,
      roomId,
      packageTypeId,
    } = booking;
    const data = {
      roomId,
      packageTypeId,
      startTime: moment(checkins && checkins[0] ? checkins[0].startTime : startTime).toISOString(),
      endTime: date.toISOString(),
      numberOfCustomer,
      discountValue,
      discountUnit,
    };
    checkPricing(data).then(response => {
      this.setState({ totalAmountChanged: response.totalAmount });
    });
  };

  checkout = isPay => () => {
    const { booking, checkout } = this.props;
    checkout(booking, isPay, { endTime: this.state.checkoutTime.toISOString() });
  };

  render() {
    const { booking, closeModal } = this.props;
    const { totalAmountChanged } = this.state;
    const {
      customerName,
      checkins,
      totalAmount,
      numberOfCustomer,
      discountUnit,
      discountValue,
      isPaid,
    } = booking;
    const CHECKIN_DATA = [
      {
        title: 'bookings.customerName',
        value: customerName,
      },
      {
        title: 'bookings.package',
        value: booking.package ? booking.package.name : '',
      },
      {
        title: 'bookings.packageAmount',
        value: `${formatMoney(booking.package ? booking.package.price : 0)} VND`,
      },
      {
        title: 'bookings.numberOfCustomer',
        value: numberOfCustomer,
      },
      {
        title: 'bookings.discount',
        value: formatDisCount(discountUnit, discountValue),
      },
      {
        title: 'bookings.checkinDate',
        value: checkins && checkins[0] ? formatDate(checkins[0].startTime) : '',
      },
      {
        title: 'bookings.startTime',
        value: checkins && checkins[0] ? formatTime(checkins[0].startTime) : '',
      },
      {
        title: 'bookings.endTime',
        value: (
          <TimePicker
            allowEmpty={false}
            format="HH:mm"
            defaultValue={
              checkins && checkins[0] && checkins[0].endTime
                ? formatTime(checkins[0].endTime)
                : moment()
            }
            onChange={this.onChangeDate}
          />
        ),
      },
      {
        title: 'bookings.durations',
        value:
          checkins && checkins[0] ? `${formatDuration(checkins[0], this.state.checkoutTime)}` : '',
      },
    ];

    const footer = (
      <div className="modal-footer">
        <Button size="large" onClick={closeModal}>
          {i18next.t('button.cancel')}
        </Button>
        {!isPaid && (
          <Popconfirm
            placement="top"
            title={i18next.t('bookings.confirmPay')}
            onConfirm={this.checkout(true)}
            okText={i18next.t('button.ok')}
            cancelText={i18next.t('button.cancel')}
          >
            <Button size="large" type="primary">
              {i18next.t('button.checkoutAndPay')}
            </Button>
          </Popconfirm>
        )}
        <Button size="large" type="primary" onClick={this.checkout()}>
          {i18next.t('button.checkout')}
        </Button>
      </div>
    );

    return (
      <CheckoutModalWrapper>
        {isPaid ? (
          <Text type="h4White" align="center" className="modalTitle">
            {i18next.t('bookings.isPaid')}
          </Text>
        ) : (
          <Text type="h4White" align="center" className="modalTitle">
            {i18next.t('bookings.totalAmount')} {formatMoney(totalAmountChanged || totalAmount)} VND
          </Text>
        )}
        {CHECKIN_DATA.map(data => (
          <InfoRow key={data.title} {...data} />
        ))}
        {footer}
      </CheckoutModalWrapper>
    );
  }
}

CheckoutModal.propTypes = {
  checkout: PropTypes.func,
  booking: PropTypes.object,
  closeModal: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    booking: currentBooking(state),
  };
}

const mapDispatchToProps = (dispatch, props) => ({
  checkout: (booking, isPay, payload) => dispatch(checkoutBooking(booking, isPay, payload)),
  closeModal: () => dispatch(replace(props.location.pathname)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutModal);
