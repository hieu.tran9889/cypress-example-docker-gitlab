import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const BookingsShow = props => (
  <RestShow {...props} hasEdit resource="bookings">
    <RestFieldItem source="customerName" header="bookings.customerName" />
    <RestFieldItem source="email" header="bookings.email" />
    <RestFieldItem source="roomId" header="bookings.room" />
    <RestFieldItem source="packageId" header="bookings.package" />
    <RestFieldItem source="packageTypeId" header="bookings.packageType" />
    <RestFieldItem source="discountValue" header="bookings.discount" />
    <RestFieldItem source="discountUnit" header="bookings.discountUnit" />
    <RestFieldItem source="startTime" header="bookings.startTime" />
    <RestFieldItem source="expectedEndTime" header="bookings.expectedEndTime" />
    <RestFieldItem source="note" header="bookings.note" />
    <RestFieldItem source="totalAmount" header="bookings.totalAmount" />
    <RestFieldItem source="status" header="bookings.status" />
    <RestFieldItem source="numberOfCustomer" header="bookings.numberOfCustomer" />
  </RestShow>
);

export default BookingsShow;
