import styled from 'styled-components';

export const EditBookingWrapper = styled.div`
  .row {
    display: flex;
    justify-content: space-between;
    margin-top: 20px;
  }
`;
