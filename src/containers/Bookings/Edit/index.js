import React from 'react';
import { Tabs, List, Empty } from 'antd';
import i18next from 'i18next';
import { useSelector } from 'react-redux';
import Edit from '../../rest/Edit';
import Form from '../components/Form';
// import { getIdByUrl } from '../../../utils/tools';
import { TRANSACTION_TYPE } from '../../../configs/localData';
import Text from '../../../components/common/Text';
import { getTotalValue } from '../../../utils/textUtils';
import PaymentItem from '../../Transactions/components/PaymentItem';
import { EditBookingWrapper } from './styles';
import EditButtonGroup from '../components/EditButtonGroup';
// import AddBookingPayment from '../AddBookingPayment';
// import { addPaymentForBooking } from '../../../redux/realtimeBookings/actions';
import { currentBooking } from '../../../redux/realtimeBookings/selectors';

const BookingsEdit = props => {
  const bookingData = useSelector(currentBooking);
  // const dispatch = useDispatch();

  // const bookingId = getIdByUrl({ ...props, resource: 'bookings' });
  // const addPayment = data => {
  //   dispatch(
  //     addPaymentForBooking(bookingId, {
  //       userId: bookingData.userId,
  //       bookingId: Number(bookingId),
  //       ...data,
  //     })
  //   );
  // };
  return (
    <EditBookingWrapper>
      <Text type="h4White" align="center" className="modalTitle">
        {i18next.t('bookings.title')}
      </Text>
      <Tabs>
        <Tabs.TabPane tab={i18next.t('bookings.info')} key="info">
          <Edit
            {...props}
            noCardWrapper
            header={null}
            customEditButton={<EditButtonGroup record={bookingData} />}
            record={bookingData}
            resource="bookings"
          >
            <Form isEdit />
          </Edit>
        </Tabs.TabPane>
        <Tabs.TabPane tab={i18next.t('transactions.header')} key="payment">
          {/* <AddBookingPayment {...props} noCardWrapper bookingId={bookingId} onSubmit={addPayment} /> */}
          <div className="row">
            <Text type="headline" align="center" className="txtIncome">
              {i18next.t(TRANSACTION_TYPE[0].text)}:
              {getTotalValue(
                bookingData && bookingData.transactions
                  ? bookingData.transactions.filter(data => data.category === 'INCOME')
                  : [],
                'amount'
              )}{' '}
              VND
            </Text>
            <Text type="headline" align="center" className="txtExpense">
              {i18next.t(TRANSACTION_TYPE[1].text)}: -
              {getTotalValue(
                bookingData && bookingData.transactions
                  ? bookingData.transactions.filter(data => data.category !== 'INCOME')
                  : [],
                'amount'
              )}{' '}
              VND
            </Text>
          </div>
          {bookingData && bookingData.transactions && bookingData.transactions.length && (
            <List
              dataSource={bookingData ? bookingData.transactions : []}
              renderItem={item => (
                <List.Item key={item.id}>
                  <PaymentItem record={item} />
                </List.Item>
              )}
            />
          )}
          {bookingData && bookingData.transactions && bookingData.transactions.length === 0 && (
            <Empty />
          )}
          {bookingData && !bookingData.transactions && <Empty />}
        </Tabs.TabPane>
      </Tabs>
    </EditBookingWrapper>
  );
};

BookingsEdit.propTypes = {};

export default BookingsEdit;
