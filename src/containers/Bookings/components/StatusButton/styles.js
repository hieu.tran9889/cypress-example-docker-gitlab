import styled from 'styled-components';

export const StatusButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  .textTitle {
    color: ${({ theme }) => theme.palette.primary};
  }
  .selectView {
    color: ${({ theme }) => theme.palette.secondary};
    background: ${({ theme }) => theme.palette.primary};
    border-radius: 20px;
    margin-right: 5px;
    :focus,
    :active,
    :hover {
      border-color: transparent;
      border-radius: 20px;
    }
    .ant-select-selection-selected-value,
    .ant-select-arrow,
    .ant-select-arrow-icon {
      color: ${({ theme }) => theme.palette.secondary} !important;
    }
    .ant-select-selection--single {
      border-bottom: 0px !important;
      background: ${({ theme }) => theme.palette.primary};
      border-radius: 20px;
      :focus {
        border-radius: 20px !important;
      }
    }
  }
  .anticon {
    background: transparent;
    font-size: 20px;
    color: ${({ theme }) => theme.text.primary};
    cursor: pointer;
    :focus,
    :active,
    :hover {
      cursor: pointer;
      color: ${({ theme }) => theme.palette.primary};
      transform: scale(1.1, 1.1);
    }
  }
  .ant-menu-vertical {
    border-right: none;
  }
  .ant-btn {
    .anticon {
      color: white;
    }
  }
`;
