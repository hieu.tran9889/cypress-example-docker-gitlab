import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import i18next from 'i18next';
import { Menu, Icon, Dropdown, Tooltip, Button } from 'antd';
import { BOOKING_STATUS } from '../../../../configs/localData';
import { updateBookingStatus } from '../../../../redux/realtimeBookings/actions';
import { StatusButtonWrapper } from './styles';

const StatusButton = ({ record, updateStatus, showBooking, isShowDetail }) => {
  const handleMenuClick = e => {
    if (e.key === 'detail') {
      showBooking(record);
    } else {
      updateStatus(record.id, BOOKING_STATUS.find(data => data.value === e.key).requestSuffixUrl);
    }
  };
  const menu = (
    <Menu onClick={handleMenuClick}>
      {BOOKING_STATUS.filter(data => data.requestSuffixUrl).map(data => (
        <Menu.Item value={data.value} key={data.value}>
          {i18next.t(data.text)}
        </Menu.Item>
      ))}
      {!isShowDetail && (
        <Menu.Item value="detail" key="detail">
          {i18next.t('button.detail')}
        </Menu.Item>
      )}
    </Menu>
  );
  return (
    <StatusButtonWrapper>
      <Dropdown overlay={menu} placement="bottomLeft" trigger={['hover']}>
        <Tooltip title={i18next.t('button.action')}>
          {!isShowDetail ? (
            <Icon type="ic-setting" />
          ) : (
            <Button size="large" type="primary">
              {i18next.t('button.action')}
              <Icon type="down" />
            </Button>
          )}
        </Tooltip>
      </Dropdown>
    </StatusButtonWrapper>
  );
};

StatusButton.propTypes = {
  record: PropTypes.object,
  updateStatus: PropTypes.func,
  showBooking: PropTypes.func,
  isShowDetail: PropTypes.bool,
};

function mapStateToProps() {
  return {};
}

const mapDispatchToProps = dispatch => ({
  updateStatus: (id, status) => dispatch(updateBookingStatus(id, status)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusButton);
