import React from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';
import Text from '../../../../components/common/Text';
import { InfoRowWrapper } from './styles';

const InfoRow = ({ title, value }) => (
  <InfoRowWrapper>
    <div className="txtWrapper">
      <Text type="body3" className="txtTitle" align="left">
        {i18next.t(title)}
      </Text>
    </div>
    <div className="txtWrapper">
      <Text type="body3" className="txtValue">
        {value}
      </Text>
    </div>
  </InfoRowWrapper>
);
InfoRow.propTypes = {
  title: PropTypes.string,
  value: PropTypes.any,
};

export default InfoRow;
