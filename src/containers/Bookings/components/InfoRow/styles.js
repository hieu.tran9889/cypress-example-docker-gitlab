import styled from 'styled-components';

export const InfoRowWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ theme }) => theme.background.container};
  margin-top: 8px;
  height: 40px;
  :last-child {
    border-bottom: 0px;
  }
  .txtWrapper {
    flex: 1;
  }
  .txtTitle {
    color: ${({ theme }) => theme.text.highlight};
    margin-left: 10px;
  }
  .txtValue {
    width: 100%;
    color: ${({ theme }) => theme.palette.primary};
  }
`;
