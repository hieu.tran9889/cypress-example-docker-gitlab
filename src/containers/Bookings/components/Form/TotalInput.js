import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Icon, InputNumber } from 'antd';
import i18next from 'i18next';
import { getRecordData } from '../../../../utils/tools';
import Text from '../../../../components/common/Text';
import { formatMoney } from '../../../../utils/textUtils';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import { TotalInputWrapper } from './styles';

const TotalInput = ({ form, record }) => {
  const [isEdit, setIsEdit] = useState(false);
  const toggleEdit = () => {
    setIsEdit(!isEdit);
  };
  const total = form.getFieldValue('totalAmount') || getRecordData(record, 'totalAmount');
  return (
    <TotalInputWrapper>
      <Text type="body">{i18next.t('bookings.totalAmount')}</Text>
      <div className="input" style={{ display: isEdit ? 'block' : 'none' }}>
        <RestInputItem
          ruleType="number"
          ContentComponent={InputNumber}
          defaultValue={0}
          source="totalAmount"
        />
      </div>
      {!isEdit && (
        <Text type="body">
          &nbsp;
          {`${formatMoney(total)} `}
        </Text>
      )}
      <Text type="body" className="textUnit">
        {i18next.t('text.vnd')}
      </Text>{' '}
      &nbsp;
      {!record || !record.isPaid ? (
        <Icon
          onClick={toggleEdit}
          type={isEdit ? 'check' : 'edit'}
          theme="outlined"
          className="iconEdit"
        />
      ) : (
        <span />
      )}
    </TotalInputWrapper>
  );
};

TotalInput.propTypes = {
  form: PropTypes.object,
  record: PropTypes.object,
};

function mapStateToProps() {
  return {};
}

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TotalInput);
