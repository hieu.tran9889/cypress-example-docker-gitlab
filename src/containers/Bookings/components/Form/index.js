import React, { useState, useEffect } from 'react';
import { Col, Row, InputNumber, Input, Badge } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import i18next from 'i18next';
import { RestInputContext } from '../../../../components/RestInput/RestInputContext';
import CustomerInfo from './CustomerInfo';
import PackageSelection from './PackageSelection';
import { getRooms, getConfigByName } from '../../../../redux/config/selectors';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import RestSelect from '../../../../components/RestInput/RestSelect';
import { DISCOUNT_UNIT, PACKAGE_TYPES_TIME_UNIT } from '../../../../configs/localData';
import DateTimePicker from '../../../../components/RestInput/RestDateTimePicker';
import TotalInput from './TotalInput';
import { checkPricing } from '../../../../api/bookings';
import { getDailyStatus } from '../../../../utils/textUtils';

const BookingsForm = ({ record, form, isEdit, ...props }) => {
  const [isCheckPrice, setIsCheckPrice] = useState(true);

  const updateEndTime = (packageTypeId, startTime, expectedEndTime) => {
    const { packageTypes } = props;

    const packageType = packageTypes && packageTypes.find(e => e.id === packageTypeId);
    if (!packageType) return;
    packageType.unit = PACKAGE_TYPES_TIME_UNIT[packageType.name.toLowerCase()];
    let estimateEndTime = moment(startTime);
    if (packageType.unit === 'fivedays') {
      estimateEndTime = moment(startTime).add(5, 'days');
    } else if (packageType.unit === 'sixdays') {
      estimateEndTime = moment(startTime).add(6, 'days');
    } else {
      estimateEndTime = moment(startTime).add(1, packageType.unit);
    }
    const formattedData = {
      startTime,
      expectedEndTime,
    };

    if (estimateEndTime.diff(moment(expectedEndTime)) > 0) {
      formattedData.expectedEndTime = estimateEndTime;
    }
    // eslint-disable-next-line
    return estimateEndTime;
  };

  const checkPrice = (newData = {}) => {
    setIsCheckPrice(false);
    const values = form.getFieldsValue();
    const {
      roomId,
      packageTypeId,
      startTime,
      expectedEndTime,
      numberOfCustomer,
      discountValue,
      discountUnit,
    } = values;
    // eslint-disable-next-line
    newData.packageTypeId = newData.packageTypeId || packageTypeId;

    if (newData.packageTypeId) {
      const estimateEndTime = newData.endTime
        ? newData.endTime
        : updateEndTime(newData.packageTypeId, startTime, newData.expectedEndTime);
      if (estimateEndTime !== expectedEndTime) {
        form.setFieldsValue({ expectedEndTime: estimateEndTime });
      }
      const data = {
        roomId,
        packageTypeId,
        startTime: moment(startTime).toISOString(),
        endTime: moment(estimateEndTime).toISOString(),
        numberOfCustomer,
        discountValue,
        discountUnit,
        ...newData,
      };
      checkPricing(data).then(response => {
        form.setFieldsValue({ totalAmount: response.totalAmount });
      });
    }
  };

  const checkPriceForDailyPackage = () => {
    form.setFieldsValue({
      ...record,
      expectedEndTime:
        record && record.package && record.package.isHour
          ? moment().toISOString()
          : record.expectedEndTime,
    });
    checkPrice();
  };

  const getValueFromEvent = key => value => {
    setIsCheckPrice(true);
    checkPrice({ [key]: value });
    if (typeof value === 'number' && value < 0) return 1;
    return value;
  };

  useEffect(() => {
    checkPriceForDailyPackage();
    if (props.rooms.length > 0 && isCheckPrice) {
      checkPrice();
    }
  }, [record]);

  return (
    <Row gutter={16}>
      <Col span={8}>
        <RestInputContext.Consumer>
          {({ record, form }) => <CustomerInfo {...props} {...{ record, form }} />}
        </RestInputContext.Consumer>
      </Col>
      <Col span={8}>
        <RestInputContext.Consumer>
          {({ record, form }) => (
            <PackageSelection
              formOptions={{
                getValueFromEvent,
              }}
              {...props}
              {...{ record, form }}
            />
          )}
        </RestInputContext.Consumer>
      </Col>
      <Col span={8}>
        <RestInputItem
          formOptions={{ getValueFromEvent: getValueFromEvent('numberOfCustomer') }}
          ContentComponent={InputNumber}
          ruleType="number"
          source="numberOfCustomer"
          header="bookings.numberOfCustomer"
        />
        <Row>
          <Col span={12}>
            <RestInputItem
              defaultValue={0}
              source="discountValue"
              header="bookings.discount"
              formOptions={{ getValueFromEvent: getValueFromEvent('discountValue') }}
              ContentComponent={InputNumber}
              ruleType="number"
            />
          </Col>
          <Col span={12}>
            <RestSelect
              resourceData={DISCOUNT_UNIT}
              defaultValue="percent"
              source="discountUnit"
              valueProp="value"
              titleProp="text"
              header="bookings.space"
              ruleType="string"
              formOptions={{ getValueFromEvent: getValueFromEvent('discountUnit') }}
            />
          </Col>
        </Row>
      </Col>
      <Col span={24}>
        <header header="bookings.space" />
      </Col>
      <Col span={isEdit ? 8 : 12}>
        <DateTimePicker
          required
          formOptions={{ getValueFromEvent: getValueFromEvent('startTime') }}
          initialValue={moment()}
          source="startTime"
          header="bookings.startTime"
        />
      </Col>
      <Col span={isEdit ? 8 : 12}>
        <DateTimePicker
          required
          formOptions={{ getValueFromEvent: getValueFromEvent('endTime') }}
          initialValue={moment().add(1, 'hour')}
          source="expectedEndTime"
          header="bookings.expectedEndTime"
        />
      </Col>
      {isEdit ? (
        <Col span={8}>
          <div>
            <label className="txtTitleForm">{i18next.t('bookings.checkinTime')}</label>
            <br />
            {getDailyStatus(record).timeStr}
          </div>
        </Col>
      ) : (
        <span />
      )}
      {/* {isEdit ? <Col span={6}>{getDailyStatus(record)}</Col> : <span />} */}
      <Col span={24}>
        <RestInputItem source="note" header="bookings.note" ContentComponent={Input.TextArea} />
        <RestInputContext.Consumer>
          {({ record, form }) => <TotalInput {...props} {...{ record, form }} />}
        </RestInputContext.Consumer>
        <div {...props} className="isPaidView">
          {record && record.isPaid ? <Badge status="success" /> : <span />}
        </div>
      </Col>
    </Row>
  );
};

BookingsForm.propTypes = {
  form: PropTypes.object,
  packageTypes: PropTypes.array,
  rooms: PropTypes.array,
  record: PropTypes.object,
  isEdit: PropTypes.bool,
};
BookingsForm.defaultProps = {
  isEdit: false,
};
function mapSateToProps(state) {
  return {
    rooms: getRooms(state),
    packageTypes: getConfigByName(state, 'packageTypes'),
  };
}

const BookingFormConnect = props => (
  <RestInputContext.Consumer>
    {({ record, form }) => <BookingsForm {...props} {...{ record, form }} />}
  </RestInputContext.Consumer>
);

const mapDispatchToProps = () => ({});

export default connect(
  mapSateToProps,
  mapDispatchToProps
)(BookingFormConnect);
