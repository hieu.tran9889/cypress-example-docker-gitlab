import styled from 'styled-components';

export const ReservationsFormWrapper = styled.div`
  .ant-select-selection__clear {
    background-color: transparent;
    color: white;
    border-radius: 5px;
  }
`;

export const TotalInputWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  .textUnit {
    margin-left: 5px;
  }
  .iconEdit {
    margin-left: 10px;
  }
  .input {
    width: 50%;
    margin-left: 5px;
    margin-right: 5px;
    .ant-form-item {
      margin-bottom: 0px;
    }
  }
  .paidView {
    padding: 2px 8px 2px 8px;
    border-radius: 20px;
  }
`;

export const FooterButtonGroupWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  .viewLeft {
    flex: 1;
  }
  .ant-btn {
    width: 150px;
    margin-left: 5px;
    border: none;
    &:hover,
    &:focus,
    &:active {
      border: none;
    }
  }
`;
