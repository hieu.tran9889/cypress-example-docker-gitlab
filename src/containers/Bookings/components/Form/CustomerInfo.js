import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18next from 'i18next';
import ReferenceInput from '../../../rest/ReferenceInput';
import RestAutoComplete from '../../../../components/RestInput/RestAutoComplete';
import crudSelectors from '../../../../redux/crudSelectors';
import RestHiddenInput from '../../../../components/RestInput/RestHiddenInput';

const CustomerInfo = ({ isEdit, form, users }) => {
  const onSelectUser = value => {
    const user = users.find(data => `${data.name}` === `${value}` || data.email === `${value}`);
    if (!user) return;
    form.setFieldsValue({
      email: user.email,
      customerName: user.name,
      userId: user.id,
    });
  };
  const checkUserSelected = prop => value => {
    const values = form.getFieldsValue();
    if (values.userId && values[prop] !== value) {
      form.setFieldsValue({ userId: undefined });
    }
    return value;
  };
  return (
    <div>
      <ReferenceInput searchKey="name" source="customerName" mappedBy="name" resource="users">
        <RestAutoComplete
          required
          formOptions={{
            getValueFromEvent: checkUserSelected('customerName'),
          }}
          titleProp="name"
          valueProp="name"
          source="customerName"
          header="bookings.customerName"
          material
          rules={[
            {
              pattern: new RegExp('\\w+'),
              message: i18next.t('error.customerName'),
            },
          ]}
          onSelect={onSelectUser}
          disabled={isEdit}
        />
      </ReferenceInput>
      <ReferenceInput source="email" mappedBy="email" resource="users">
        <RestAutoComplete
          required
          formOptions={{
            getValueFromEvent: checkUserSelected('email'),
          }}
          titleProp="email"
          valueProp="email"
          source="email"
          header="bookings.email"
          material
          rules={[
            {
              type: 'email',
              message: i18next.t('error.email'),
            },
          ]}
          onSelect={onSelectUser}
          disabled={isEdit}
        />
      </ReferenceInput>
      <RestHiddenInput ruleType="number" required={false} source="userId" />
    </div>
  );
};

CustomerInfo.propTypes = {
  form: PropTypes.object,
  users: PropTypes.array,
  isEdit: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    users: crudSelectors.users.getDataArr(state),
  };
}

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerInfo);
