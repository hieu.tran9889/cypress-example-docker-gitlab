import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getPackagesGroupByRoom, getRooms } from '../../../../redux/config/selectors';
import RestSelect from '../../../../components/RestInput/RestSelect';
import { getRecordData } from '../../../../utils/tools';

const PackageSelection = ({ rooms, form, formOptions, packagesGroupByRoom, record }) => {
  useEffect(() => {
    const selectedRoom = form.getFieldValue('roomId');
    if (!selectedRoom && rooms.length > 0) {
      form.setFieldsValue({
        roomId: rooms[0].id,
        packageTypeId: packagesGroupByRoom[rooms[0].id][0].packageTypeId,
        numberOfCustomer: rooms[0].capacity.min,
      });
    }
  });

  const getValueFromEvent = key => value => {
    if (rooms.length > 0) {
      form.setFieldsValue({
        packageTypeId: packagesGroupByRoom[value][0].packageTypeId,
      });
      formOptions.getValueFromEvent(key)(value);
    }
    return value;
  };

  const getValueFromEventForPackageType = value => {
    formOptions.getValueFromEvent('packageTypeId')(value);
    return value;
  };

  return (
    <div>
      <RestSelect
        formOptions={{
          getValueFromEvent: getValueFromEvent('roomId'),
        }}
        resourceData={rooms}
        source="roomId"
        header="bookings.room"
        valueProp="id"
        titleProp="name"
        ruleType="number"
        allowClear={false}
      />
      <RestSelect
        resourceData={
          packagesGroupByRoom[form.getFieldValue('roomId') || getRecordData(record, 'roomId')] || []
        }
        formOptions={{
          getValueFromEvent: getValueFromEventForPackageType,
        }}
        valueProp="packageType.id"
        titleProp="packageType.name"
        source="packageTypeId"
        header="bookings.packageType"
        allowClear={false}
      />
    </div>
  );
};

PackageSelection.propTypes = {
  rooms: PropTypes.array,
  packagesGroupByRoom: PropTypes.object,
  form: PropTypes.object,
  formOptions: PropTypes.object,
  record: PropTypes.object,
};

const mapStateToProps = state => ({
  rooms: getRooms(state),
  packagesGroupByRoom: getPackagesGroupByRoom(state),
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PackageSelection);
