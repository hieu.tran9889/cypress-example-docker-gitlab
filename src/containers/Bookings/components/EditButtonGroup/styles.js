import styled from 'styled-components';

export const EditButtonGroupWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: 20px;
  .vLeft {
    flex: 1;
  }
  .ant-btn {
    flex: 1;
    margin-left: 10px;
    &:first-child {
      margin-right: 0px;
    }
    .modal-footer {
      text-align: center;
      margin-top: 20px;
    }
  }
`;
