import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Popconfirm } from 'antd';
import i18next from 'i18next';
import { payBooking as payBookingAction } from '../../../../redux/realtimeBookings/actions';
import StatusButton from '../StatusButton';
import { EditButtonGroupWrapper } from './styles';

class EditButtonGroup extends Component {
  getFormatedData = async () => {
    const data = await this.props.getData();
    return data;
  };

  payBooking = async () => {
    const { payBooking, record, onBack } = this.props;
    const data = await this.getFormatedData();
    payBooking(record.id, { totalAmount: data.totalAmount });
    onBack();
  };

  save = () => {
    this.props.handleSubmit();
  };

  render() {
    const { onBack, record } = this.props;

    return (
      <EditButtonGroupWrapper>
        <div className="vLeft">
          <Button size="large" onClick={onBack} className="btnCancel">
            {i18next.t('button.cancel')}
          </Button>
        </div>
        <StatusButton isShowDetail {...this.props} />
        {(!record || !record.isPaid) && (
          <Popconfirm
            placement="topRight"
            title={i18next.t('bookings.confirmPay')}
            onConfirm={this.payBooking}
            okText={i18next.t('button.ok')}
            cancelText={i18next.t('button.cancel')}
          >
            <Button size="large" type="primary">
              {i18next.t('button.payBooking')}
            </Button>
          </Popconfirm>
        )}
        <Button size="large" type="primary" onClick={this.save}>
          {i18next.t('button.save')}
        </Button>
      </EditButtonGroupWrapper>
    );
  }
}
EditButtonGroup.propTypes = {
  onBack: PropTypes.func,
  record: PropTypes.object,
  handleSubmit: PropTypes.func,
};

function mapStateToProps() {
  return {};
}

const mapDispatchToProps = dispatch => ({
  payBooking: (id, data) => dispatch(payBookingAction(id, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditButtonGroup);
