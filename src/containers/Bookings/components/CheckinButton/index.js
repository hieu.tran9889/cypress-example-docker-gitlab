import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip, Icon } from 'antd';
import { getDailyStatus } from '../../../../utils/textUtils';
import { CheckinButtonWrapper } from './styles';

const CheckinButton = ({ loading, isShowInList, record, checkin, checkout }) => {
  const { checkins } = record;
  const dailyData = getDailyStatus(record);
  if (isShowInList) {
    return null;
  }

  const content = (
    <CheckinButtonWrapper
      role="presentation"
      loading={loading}
      onClick={() => {
        if (dailyData.type === 'COMPLETE') {
          return;
        }
        checkins && checkins[0] ? checkout(record) : checkin(record);
      }}
      className={`${dailyData.className}`}
      disabled={dailyData.type === 'COMPLETE'}
    >
      <Icon className="btnContent" type={dailyData.icon} />
    </CheckinButtonWrapper>
  );

  return (
    <Tooltip placement="top" title={dailyData.text}>
      {content}
    </Tooltip>
  );
};

CheckinButton.propTypes = {
  loading: PropTypes.bool,
  isShowInList: PropTypes.bool,
  record: PropTypes.object,
  checkin: PropTypes.func,
  checkout: PropTypes.func,
};

export default CheckinButton;
