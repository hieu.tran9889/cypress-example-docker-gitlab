import styled from 'styled-components';

export const CheckinButtonWrapper = styled.div`
  &.buttons {
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }
  &.btn {
    width: 100px;
    height: 30px;
    border-radius: 15px;
    background: transparent;
    border: 1px solid ${({ theme }) => theme.palette.primary};
    cursor: pointer;
    &:hover {
      background: ${({ theme }) => theme.palette.primary};
      border: 1px solid ${({ theme }) => theme.palette.primary};
    }
  }
  .btnContent {
    width: 40px;
    height: 30px;
    padding: 0 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 24px;
    cursor: pointer;
  }
  &.btnCheckin {
    color: ${({ theme }) => theme.color.blue};
  }

  &.btnCheckout {
    color: ${({ theme }) => theme.color.pink};
  }
  &.btnDisable {
    background: transparent;
    color: ${({ theme }) => theme.color.gray};
    border: none;
    cursor: no-drop;
    &:hover {
      background: transparent;
      border: none;
    }
  }
  &.buttonDetail {
    margin-left: 5px;
    text-transform: none;
    ${'' /* border: 1px solid  ${({ theme }) => theme.color.gray}; */}
  }
`;
