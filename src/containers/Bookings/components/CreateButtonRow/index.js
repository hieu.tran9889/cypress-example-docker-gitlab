import React from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';
import i18next from 'i18next';

const ButtonCreate = props => {
  const { getData, onSubmit } = props;
  const onCreate = () => {
    getData().then(results => {
      onSubmit(results);
    });
  };
  return (
    <div className="buttonRow">
      <Button type="primary" onClick={onCreate}>
        {i18next.t('button.create')}
      </Button>
    </div>
  );
};

ButtonCreate.propTypes = {
  getData: PropTypes.func,
  onSubmit: PropTypes.func,
};

export default ButtonCreate;
