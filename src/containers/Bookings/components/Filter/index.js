import React from 'react';
// import PropTypes from 'prop-types';
import i18next from 'i18next';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="customerName" placeholder="bookings.customerName" />
    <RestInputItem
      rules={[
        {
          type: 'email',
          message: i18next.t('error.email'),
        },
      ]}
      source="email"
      placeholder="bookings.email"
    />
    <RestInputItem source="roomId" placeholder="bookings.room" />
    <RestInputItem source="packageId" placeholder="bookings.package" />
    <RestInputItem source="packageTypeId" placeholder="bookings.packageType" />
    <RestInputItem source="discountValue" placeholder="bookings.discount" />
    <RestInputItem source="discountUnit" placeholder="bookings.discountUnit" />
    <RestInputItem source="startTime" placeholder="bookings.startTime" />
    <RestInputItem source="expectedEndTime" placeholder="bookings.expectedEndTime" />
    <RestInputItem source="note" placeholder="bookings.note" />
    <RestInputItem source="totalAmount" placeholder="bookings.totalAmount" />
    <RestInputItem source="status" placeholder="bookings.status" />
    <RestInputItem source="numberOfCustomer" placeholder="bookings.numberOfCustomer" />
  </div>
);

Filter.propTypes = {};

export default Filter;
