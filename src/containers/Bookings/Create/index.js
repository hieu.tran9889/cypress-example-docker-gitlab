import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form';

const BookingsCreate = props => (
  <Create {...props} resource="bookings">
    <Form />
  </Create>
);

BookingsCreate.propTypes = {};

export default BookingsCreate;
