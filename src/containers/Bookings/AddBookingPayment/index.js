import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, InputNumber, Input } from 'antd';
import i18next from 'i18next';
import RestCreate from '../../rest/Create';
import RestHiddenInput from '../../../components/RestInput/RestHiddenInput';
import ButtonCreate from '../components/CreateButtonRow';
import RestSelect from '../../../components/RestInput/RestSelect';
import RestInputItem from '../../../components/RestInput/RestInputItem';
import ReferenceInput from '../../rest/ReferenceInput';
import { TRANSACTION_TYPE, PAYMENT_METHOD } from '../../../configs/localData/index';

const DefaultPaymentType = props => {
  const defaultPaymentType = props.resourceData
    ? props.resourceData.find(data => data.default)
    : null;
  return defaultPaymentType ? (
    <RestHiddenInput
      form={props.form}
      ruleType="number"
      source="transactionTypeId"
      defaultValue={defaultPaymentType.id}
    />
  ) : (
    <span />
  );
};

const CreateServices = props => (
  <RestCreate
    {...props}
    noCardWrapper
    header={null}
    resource="transactions"
    customSubmitButton={<ButtonCreate onSubmit={props.onSubmit} />}
  >
    <Row gutter={16}>
      <Col span={12}>
        <RestSelect
          ruleType="string"
          resourceData={PAYMENT_METHOD.map(e => ({ ...e, text: i18next.t(e.text) }))}
          valueProp="value"
          titleProp="text"
          source="payType"
          defaultValue="cash"
          header="transactions.payType"
        />
        <RestSelect
          ruleType="string"
          resourceData={TRANSACTION_TYPE.map(e => ({ ...e, text: i18next.t(e.text) }))}
          valueProp="value"
          titleProp="text"
          source="category"
          defaultValue="INCOME"
          header="transactions.category"
        />
      </Col>
      <Col span={12}>
        <RestInputItem
          ContentComponent={InputNumber}
          ruleType="number"
          source="amount"
          header="transactions.amount"
        />
        <RestInputItem isReference>
          <ReferenceInput
            initialFilter={{
              limit: 50,
              offset: 0,
              filter: { category: props.type, isActive: true },
            }}
            source="transactionTypeId"
            resource="transactionTypes"
          >
            <RestSelect
              valueProp="id"
              titleProp={`displayName.${i18next.language}`}
              source="transactionTypeId"
              header="transactionTypes.title"
            />
          </ReferenceInput>
        </RestInputItem>
      </Col>
      <Col span={24}>
        <RestInputItem
          ContentComponent={Input.TextArea}
          source="description"
          header="transactions.description"
        />
      </Col>
    </Row>
  </RestCreate>
);

DefaultPaymentType.propTypes = {
  form: PropTypes.object,
  resourceData: PropTypes.array,
};

CreateServices.propTypes = {
  bookingId: PropTypes.string,
  onSubmit: PropTypes.func,
  type: PropTypes.string,
};

export default CreateServices;
