import React, { Component } from 'react';
import moment from 'moment';
import i18next from 'i18next';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, DatePicker, TimePicker } from 'antd';
import { replace } from 'connected-react-router';
import { checkinBooking } from '../../../redux/realtimeBookings/actions';
import InfoRow from '../components/InfoRow';
import Text from '../../../components/common/Text';
import { formatMoney, formatDisCount } from '../../../utils/textUtils';
import { CheckinModalWrapper } from './styles';
import { currentBooking } from '../../../redux/realtimeBookings/selectors';

class CheckinModal extends Component {
  constructor(props) {
    super(props);
    this.checkinTime = moment();
  }

  onChangeDate = date => {
    this.checkinTime.set('day', date.get('day'));
    this.checkinTime.set('month', date.get('month'));
    this.checkinTime.set('year', date.get('year'));
  };

  onChangeTime = time => {
    this.checkinTime.set('hours', time.get('hours'));
    this.checkinTime.set('minutes', time.get('minutes'));
  };

  checkin = () => {
    const { booking, checkin } = this.props;
    checkin(booking, {
      startTime: this.checkinTime.toISOString(),
    });
  };

  render() {
    const { booking, closeModal } = this.props;
    const { customerName, numberOfCustomer, discountUnit, discountValue } = booking;

    const CHECKIN_DATA = [
      {
        title: 'bookings.customerName',
        value: customerName,
      },
      {
        title: 'bookings.package',
        value: booking.package ? booking.package.name : '',
      },
      {
        title: 'bookings.packageAmount',
        value: `${formatMoney(booking.package ? booking.package.price : 0)} VND`,
      },
      {
        title: 'bookings.numberOfCustomer',
        value: numberOfCustomer,
      },
      {
        title: 'bookings.discount',
        value: formatDisCount(discountUnit, discountValue),
      },
      {
        title: 'bookings.checkinDate',
        value: (
          <DatePicker allowClear={false} defaultValue={moment()} onChange={this.onChangeDate} />
        ),
      },
      {
        title: 'bookings.startTime',
        value: (
          <TimePicker
            allowEmpty={false}
            format="HH:mm"
            defaultValue={moment()}
            onChange={this.onChangeTime}
          />
        ),
      },
    ];

    const footer = (
      <div className="modal-footer">
        <Button size="large" onClick={closeModal}>
          {i18next.t('button.cancel')}
        </Button>
        <Button size="large" type="primary" onClick={this.checkin}>
          {i18next.t('button.checkin')}
        </Button>
      </div>
    );

    return (
      <CheckinModalWrapper>
        <Text type="h4White" align="center" className="modalTitle">
          {i18next.t('bookings.checkin')}
        </Text>
        {CHECKIN_DATA.map(data => (
          <InfoRow key={data.title} {...data} />
        ))}
        {footer}
      </CheckinModalWrapper>
    );
  }
}

CheckinModal.propTypes = {
  checkin: PropTypes.func,
  booking: PropTypes.object,
  closeModal: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    booking: currentBooking(state),
  };
}

const mapDispatchToProps = (dispatch, props) => ({
  checkin: (booking, payload) => dispatch(checkinBooking(booking, payload)),
  closeModal: () => dispatch(replace(props.location.pathname)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckinModal);
