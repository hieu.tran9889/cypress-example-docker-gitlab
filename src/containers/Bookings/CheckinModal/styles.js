import styled from 'styled-components';

export const CheckinModalWrapper = styled.div`
  .ant-calendar-picker,
  .ant-time-picker {
    width: 100%;
    height: 40px;
    border: 1px solid #0f1125;
    border-radius: 2px;
    background: ${({ theme }) => theme.border.default};
    &:hover {
      border: 1px solid ${({ theme }) => theme.palette.primary};
    }
    .anticon {
      color: ${({ theme }) => theme.palette.primary};
    }
    .ant-calendar-picker-input,
    .ant-input,
    .ant-time-picker-input {
      background: transparent;
      border-bottom: none !important;
      padding-left: 10px;
      height: 40px;
      color: ${({ theme }) => theme.palette.primary};
      &:hover {
        border: none;
      }
    }
  }
  .ant-modal-body {
    padding: 0px;
  }
  .modal-footer {
    text-align: center;
    align-items: center;
    margin-top: 20px;
    display: flex;
    .ant-btn {
      flex: 1;
      &:first-child {
        margin-right: 20px;
      }
    }
  }
`;
