import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Table, Tabs } from 'antd';
import Column from 'antd/lib/table/Column';
import i18next from 'i18next';
import { push } from 'connected-react-router';
import {
  checkinBooking,
  getPreCheckout as getPreCheckoutAction,
  setBookingFilter as setBookingFilterAction,
} from '../../../redux/realtimeBookings/actions';
import { getCurrentTabBookings } from '../../../redux/realtimeBookings/selectors';
import { formatMoney, getDailyStatus } from '../../../utils/textUtils';
import { BOOKING_STATUS, CHECKIN_STATUS, PAYMENT_STATUS } from '../../../configs/localData';
import CheckinButton from '../components/CheckinButton/index';
import { getRecordData } from '../../../utils/tools';
import UserInfo from '../../../components/RestField/UserInfo/index';
import StatusButton from '../components/StatusButton';

const ListBookingWithSocket = ({
  currentTabBookings,
  setBookingFilter,
  getPreCheckout,
  pushRoute,
  location,
  rooms,
  packageTypes,
}) => {
  const onChangeCheckinType = value => {
    setBookingFilter({
      checkinType: value,
    });
  };

  const showBooking = data => {
    pushRoute(`#bookings/${data.id}/edit`);
    getPreCheckout(data.id);
  };

  const checkout = data => {
    pushRoute(`#bookings/checkout/${data.id}`);
    getPreCheckout(data.id);
  };

  const checkin = data => {
    pushRoute(`#bookings/checkin/${data.id}`);
    getPreCheckout(data.id);
  };

  useEffect(() => {
    const id = location.hash.replace('#bookings/checkout/', '').replace('#bookings/checkin/', '');
    id && getPreCheckout(id);
    onChangeCheckinType(CHECKIN_STATUS[0].value);
  }, []);

  return (
    <div>
      <Tabs onChange={onChangeCheckinType}>
        {CHECKIN_STATUS.map(e => (
          <Tabs.TabPane key={e.value} tab={i18next.t(e.text)} />
        ))}
      </Tabs>
      <Table
        pagination={{
          pageSize: 20,
        }}
        rowKey="id"
        dataSource={currentTabBookings}
      >
        <Column
          render={(data, record) => (
            <UserInfo
              record={{ email: record.email, name: record.customerName, id: record.userId }}
              prefixLink="customers"
              source="name"
              roleProp="email"
              header="customers.name"
            />
          )}
          title={i18next.t('bookings.customer')}
          dataIndex="customerName"
          key="customerName"
        />
        <Column
          filters={rooms?.ids?.map(id => ({
            value: id,
            text: rooms?.data[id].displayName[i18next.language],
          }))}
          onFilter={(value, record) => record.roomId === value}
          title={i18next.t('bookings.room')}
          render={roomId => rooms?.data[roomId]?.displayName[i18next.language]}
          dataIndex="roomId"
          key="roomId"
        />
        <Column
          filters={packageTypes?.ids?.map(id => ({
            value: id,
            text: packageTypes?.data[id].displayName[i18next.language],
          }))}
          onFilter={(value, record) => record.packageTypeId === value}
          title={i18next.t('bookings.packageType')}
          render={packageTypeId => packageTypes?.data[packageTypeId]?.displayName[i18next.language]}
          dataIndex="packageTypeId"
          key="packageTypeId"
        />
        <Column
          sorter={(a, b) =>
            Number(getRecordData(a, 'totalAmount')) - Number(getRecordData(b, 'totalAmount'))
          }
          render={data => formatMoney(data)}
          title={i18next.t('bookings.totalAmount')}
          dataIndex="totalAmount"
          key="totalAmount"
        />
        <Column
          filters={PAYMENT_STATUS.map(data => ({
            value: data.value,
            text: i18next.t(data.text),
          }))}
          onFilter={(value, record) => `${getRecordData(record, 'isPaid')}` === value}
          render={data => (data ? i18next.t('bookings.paid') : i18next.t('bookings.unpaid'))}
          title={i18next.t('bookings.isPaid')}
          dataIndex="isPaid"
          key="isPaid"
        />
        <Column
          filters={BOOKING_STATUS.map(data => ({
            value: data.value,
            text: i18next.t(data.text),
          }))}
          onFilter={(value, record) => getRecordData(record, 'status') === value}
          render={data => i18next.t(BOOKING_STATUS.find(e => e.value === data).text)}
          title={i18next.t('bookings.status')}
          dataIndex="status"
          key="status"
        />
        <Column
          render={(data, record) => getDailyStatus(record).timeStr}
          title={i18next.t('bookings.checkinTime')}
          dataIndex="checkins.0.startTime"
          key="checkins.0.startTime"
        />
        <Column
          render={(data, record) => (
            <div style={{ display: 'flex' }}>
              <CheckinButton checkout={checkout} checkin={checkin} record={record} />
              <StatusButton record={record} showBooking={showBooking} />
            </div>
          )}
          dataIndex="id"
          key="action"
        />
      </Table>
    </div>
  );
};
ListBookingWithSocket.propTypes = {
  currentTabBookings: PropTypes.array,
  setBookingFilter: PropTypes.func,
  getPreCheckout: PropTypes.func,
  pushRoute: PropTypes.func,
  location: PropTypes.object,
  rooms: PropTypes.object,
  packageTypes: PropTypes.object,
};

ListBookingWithSocket.defaultProps = {};

function mapStateToProps(state) {
  return {
    currentTabBookings: getCurrentTabBookings(state),
    packageTypes: state.config.packageTypes,
    rooms: state.config.rooms,
  };
}

const mapDispatchToProps = dispatch => ({
  checkin: data => dispatch(checkinBooking(data)),
  getPreCheckout: id => dispatch(getPreCheckoutAction(id)),
  setBookingFilter: data => dispatch(setBookingFilterAction(data)),
  pushRoute: data => dispatch(push(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListBookingWithSocket);
