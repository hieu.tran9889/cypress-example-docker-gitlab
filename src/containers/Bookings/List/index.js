import React from 'react';
import i18next from 'i18next';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';
import { formatDateTime, formatMoney } from '../../../utils/textUtils';
import { DISCOUNT_UNIT, BOOKING_STATUS } from '../../../configs/localData';
import EnumField from '../../../components/RestField/EnumField';
import { dateFilterDropdown } from '../../../components/RestInput/RestDateTimePicker';
import UserInfo from '../../../components/RestField/UserInfo/index';

const BookingsList = ({ isCustomerPage, ...props }) => (
  <List {...props} resource="bookings">
    <RestFieldItem
      header="checkins.bookingId"
      format={data => (
        <Link href={`#bookings/${data}/edit`} to={`#bookings/${data}/edit`}>
          #{data}
        </Link>
      )}
      source="id"
      width={100}
    />
    {!isCustomerPage && (
      <RestFieldItem
        format={(data, record) => (
          <UserInfo
            record={{ email: record.email, name: record.customerName, id: record.userId }}
            prefixLink="customers"
            source="name"
            roleProp="email"
            header="customers.name"
          />
        )}
        hasSearch
        source="customerName"
        header="bookings.customerName"
      />
    )}
    <RestFieldItem width={150} header="bookings.package" source="package.name" />
    <EnumField
      resourceData={DISCOUNT_UNIT}
      format={(data, record) => `${record.discountValue || '0'}${data}`}
      source="discountUnit"
      header="bookings.discount"
    />
    <RestFieldItem
      format={(data, record) =>
        `${formatDateTime(record.startTime)} - ${formatDateTime(record.expectedEndTime)}`
      }
      filterDropdown={dateFilterDropdown}
      source="startTime"
      header="bookings.startTime"
    />
    <RestFieldItem
      sorter
      format={data => formatMoney(data)}
      source="totalAmount"
      header="bookings.totalAmount"
    />
    <EnumField
      filters={BOOKING_STATUS.map(e => ({ ...e, text: i18next.t(e.text) }))}
      titleProp="text"
      resourceData={BOOKING_STATUS}
      source="status"
      header="bookings.status"
      width={130}
    />
    <RestFieldItem source="numberOfCustomer" header="bookings.numberOfCustomer" />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

BookingsList.propTypes = {
  isCustomerPage: PropTypes.bool,
};

export default BookingsList;
