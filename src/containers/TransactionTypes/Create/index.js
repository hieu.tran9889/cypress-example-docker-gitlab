import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form';

const TransactionTypesCreate = props => (
  <Create {...props} resource="transactionTypes">
    <Form />
  </Create>
);

TransactionTypesCreate.propTypes = {};

export default TransactionTypesCreate;
