import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const TransactionTypesShow = props => (
  <RestShow {...props} hasEdit resource="transactionTypes">
    <RestFieldItem source="name" header="transactionTypes.name" />
    <RestFieldItem source="category" header="transactionTypes.category" />
    <RestFieldItem source="displayName.en" header="transactionTypes.displayName.en" />
    <RestFieldItem source="displayName.vi" header="transactionTypes.displayName.vi" />
    <RestFieldItem source="isActive" header="transactionTypes.isActive" />
    <RestFieldItem source="default" header="transactionTypes.default" />
  </RestShow>
);

export default TransactionTypesShow;
