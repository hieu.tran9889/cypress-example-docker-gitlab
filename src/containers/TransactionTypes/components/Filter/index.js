import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="name" placeholder="transactionTypes.name" />
    <RestInputItem source="category" placeholder="transactionTypes.category" />
    <RestInputItem source="displayName.en" placeholder="transactionTypes.displayName.en" />
    <RestInputItem source="displayName.vi" placeholder="transactionTypes.displayName.vi" />
    <RestInputItem source="isActive" placeholder="transactionTypes.isActive" />
    <RestInputItem source="default" placeholder="transactionTypes.default" />
  </div>
);

Filter.propTypes = {};

export default Filter;
