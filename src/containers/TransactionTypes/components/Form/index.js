import React from 'react';
import { Row, Col, Switch } from 'antd';
import i18next from 'i18next';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import RestSelect from '../../../../components/RestInput/RestSelect';
import { TRANSACTION_TYPE } from '../../../../configs/localData';

const TransactionTypesForm = () => (
  <Row gutter={16}>
    <Col span={12}>
      <RestInputItem source="name" header="transactionTypes.name" />
      <RestInputItem source="displayName.en" header="transactionTypes.displayName.en" />
      <RestInputItem
        ruleType="boolean"
        ContentComponent={Switch}
        source="isActive"
        header="transactionTypes.isActive"
      />
    </Col>
    <Col span={12}>
      <RestSelect
        ruleType="string"
        resourceData={TRANSACTION_TYPE.map(e => ({ ...e, text: i18next.t(e.text) }))}
        valueProp="value"
        titleProp="text"
        source="category"
        header="transactionTypes.category"
      />
      <RestInputItem source="displayName.vi" header="transactionTypes.displayName.vi" />
    </Col>
  </Row>
);

TransactionTypesForm.propTypes = {};

export default TransactionTypesForm;
