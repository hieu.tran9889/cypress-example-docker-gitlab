import React from 'react';
import { Switch } from 'antd';
import i18next from 'i18next';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';
import { TRANSACTION_TYPE, ACTIVE_TYPES } from '../../../configs/localData';

const TransactionTypesList = props => (
  <List {...props} resource="transactionTypes">
    <RestFieldItem hasSearch source="name" header="transactionTypes.name" />
    <RestFieldItem
      filters={TRANSACTION_TYPE.map(e => ({ ...e, text: i18next.t(e.text) }))}
      resourceData={TRANSACTION_TYPE}
      source="category"
      header="transactionTypes.category"
    />
    <RestFieldItem source="displayName.en" header="transactionTypes.displayName.en" />
    <RestFieldItem source="displayName.vi" header="transactionTypes.displayName.vi" />
    <RestFieldItem
      filters={ACTIVE_TYPES.map(e => ({ ...e, text: i18next.t(e.text) }))}
      valueProp="checked"
      component={<Switch />}
      source="isActive"
      header="transactionTypes.isActive"
    />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

TransactionTypesList.propTypes = {};

export default TransactionTypesList;
