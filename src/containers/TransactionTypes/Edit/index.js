import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form';

const TransactionTypesEdit = props => (
  <Edit {...props} resource="transactionTypes">
    <Form />
  </Edit>
);

TransactionTypesEdit.propTypes = {};

export default TransactionTypesEdit;
