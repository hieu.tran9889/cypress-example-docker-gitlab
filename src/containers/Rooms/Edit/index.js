import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form';

const RoomsEdit = props => (
  <Edit {...props} resource="rooms">
    <Form />
  </Edit>
);

RoomsEdit.propTypes = {};

export default RoomsEdit;
