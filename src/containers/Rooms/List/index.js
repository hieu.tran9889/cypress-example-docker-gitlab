import React from 'react';
import { Switch, Icon } from 'antd';
import i18next from 'i18next';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';
import GridPhotos from '../../../components/common/GridPhotos';
import { ACTIVE_TYPES } from '../../../configs/localData';

const RoomsList = props => (
  <List {...props} resource="rooms">
    <RestFieldItem hasSearch source="name" header="rooms.name" />
    <RestFieldItem source="displayName.en" header="rooms.displayName.en" />
    <RestFieldItem source="displayName.vi" header="rooms.displayName.vi" />
    <RestFieldItem
      sorter
      format={(data, record) => (
        <span>
          {`${record.capacity.min} - ${record.capacity.max}  `}
          <Icon type="team" style={{ fontSize: 17 }} />
        </span>
      )}
      source="capacity.max"
      header="rooms.capacity.title"
    />
    <RestFieldItem
      format={(data, record) => <GridPhotos images={record.images} />}
      source="images"
      header="rooms.images"
    />
    <RestFieldItem
      filters={ACTIVE_TYPES.map(e => ({ ...e, text: i18next.t(e.text) }))}
      valueProp="defaultChecked"
      component={<Switch />}
      source="isActive"
      header="rooms.isActive"
    />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

RoomsList.propTypes = {};

export default RoomsList;
