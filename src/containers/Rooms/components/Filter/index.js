import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="image" placeholder="rooms.image" />
    <RestInputItem source="name" placeholder="rooms.name" />
    <RestInputItem source="displayName.en" placeholder="rooms.displayName.en" />
    <RestInputItem source="displayName.vi" placeholder="rooms.displayName.vi" />
    <RestInputItem source="capacity.min" placeholder="rooms.capacity.min" />
    <RestInputItem source="capacity.max" placeholder="rooms.capacity.max" />
    <RestInputItem source="locationId" placeholder="rooms.locationId" />
    <RestInputItem source="description" placeholder="rooms.description" />
    <RestInputItem source="facilities" placeholder="rooms.facilities" />
    <RestInputItem source="images" placeholder="rooms.images" />
    <RestInputItem source="squareByMeter" placeholder="rooms.squareByMeter" />
    <RestInputItem source="isActive" placeholder="rooms.isActive" />
  </div>
);

Filter.propTypes = {};

export default Filter;
