import React from 'react';
import { Col, Row, InputNumber, Switch, Input } from 'antd';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import RestMultiPhotos from '../../../../components/RestInput/RestMultiPhotos/index';

const RoomsForm = () => (
  <Row type="flex" gutter={16}>
    <Col span={12}>
      <Row gutter={16}>
        <Col span={12}>
          <RestInputItem source="name" header="rooms.name" />
          <RestInputItem source="displayName.en" header="rooms.displayName.en" />
          <RestInputItem
            ruleType="number"
            ContentComponent={InputNumber}
            source="capacity.min"
            header="rooms.capacity.min"
          />
        </Col>
        <Col span={12}>
          <RestInputItem
            ruleType="boolean"
            ContentComponent={Switch}
            source="isActive"
            header="rooms.isActive"
          />
          <RestInputItem source="displayName.vi" header="rooms.displayName.vi" />
          <RestInputItem
            ruleType="number"
            ContentComponent={InputNumber}
            source="capacity.max"
            header="rooms.capacity.max"
          />
        </Col>
        <Col span={24}>
          <RestInputItem
            ContentComponent={Input.TextArea}
            source="description"
            header="rooms.description"
          />
        </Col>
      </Row>
    </Col>
    <Col span={12}>
      <RestMultiPhotos defaultSourceKey="image" source="images" placeholder="rooms.images" />
    </Col>
  </Row>
);

RoomsForm.propTypes = {};

export default RoomsForm;
