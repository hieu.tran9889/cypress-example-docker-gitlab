import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const RoomsShow = props => (
  <RestShow {...props} hasEdit resource="rooms">
    <RestFieldItem source="image" header="rooms.image" />
    <RestFieldItem source="name" header="rooms.name" />
    <RestFieldItem source="displayName.en" header="rooms.displayName.en" />
    <RestFieldItem source="displayName.vi" header="rooms.displayName.vi" />
    <RestFieldItem source="capacity.min" header="rooms.capacity.min" />
    <RestFieldItem source="capacity.max" header="rooms.capacity.max" />
    <RestFieldItem source="locationId" header="rooms.locationId" />
    <RestFieldItem source="description" header="rooms.description" />
    <RestFieldItem source="facilities" header="rooms.facilities" />
    <RestFieldItem source="images" header="rooms.images" />
    <RestFieldItem source="squareByMeter" header="rooms.squareByMeter" />
    <RestFieldItem source="isActive" header="rooms.isActive" />
  </RestShow>
);

export default RoomsShow;
