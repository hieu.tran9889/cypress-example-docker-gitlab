import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form';

const RoomsCreate = props => (
  <Create {...props} resource="rooms">
    <Form />
  </Create>
);

RoomsCreate.propTypes = {};

export default RoomsCreate;
