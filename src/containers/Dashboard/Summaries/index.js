import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';
import { Row, Col } from 'antd';
import SummaryCard from '../../../components/common/SummaryCard';
import { formatMoney } from '../../../utils/textUtils';

const Summaries = ({
  theme,
  totalBookingCount,
  totalIncome,
  totalExpense,
  numberOfCheckinedCustomer,
  numberOfCheckoutedCustomer,
}) => {
  const SUMMARIES = [
    {
      icon: 'team',
      header: 'home.summary.currentCheckin',
      value: numberOfCheckinedCustomer
        ? `${numberOfCheckoutedCustomer}/${numberOfCheckinedCustomer}`
        : '0/0',
      color: theme.color.violet,
    },
    {
      icon: 'ic-seat',
      header: 'home.summary.booking',
      value: totalBookingCount || 0,
      color: theme.color.blueShade,
    },
    {
      icon: 'ic-payment',
      header: 'home.summary.income',
      value: formatMoney(totalIncome) || 0,
      color: theme.color.lightGreen,
    },
    {
      icon: 'ic-payment',
      header: 'home.summary.expense',
      value: formatMoney(totalExpense) || 0,
      color: theme.color.green,
    },
  ];
  return (
    <Row gutter={24} type="flex">
      {SUMMARIES.map((data, index) => (
        <Col key={String(index)} lg={6} md={12} xs={24}>
          <SummaryCard {...data} />
        </Col>
      ))}
    </Row>
  );
};
Summaries.propTypes = {
  theme: PropTypes.object,
  totalBookingCount: PropTypes.number,
  totalIncome: PropTypes.number,
  totalExpense: PropTypes.number,
  numberOfCheckinedCustomer: PropTypes.number,
  numberOfCheckoutedCustomer: PropTypes.number,
};

export default connect(state => ({
  totalBookingCount: state.realtimeBookings.totalBookingCount,
  totalIncome: state.realtimeBookings.totalIncome,
  totalExpense: state.realtimeBookings.totalExpense,
  numberOfCheckinedCustomer: state.realtimeBookings.numberOfCheckinedCustomer,
  numberOfCheckoutedCustomer: state.realtimeBookings.numberOfCheckoutedCustomer,
}))(withTheme(Summaries));
