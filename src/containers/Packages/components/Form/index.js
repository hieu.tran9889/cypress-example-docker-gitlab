import React from 'react';
import { Row, Col, Input, InputNumber, Switch } from 'antd';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import RestSelect from '../../../../components/RestInput/RestSelect';
import ReferenceInput from '../../../rest/ReferenceInput';

const PackagesForm = () => (
  <div>
    <Row gutter={16}>
      <Col span={12}>
        <RestInputItem source="name" header="packages.name" />
        <RestInputItem source="displayName.en" header="packages.displayName.en" />
        <RestInputItem isReference>
          <ReferenceInput source="roomId" resource="rooms">
            <RestSelect valueProp="id" titleProp="name" source="roomId" header="packages.room" />
          </ReferenceInput>
        </RestInputItem>
        <RestInputItem
          ruleType="number"
          ContentComponent={InputNumber}
          source="price"
          header="packages.price"
        />
      </Col>
      <Col span={12}>
        <RestInputItem source="shortName" header="packages.shortName" />
        <RestInputItem source="displayName.vi" header="packages.displayName.vi" />
        <RestInputItem isReference>
          <ReferenceInput source="packageTypeId" resource="packageTypes">
            <RestSelect
              valueProp="id"
              titleProp="name"
              source="packageTypeId"
              header="packages.packageType"
            />
          </ReferenceInput>
        </RestInputItem>
        <RestInputItem source="locationId" header="packages.location" />
      </Col>
      <Col span={24}>
        <RestInputItem
          ruleType="boolean"
          ContentComponent={Switch}
          source="isActive"
          header="packageTypes.isActive"
        />
        <RestInputItem
          ContentComponent={Input.TextArea}
          source="description"
          header="packages.description"
        />
      </Col>
    </Row>
  </div>
);

PackagesForm.propTypes = {};

export default PackagesForm;
