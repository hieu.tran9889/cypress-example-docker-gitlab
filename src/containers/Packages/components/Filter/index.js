import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="shortName" placeholder="packages.shortName" />
    <RestInputItem source="name" placeholder="packages.name" />
    <RestInputItem source="roomId" placeholder="packages.room" />
    <RestInputItem source="packageTypeId" placeholder="packages.packageType" />
    <RestInputItem source="price" placeholder="packages.price" />
    <RestInputItem source="isDefault" placeholder="packages.isDefault" />
    <RestInputItem source="isHour" placeholder="packages.isHour" />
    <RestInputItem source="isPayByQuantity" placeholder="packages.isPayByQuantity" />
    <RestInputItem source="isPayWhenCheckout" placeholder="packages.isPayWhenCheckout" />
    <RestInputItem source="description" placeholder="packages.description" />
    <RestInputItem source="locationId" placeholder="packages.location" />
  </div>
);

Filter.propTypes = {};

export default Filter;
