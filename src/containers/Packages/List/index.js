import React from 'react';
import { Switch } from 'antd';
import i18next from 'i18next';
import { useSelector } from 'react-redux';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';
import { formatMoney } from '../../../utils/textUtils';
import { ACTIVE_TYPES } from '../../../configs/localData';

const PackagesList = props => {
  const packageTypes = useSelector(state => state.config.packageTypes);
  const rooms = useSelector(state => state.config.rooms);
  return (
    <List {...props} resource="packages">
      {/* <Reference source="locationId" header="packages.location" resource="locations">
      <RestFieldItem source="name" />
    </Reference> */}
      <RestFieldItem hasSearch source="shortName" header="packages.shortName" />
      <RestFieldItem hasSearch source="name" header="packages.name" />
      <RestFieldItem
        filters={rooms?.ids?.map(id => ({
          value: `${id}`,
          text: rooms?.data[id]?.displayName[i18next.language],
        }))}
        format={id => rooms?.data[id]?.displayName[i18next.language]}
        source="roomId"
        header="packages.room"
      />
      <RestFieldItem
        filters={packageTypes?.ids?.map(id => ({
          value: `${id}`,
          text: packageTypes?.data[id]?.displayName[i18next.language],
        }))}
        format={id => packageTypes?.data[id]?.displayName[i18next.language]}
        source="packageTypeId"
        header="packages.packageType"
      />
      <RestFieldItem
        sorter
        format={data => formatMoney(data)}
        source="price"
        header="packages.price"
      />
      <RestFieldItem
        filters={ACTIVE_TYPES.map(data => ({
          id: data.id,
          value: data.value,
          text: i18next.t(data.text),
        }))}
        valueProp="checked"
        component={<Switch />}
        source="isActive"
        header="packages.isActive"
      />
      <ActionGroup>
        <EditButton />
        <DeleteButton />
      </ActionGroup>
    </List>
  );
};

PackagesList.propTypes = {};

export default PackagesList;
