import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const PackagesShow = props => (
  <RestShow {...props} hasEdit resource="packages">
    <RestFieldItem source="shortName" header="packages.shortName" />
    <RestFieldItem source="name" header="packages.name" />
    <RestFieldItem source="roomId" header="packages.room" />
    <RestFieldItem source="packageTypeId" header="packages.packageType" />
    <RestFieldItem source="price" header="packages.price" />
    <RestFieldItem source="isDefault" header="packages.isDefault" />
    <RestFieldItem source="isHour" header="packages.isHour" />
    <RestFieldItem source="isPayByQuantity" header="packages.isPayByQuantity" />
    <RestFieldItem source="isPayWhenCheckout" header="packages.isPayWhenCheckout" />
    <RestFieldItem source="description" header="packages.description" />
    <RestFieldItem source="locationId" header="packages.location" />
  </RestShow>
);

export default PackagesShow;
