import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form';

const PackagesEdit = props => (
  <Edit {...props} resource="packages">
    <Form />
  </Edit>
);

PackagesEdit.propTypes = {};

export default PackagesEdit;
