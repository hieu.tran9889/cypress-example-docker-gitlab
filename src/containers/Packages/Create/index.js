import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form';

const PackagesCreate = props => (
  <Create {...props} resource="packages">
    <Form />
  </Create>
);

PackagesCreate.propTypes = {};

export default PackagesCreate;
