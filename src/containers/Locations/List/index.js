import React from 'react';
import List from "../../rest/List";
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';

const LocationsList = props => (
  <List {...props} resource="locations">
    <RestFieldItem source="name" header="locations.name" />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

LocationsList.propTypes = {};

export default LocationsList;
