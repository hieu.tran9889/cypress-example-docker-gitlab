import React from 'react';
import Edit from "../../rest/Edit";
import Form from '../components/Form';

const LocationsEdit = props => (
  <Edit {...props} resource="locations">
    <Form />
  </Edit>
);

LocationsEdit.propTypes = {};

export default LocationsEdit;
