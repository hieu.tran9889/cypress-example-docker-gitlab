import React from 'react';
import Create from "../../rest/Create";
import Form from '../components/Form';

const LocationsCreate = props => (
  <Create {...props} resource="locations">
    <Form />
  </Create>
);

LocationsCreate.propTypes = {};

export default LocationsCreate;
