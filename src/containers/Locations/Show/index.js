import React from 'react';
import RestShow from "../../rest/Show";
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const LocationsShow = props => (
  <RestShow {...props} hasEdit resource="locations">
    <RestFieldItem source="name" header="locations.name" />
  </RestShow>
);

export default LocationsShow;
