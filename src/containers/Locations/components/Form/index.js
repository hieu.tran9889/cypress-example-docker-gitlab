import React from 'react';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const LocationsForm = props => (
  <div {...props}>
    <RestInputItem source="name" header="locations.name" />
  </div>
);

LocationsForm.propTypes = {};

export default LocationsForm;
