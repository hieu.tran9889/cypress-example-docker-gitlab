import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form';

const PackageTypesCreate = props => (
  <Create {...props} resource="packageTypes">
    <Form />
  </Create>
);

PackageTypesCreate.propTypes = {};

export default PackageTypesCreate;
