import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const PackageTypesShow = props => (
  <RestShow {...props} hasEdit resource="packageTypes">
    <RestFieldItem source="name" header="packageTypes.name" />
    <RestFieldItem source="displayName.vi" header="packageTypes.displayName.vi" />
    <RestFieldItem source="isActive" header="packageTypes.isActive" />
  </RestShow>
);

export default PackageTypesShow;
