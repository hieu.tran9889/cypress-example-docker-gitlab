import React from 'react';
import { Switch } from 'antd';
import i18next from 'i18next';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';
import { ACTIVE_TYPES } from '../../../configs/localData'

const PackageTypesList = props => (
  <List {...props} resource="packageTypes">
    <RestFieldItem hasSearch source="name" header="packageTypes.name" />
    <RestFieldItem source="displayName.vi" header="packageTypes.displayName.vi" />
    <RestFieldItem
      filters={ACTIVE_TYPES.map(e => ({ ...e, text: i18next.t(e.text) }))}
      valueProp="checked"
      component={<Switch />}
      source="isActive"
      header="packageTypes.isActive"
    />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

PackageTypesList.propTypes = {};

export default PackageTypesList;
