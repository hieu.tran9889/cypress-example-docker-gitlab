import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="name" placeholder="packageTypes.name" />
    <RestInputItem source="displayName.vi" placeholder="packageTypes.displayName.vi" />
    <RestInputItem source="isActive" placeholder="packageTypes.isActive" />
  </div>
);

Filter.propTypes = {};

export default Filter;
