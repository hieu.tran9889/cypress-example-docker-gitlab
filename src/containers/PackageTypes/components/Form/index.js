import React from 'react';
import { Switch, Row, Col, InputNumber } from 'antd';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const PackageTypesForm = () => (
  <Row type="flex" gutter={16}>
    <Col span={12}>
      <RestInputItem source="name" header="packageTypes.name" />
      <RestInputItem source="displayName.en" header="packageTypes.displayName.en" />
      <RestInputItem
        ruleType="boolean"
        ContentComponent={Switch}
        source="isActive"
        header="packageTypes.isActive"
      />
    </Col>
    <Col span={12}>
      <RestInputItem
        ruleType="number"
        ContentComponent={InputNumber}
        source="order"
        header="packageTypes.order"
      />
      <RestInputItem source="displayName.vi" header="packageTypes.displayName.vi" />
    </Col>
  </Row>
);

PackageTypesForm.propTypes = {};

export default PackageTypesForm;
