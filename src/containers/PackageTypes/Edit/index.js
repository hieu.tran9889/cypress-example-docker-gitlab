import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form';

const PackageTypesEdit = props => (
  <Edit {...props} resource="packageTypes">
    <Form />
  </Edit>
);

PackageTypesEdit.propTypes = {};

export default PackageTypesEdit;
