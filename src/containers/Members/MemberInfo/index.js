import React from 'react';
import { Card } from 'antd';
import i18next from 'i18next';
import SaveButton from '../../../components/RestActions/SaveButton';
import RestInputItem from '../../../components/RestInput/RestInputItem';
import MaterialInput from '../../../components/common/MaterialInput';
import ReferenceInput from '../../rest/ReferenceInput';
import RestSelect from '../../../components/RestInput/RestSelect';

const MemberInfo = () => (
  <Card
    className="box"
    title={<span className="txtHeader">{i18next.t('members.detail.about')}</span>}
    extra={<SaveButton className="icEdit" />}
  >
    <RestInputItem
      rules={[
        {
          type: 'email',
          message: i18next.t('error.email'),
        },
      ]}
      required
      source="email"
    >
      <MaterialInput placeholder={i18next.t('members.email')} />
    </RestInputItem>
    <RestInputItem disabled required source="phoneNumber">
      <MaterialInput placeholder={i18next.t('members.phoneNumber')} />
    </RestInputItem>
    <RestInputItem required source="address">
      <MaterialInput placeholder={i18next.t('members.address')} />
    </RestInputItem>
    <RestInputItem isReference source="departmentId">
      <ReferenceInput source="departmentId" resource="departments">
        <RestSelect
          source="departmentId"
          valueProp="id"
          titleProp="name"
          header="members.department"
        />
      </ReferenceInput>
    </RestInputItem>
  </Card>
);
MemberInfo.propTypes = {};

export default MemberInfo;
