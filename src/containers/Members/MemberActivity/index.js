import React from 'react';
// import PropTypes from 'prop-types';
import { Timeline, Card, Avatar, Icon } from 'antd';
import i18next from 'i18next';
import Text from '../../../components/common/Text';

const ActivityLogs = () => (
  <Timeline>
    <TimelineItem />
    <TimelineItem />
    <TimelineItem />
    <TimelineItem />
    <TimelineItem />
  </Timeline>
);

const TimelineItem = () => {
  const title = (
    <div className="headerItem">
      <Avatar />
      <div className="headerContent">
        <Text type="body">
          Sanne Viscaal
          {' '}
          <Text type="bodyGray" inline>
            send mail to Donal Trump
          </Text>
        </Text>
        <Text type="smallText">14:30 AM</Text>
      </div>
    </div>
  );
  const extra = (
    <a href="#edit" className="txtEdit">
      {i18next.t('button.edit')}
    </a>
  );
  const dot = (
    <div className="dot">
      <Icon type="mail" className="icDot" />
    </div>
  );
  return (
    <Timeline.Item dot={dot}>
      <Text type="h4">10/04/1993</Text>
      <Card title={title} extra={extra} className="timelineItem">
        <Text type="body">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
          been the standard dummy text ever since the 1500s, when an unknown printer took a galley
          of type and scrambled it to make a type specimen book. It has survived not only five
          centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
          It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
          passages, and more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </Text>
      </Card>
    </Timeline.Item>
  );
};
ActivityLogs.propTypes = {};

export default ActivityLogs;
