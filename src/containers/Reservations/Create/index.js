import React from 'react';
import Create from '../../rest/Create';
import Form from '../components/Form/index';

const ReservationsCreate = props => (
  <Create {...props} resource="reservations">
    <Form />
  </Create>
);

ReservationsCreate.propTypes = {};

export default ReservationsCreate;
