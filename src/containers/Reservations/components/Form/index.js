import React from 'react';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const ReservationsForm = props => (
  <div {...props}>
    <RestInputItem source="startTime" header="reservations.startTime" />
  </div>
);

ReservationsForm.propTypes = {};

export default ReservationsForm;
