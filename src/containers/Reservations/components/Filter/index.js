import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = props => (
  <div {...props}>
    <RestInputItem source="startTime" placeholder="reservations.startTime" />
  </div>
);

Filter.propTypes = {};

export default Filter;
