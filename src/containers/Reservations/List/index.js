import React from 'react';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import DeleteButton from '../../../components/RestActions/DeleteButton';

const ReservationsList = props => (
  <List {...props} resource="reservations">
    <RestFieldItem source="startTime" header="reservations.startTime" />
    <ActionGroup>
      <EditButton />
      <DeleteButton />
    </ActionGroup>
  </List>
);

ReservationsList.propTypes = {};

export default ReservationsList;
