import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form';

const ReservationsEdit = props => (
  <Edit {...props} resource="reservations">
    <Form />
  </Edit>
);

ReservationsEdit.propTypes = {};

export default ReservationsEdit;
