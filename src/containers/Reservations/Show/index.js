import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const ReservationsShow = props => (
  <RestShow {...props} hasEdit resource="reservations">
    <RestFieldItem source="startTime" header="reservations.startTime" />
  </RestShow>
);

export default ReservationsShow;
