import React from 'react';
import RestShow from '../../rest/Show';
import RestFieldItem from '../../../components/RestField/RestFieldItem';

const TransactionsShow = props => (
  <RestShow {...props} hasEdit resource="transactions">
    <RestFieldItem source="category" header="transactions.category" />
    <RestFieldItem source="transactionTypeId" header="transactions.transactionType" />
    <RestFieldItem source="payType" header="transactions.payType" />
    <RestFieldItem source="bookingId" header="transactions.bookingId" />
    <RestFieldItem source="userId" header="transactions.userId" />
    <RestFieldItem source="tags" header="transactions.tags" />
    <RestFieldItem source="v" header="transactions.v" />
    <RestFieldItem source="description" header="transactions.description" />
    <RestFieldItem source="amount" header="transactions.amount" />
  </RestShow>
);

export default TransactionsShow;
