import React from 'react';
import { Row, Col, InputNumber, Input } from 'antd';
import PropTypes from 'prop-types';
import i18next from 'i18next';
import { useSelector } from 'react-redux';
import RestInputItem from '../../../../components/RestInput/RestInputItem';
import RestSelect from '../../../../components/RestInput/RestSelect';
import { PAYMENT_METHOD } from '../../../../configs/localData/index';
import crudSelectors from '../../../../redux/crudSelectors';

const TransactionsForm = ({ isEdit, ...props }) => {
  const transactionTypes = useSelector(crudSelectors.transactionTypes.getDataArr);
  return (
    <Row gutter={16}>
      <Col span={12}>
        <RestSelect
          disabled={isEdit}
          ruleType="string"
          resourceData={PAYMENT_METHOD.map(e => ({ ...e, text: i18next.t(e.text) }))}
          valueProp="value"
          titleProp="text"
          source="payType"
          defaultValue="cash"
          header="transactions.payType"
        />
      </Col>
      <Col span={12}>
        <RestInputItem
          disabled={isEdit}
          ContentComponent={InputNumber}
          ruleType="number"
          source="amount"
          header="transactions.amount"
        />
      </Col>
      <Col span={24}>
        <RestSelect
          resourceData={transactionTypes.filter(e => e.category === props.type)}
          disabled={isEdit}
          valueProp="id"
          titleProp="name"
          source="transactionTypeId"
          header="transactions.transactionType"
        />
        <RestInputItem
          ContentComponent={Input.TextArea}
          source="description"
          header="transactions.description"
        />
      </Col>
    </Row>
  );
};

TransactionsForm.propTypes = {
  type: PropTypes.string,
  isEdit: PropTypes.bool,
};

TransactionsForm.defaultProps = {
  isEdit: false,
};

export default TransactionsForm;
