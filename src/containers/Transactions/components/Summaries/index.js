import React, { useEffect } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';
import { Row, Col } from 'antd';
import { getPaymentReports as getPaymentReportsActions } from '../../../../redux/reports/actions';
import SummaryCard from '../../../../components/common/SummaryCard';
import { formatMoney } from '../../../../utils/textUtils';
import { getFilterEndTime, getFilterStartTime } from '../../../../redux/reports/selectors';

const Summaries = ({
  theme,
  paymentReports,
  getPaymentReports,
  filterStartTime,
  filterEndTime,
}) => {
  const SUMMARIES = [
    {
      icon: 'ic-payment',
      header: 'transactions.summaries.profit',
      value: formatMoney(
        paymentReports && paymentReports.totalIncome
          ? paymentReports.totalIncome - paymentReports.totalExpense
          : 0
      ),
      color: theme.color.violet,
    },
    {
      icon: 'plus',
      header: 'transactions.summaries.income',
      value: formatMoney(paymentReports.totalIncome) || 0,
      color: theme.color.blueShade,
    },
    {
      icon: 'minus',
      header: 'transactions.summaries.expense',
      value: formatMoney(paymentReports.totalExpense) || 0,
      color: theme.color.lightGreen,
    },
  ];

  const getReports = (startTime, endTime) => {
    const paymentReportsParams =
      startTime && endTime
        ? {
            startTime,
            endTime,
          }
        : {
            startTime: moment()
              .startOf('M')
              .toISOString(),
            endTime: moment().toISOString(),
          };
    delete paymentReportsParams.createdAt;
    getPaymentReports(paymentReportsParams);
  };

  useEffect(() => {
    getReports(filterStartTime, filterEndTime);
  }, [filterStartTime, filterEndTime, getReports]);

  return (
    <Row gutter={24} type="flex">
      {SUMMARIES.map((data, index) => (
        <Col key={String(index)} lg={8} md={8} xs={24}>
          <SummaryCard {...data} />
        </Col>
      ))}
    </Row>
  );
};
Summaries.propTypes = {
  theme: PropTypes.object,
  getPaymentReports: PropTypes.func,
  paymentReports: PropTypes.object,
  filterStartTime: PropTypes.string,
  filterEndTime: PropTypes.string,
};

export default connect(
  state => ({
    paymentReports: state.reports.paymentReports,
    filterStartTime: getFilterStartTime(state),
    filterEndTime: getFilterEndTime(state),
  }),
  dispatch => ({
    getPaymentReports: data => dispatch(getPaymentReportsActions(data)),
  })
)(withTheme(Summaries));
