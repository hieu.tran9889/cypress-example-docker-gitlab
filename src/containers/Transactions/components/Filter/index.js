import React from 'react';
// import PropTypes from 'prop-types';
import RestInputItem from '../../../../components/RestInput/RestInputItem';

const Filter = () => (
  <div>
    <RestInputItem source="category" placeholder="transactions.category" />
    <RestInputItem source="transactionTypeId" placeholder="transactions.transactionType" />
    <RestInputItem source="payType" placeholder="transactions.payType" />
    <RestInputItem source="bookingId" placeholder="transactions.bookingId" />
    <RestInputItem source="userId" placeholder="transactions.userId" />
    <RestInputItem source="tags" placeholder="transactions.tags" />
    <RestInputItem source="v" placeholder="transactions.v" />
    <RestInputItem source="description" placeholder="transactions.description" />
    <RestInputItem source="amount" placeholder="transactions.amount" />
  </div>
);

Filter.propTypes = {};

export default Filter;
