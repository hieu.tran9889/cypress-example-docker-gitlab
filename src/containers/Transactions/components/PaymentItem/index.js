import React from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';
import { PaymentItemWrapper } from './styles';
import Text from '../../../../components/common/Text';
import { formatMoney } from '../../../../utils/textUtils';

const PaymentItem = ({ record }) => (
  <PaymentItemWrapper>
    <Text
      className="value"
      style={{
        color: record.transactionType === 'EXPENSE' ? '#0992d0' : '#e64c38',
      }}
    >
      {record.transactionType === 'EXPENSE' ? '-' : ''}
      {formatMoney(record.amount)} VND
    </Text>
  </PaymentItemWrapper>
);

PaymentItem.propTypes = {
  record: PropTypes.object,
};

export default withTheme(PaymentItem);
