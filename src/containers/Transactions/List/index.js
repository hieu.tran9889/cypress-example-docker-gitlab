import React from 'react';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { Tag } from 'antd';
import PropTypes from 'prop-types';
import List from '../../rest/List';
import RestFieldItem from '../../../components/RestField/RestFieldItem';
import ActionGroup from '../../../components/RestActions/ActionGroup';
import EditButton from '../../../components/RestActions/EditButton';
import UserInfo from '../../../components/RestField/UserInfo';
import Reference from '../../rest/Reference';
import EnumField from '../../../components/RestField/EnumField/index';
import { TRANSACTION_TYPE, PAYMENT_METHOD } from '../../../configs/localData';
import { formatMoney, formatDateTime, formatDate } from '../../../utils/textUtils';
import { dateFilterDropdown } from '../../../components/RestInput/RestDateTimePicker';
import { getFilterStartTime, getFilterEndTime } from '../../../redux/reports/selectors';

const TransactionsList = ({ isCustomerPage, startTime, endTime, ...props }) => (
  <List
    {...props}
    hasCreate={false}
    header={i18next.t('transactions.title', {
      range: startTime
        ? `${formatDate(startTime)} - ${formatDate(endTime)}`
        : i18next.t('transactions.all'),
    })}
    resource="transactions"
  >
    {!isCustomerPage && (
      <Reference source="userId" header="checkins.user" resource="users">
        <RestFieldItem
          format={(data, record) => <UserInfo prefixLink="customers" record={record} />}
          source="email"
        />
      </Reference>
    )}
    <EnumField
      format={data => <Tag>{data}</Tag>}
      filters={TRANSACTION_TYPE.map(e => ({ ...e, text: i18next.t(e.text) }))}
      resourceData={TRANSACTION_TYPE}
      source="category"
      header="transactions.category"
    />
    <RestFieldItem source="description" header="transactions.description" />
    <EnumField
      filters={PAYMENT_METHOD.map(e => ({ ...e, text: i18next.t(e.text) }))}
      resourceData={PAYMENT_METHOD}
      source="payType"
      header="transactions.payType"
    />
    <RestFieldItem
      sorter
      format={data => formatMoney(data)}
      source="amount"
      header="transactions.amount"
    />
    <RestFieldItem
      sorter
      filterDropdown={dateFilterDropdown}
      format={data => formatDateTime(data)}
      source="createdAt"
      header="transactions.createdAt"
    />
    <ActionGroup>
      <EditButton />
    </ActionGroup>
  </List>
);

TransactionsList.propTypes = {
  isCustomerPage: PropTypes.bool,
};

export default connect(state => ({
  startTime: getFilterStartTime(state),
  endTime: getFilterEndTime(state),
}))(TransactionsList);
