import React from 'react';
import RestCreate from '../../rest/Create';
import Form from '../components/Form/index';

const CreateForIncomePayments = props => (
  <RestCreate
    {...props}
    formatOnSubmit={data => ({
      ...data,
      category: 'INCOME',
    })}
    showModal
    resource="transactions"
    header="transactions.createIncome"
  >
    <Form type="INCOME" />
  </RestCreate>
);

CreateForIncomePayments.propTypes = {};

export default CreateForIncomePayments;
