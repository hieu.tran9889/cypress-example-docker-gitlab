import React from 'react';
import RestCreate from '../../rest/Create';
import Form from '../components/Form';

const CreateForExpensePayments = props => (
  <RestCreate
    {...props}
    formatOnSubmit={data => ({
      ...data,
      category: 'EXPENSE',
    })}
    showModal
    resource="transactions"
    header="transactions.createExpense"
  >
    <Form type="EXPENSE" />
  </RestCreate>
);

CreateForExpensePayments.propTypes = {};

export default CreateForExpensePayments;
