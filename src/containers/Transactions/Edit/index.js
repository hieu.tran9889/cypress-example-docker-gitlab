import React from 'react';
import Edit from '../../rest/Edit';
import Form from '../components/Form/index';

const TransactionsEdit = props => (
  <Edit {...props} resource="transactions" header="transactions.edit">
    <Form isEdit />
  </Edit>
);

TransactionsEdit.propTypes = {};

export default TransactionsEdit;
