import { makeCRUDConstantCreator, makeCRUDActionsCreator } from '../crudCreator/actions';
import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const PRIMARY_KEY = 'id';
export const MODEL = 'reservations';
export const IGNORE_ACTIONS = [];
export const ReservationsTypes = {
  ...makeCRUDConstantCreator(MODEL, IGNORE_ACTIONS),
  ...makeConstantCreator(
    'GET_RESERVATIONS',
    'GET_RESERVATIONS_FAIL',
    'GET_RESERVATIONS_SUCCESS',
    'CHANGE_RESERVATIONS',
    'REMOVE_ALL_CHANGED',
    'CHANGE_DEFAULT_VIEW'
  ),
};
const CRUDReservationsActions = makeCRUDActionsCreator(MODEL, IGNORE_ACTIONS);

export const getReservations = data =>
  makeActionCreator(ReservationsTypes.GET_RESERVATIONS, { data });

export const getReservationsFailure = error =>
  makeActionCreator(ReservationsTypes.GET_RESERVATIONS_FAIL, { error });
export const changeReservationsEvent = (data, ids) =>
  makeActionCreator(ReservationsTypes.CHANGE_RESERVATIONS, { data, ids });
export const removeAllReservationsChanged = data =>
  makeActionCreator(ReservationsTypes.REMOVE_ALL_CHANGED, { data });
export const changeDefaultView = data =>
  makeActionCreator(ReservationsTypes.CHANGE_DEFAULT_VIEW, { data });

/**
 * getAllCaseTypes({pageSize, page })
 * getByIdCaseTypes(data)
 * createCaseTypes(data)
 * deleteCaseTypes()
 * editCaseTypes(data)
 */
export default { ...CRUDReservationsActions };
