import { makeReducerCreator } from '../../utils/reduxUtils';
import { makeCRUDReducerCreator, INITIAL_CRUD_STATE } from '../crudCreator/reducer';
import { MODEL, IGNORE_ACTIONS, ReservationsTypes } from './actions';

export const INITIAL_STATE = {
  ...INITIAL_CRUD_STATE,
};

const getReservationsFail = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});
const changeReservations = (state, { data, ids }) => ({
  ...state,
  changedData: data,
  changedIds: ids,
});
const removeReservationsChanged = state => ({
  ...state,
  changedData: {},
  changedIds: [],
});

const changeDefaultView = (state, { data }) => ({
  ...state,
  defaultView: data,
});
const reducer = makeReducerCreator(INITIAL_STATE, {
  ...makeCRUDReducerCreator(MODEL, IGNORE_ACTIONS),
  [ReservationsTypes.GET_RESERVATIONS_FAIL]: getReservationsFail,
  [ReservationsTypes.CHANGE_RESERVATIONS]: changeReservations,
  [ReservationsTypes.REMOVE_ALL_CHANGED]: removeReservationsChanged,
  [ReservationsTypes.CHANGE_DEFAULT_VIEW]: changeDefaultView,
});

export default reducer;
