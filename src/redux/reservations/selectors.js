import { createSelector } from 'reselect';
import { union } from 'lodash';

const getRawReservationData = state => state.reservations.data;
const getRawReservationIds = state => state.reservations.ids;
const getReservationsChangedData = state => state.reservations.changedData;
const getReservationsChangedIds = state => state.reservations.changedIds;

export const reservationsData = createSelector(
  [
    getRawReservationData,
    getRawReservationIds,
    getReservationsChangedData,
    getReservationsChangedIds,
  ],
  (data, ids, changedData, changedIds) =>
    union(ids, changedIds).map(id => {
      const reservation = data[id]
        ? {
            id,
            bookingId: data[id].bookingId,
            title: data[id].note,
            start: new Date(data[id].checkinTime),
            end: new Date(data[id].checkoutTime),
          }
        : {};
      return changedData && changedData[id] ? { ...reservation, ...changedData[id] } : reservation;
    })
);
