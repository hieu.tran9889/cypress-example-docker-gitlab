import { makeCRUDConstantCreator, makeCRUDActionsCreator } from '../crudCreator/actions';
import { makeActionCreator, makeConstantCreator } from '../../utils/reduxUtils';

export const PRIMARY_KEY = 'id';
export const MODEL = 'bookings';
export const IGNORE_ACTIONS = ['GET_ALL'];
export const BookingsTypes = {
  ...makeCRUDConstantCreator(MODEL, IGNORE_ACTIONS),
  ...makeConstantCreator(
    'GET_REALTIME_BOOKINGS_WORK',
    'GET_REALTIME_BOOKINGS_WORK_FAIL',
    'GET_REALTIME_BOOKINGS_WORK_SUCCESS',

    'GET_PRE_CHECKOUT_BOOKING',
    'GET_PRE_CHECKOUT_BOOKING_FAIL',
    'GET_PRE_CHECKOUT_BOOKING_SUCCESS',

    'CREATE_BOOKING',
    'CREATE_BOOKING_FAIL',
    'CREATE_BOOKING_SUCCESS',

    'CHECKIN_BOOKING',
    'CHECKIN_BOOKING_FAIL',
    'CHECKIN_BOOKING_SUCCESS',

    'CHECKOUT_BOOKING',
    'CHECKOUT_BOOKING_FAIL',
    'CHECKOUT_BOOKING_SUCCESS',

    'SET_BOOKING_FILTER',

    'PAY_BOOKING',
    'PAY_BOOKING_FAIL',
    'PAY_BOOKING_SUCCESS',

    'UPDATE_BOOKING_STATUS',
    'UPDATE_BOOKING_STATUS_FAIL',
    'UPDATE_BOOKING_STATUS_SUCCESS',

    'ADD_SERVICE_FOR_BOOKING',
    'ADD_SERVICE_FOR_BOOKING_FAIL',
    'ADD_SERVICE_FOR_BOOKING_SUCCESS',

    'ADD_PAYMENT_FOR_BOOKING',
    'ADD_PAYMENT_FOR_BOOKING_FAIL',
    'ADD_PAYMENT_FOR_BOOKING_SUCCESS',
    'WATCH_DASHBOARD',
    'CLOSE_DASHBOARD'
  ),
};
const CRUDBookingsActions = makeCRUDActionsCreator(MODEL, IGNORE_ACTIONS);
export const getBookings = () => makeActionCreator(BookingsTypes.GET_REALTIME_BOOKINGS_WORK);
export const getBookingsSuccess = data =>
  makeActionCreator(BookingsTypes.GET_REALTIME_BOOKINGS_WORK_SUCCESS, { data });
export const getBookingsFailure = error =>
  makeActionCreator(BookingsTypes.GET_REALTIME_BOOKINGS_WORK_FAIL, { error });

export const createBooking = (data, isCheckin) =>
  makeActionCreator(BookingsTypes.CREATE_BOOKING, { data, isCheckin });
export const createBookingSuccess = data =>
  makeActionCreator(BookingsTypes.CREATE_BOOKING_SUCCESS, { data });
export const createBookingFailure = error =>
  makeActionCreator(BookingsTypes.CREATE_BOOKING_FAIL, { error });

export const checkinBooking = (data, payload) =>
  makeActionCreator(BookingsTypes.CHECKIN_BOOKING, { data, payload });
export const checkinBookingSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.CHECKIN_BOOKING_SUCCESS, { bookingId, data });
export const checkinBookingFailure = error =>
  makeActionCreator(BookingsTypes.CHECKIN_BOOKING_FAIL, { error });

export const checkoutBooking = (data, isPay, payload) =>
  makeActionCreator(BookingsTypes.CHECKOUT_BOOKING, { data, isPay, payload });
export const checkoutBookingSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.CHECKOUT_BOOKING_SUCCESS, { bookingId, data });
export const checkoutBookingFailure = error =>
  makeActionCreator(BookingsTypes.CHECKOUT_BOOKING_FAIL, { error });

export const setBookingFilter = data =>
  makeActionCreator(BookingsTypes.SET_BOOKING_FILTER, { data });

export const getPreCheckout = bookingId =>
  makeActionCreator(BookingsTypes.GET_PRE_CHECKOUT_BOOKING, { bookingId });
export const getPreCheckoutSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.GET_PRE_CHECKOUT_BOOKING_SUCCESS, { bookingId, data });
export const getPreCheckoutFailure = error =>
  makeActionCreator(BookingsTypes.GET_PRE_CHECKOUT_BOOKING_FAIL, { error });

export const payBooking = (bookingId, data) =>
  makeActionCreator(BookingsTypes.PAY_BOOKING, { bookingId, data });
export const payBookingSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.PAY_BOOKING_SUCCESS, { bookingId, data });
export const payBookingFailure = error =>
  makeActionCreator(BookingsTypes.PAY_BOOKING_FAIL, { error });

export const updateBookingStatus = (bookingId, data) =>
  makeActionCreator(BookingsTypes.UPDATE_BOOKING_STATUS, { bookingId, data });
export const updateBookingStatusSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.UPDATE_BOOKING_STATUS_SUCCESS, { bookingId, data });
export const updateBookingStatusFailure = error =>
  makeActionCreator(BookingsTypes.UPDATE_BOOKING_STATUS_FAIL, { error });

export const addServiceForBooking = (bookingId, data) =>
  makeActionCreator(BookingsTypes.ADD_SERVICE_FOR_BOOKING, { bookingId, data });
export const addServiceForBookingSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.ADD_SERVICE_FOR_BOOKING_SUCCESS, { bookingId, data });
export const addServiceForBookingFailure = error =>
  makeActionCreator(BookingsTypes.ADD_SERVICE_FOR_BOOKING_FAIL, { error });

export const addPaymentForBooking = (bookingId, data) =>
  makeActionCreator(BookingsTypes.ADD_PAYMENT_FOR_BOOKING, { bookingId, data });
export const addPaymentForBookingSuccess = (bookingId, data) =>
  makeActionCreator(BookingsTypes.ADD_PAYMENT_FOR_BOOKING_SUCCESS, { bookingId, data });
export const addPaymentForBookingFailure = error =>
  makeActionCreator(BookingsTypes.ADD_PAYMENT_FOR_BOOKING_FAIL, { error });

export const watchDashboard = () => makeActionCreator(BookingsTypes.WATCH_DASHBOARD);
export const closeDashboard = () => makeActionCreator(BookingsTypes.CLOSE_DASHBOARD);

/**
 * getAllCaseTypes({pageSize, page })
 * getByIdCaseTypes(data)
 * createCaseTypes(data)
 * deleteCaseTypes()
 * editCaseTypes(data)
 */
export default { ...CRUDBookingsActions };
