import { takeEvery, put, call, select, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import moment from 'moment';
import { replace } from 'connected-react-router';
import rootCRUDSaga from '../crudCreator/saga';
import BookingsActions, {
  getBookingsSuccess,
  getBookingsFailure,
  BookingsTypes,
  MODEL,
  PRIMARY_KEY,
  createBookingSuccess,
  createBookingFailure,
  checkinBookingSuccess,
  checkinBookingFailure,
  checkoutBookingSuccess,
  checkoutBookingFailure,
  checkinBooking as checkinBookingAction,
  getPreCheckoutFailure,
  getPreCheckoutSuccess,
  payBookingSuccess,
  payBookingFailure,
  payBooking as payBookingAction,
  updateBookingStatusFailure,
  updateBookingStatusSuccess,
} from './actions';
import {
  getBookings,
  createBooking,
  checkinBooking,
  checkoutBooking,
  checkPricing,
  payBooking,
  updateBookingStatus,
  addPaymentForBooking,
  addServiceForBooking,
  getBooking,
} from '../../api/bookings';

import { getReservations } from '../reservations/actions';
import { initSocket } from '../../api/socket';
import { apiWrapper } from '../../utils/reduxUtils';
import { convertResponseData } from '../crudCreator/dataProvider';
import { formattedData } from '../../utils/tools';
// use IGNORE_SAGAS to replace "saga" or ignore "saga"
const IGNORE_SAGAS = ['GET_ALL', 'GET_BY_ID', 'DELETE', 'EDIT', 'CREATE'];

let dashboardChannel = null;

function* getBookingsWorkSaga() {
  try {
    const response = yield call(apiWrapper, { isShowProgress: true }, getBookings);
    if (response) {
      yield put(
        getBookingsSuccess({
          ...response,
          data: {
            ...formattedData(response.todayBooking).data,
            ...formattedData(response.pendingBooking).data,
          },
          todayBookingIds: formattedData(response.todayBooking).ids,
          pendingBookingIds: formattedData(response.pendingBooking).ids,
        })
      );
    } else {
      yield put(getBookingsFailure(response));
    }
  } catch (error) {
    yield put(getBookingsFailure(error));
  }
}

function* createBookingSaga({ data, isCheckin }) {
  try {
    const { location } = yield select(state => state.router);
    const response = yield call(apiWrapper, { isShowProgress: true }, createBooking, data);
    if (response) {
      yield put(getReservations());
      yield put(replace(location.pathname));
      yield put(createBookingSuccess(response));
      if (isCheckin) {
        yield put(checkinBookingAction(response, { startTime: response.startTime }));
      }
    } else {
      yield put(createBookingFailure(response));
    }
  } catch (error) {
    yield put(createBookingFailure(error));
  }
}

function* checkinBookingSaga({ data, payload }) {
  try {
    const { location } = yield select(state => state.router);
    const response = yield call(
      apiWrapper,
      { isShowProgress: true, isShowSuccessNoti: true },
      checkinBooking,
      data.id,
      payload
    );
    if (response) {
      yield put(checkinBookingSuccess(data.id, response));
      yield put(replace(location.pathname));
    } else {
      yield put(checkinBookingFailure(response));
    }
  } catch (error) {
    yield put(checkinBookingFailure(error));
  }
}

function* checkoutBookingSaga({ data, isPay, payload }) {
  try {
    const { location } = yield select(state => state.router);
    const response = yield call(
      apiWrapper,
      { isShowProgress: true, isShowSuccessNoti: true },
      checkoutBooking,
      data.id,
      payload
    );
    if (response) {
      yield put(checkoutBookingSuccess(data.id, response));
      yield put(replace(location.pathname));
    } else {
      yield put(checkoutBookingFailure(response));
    }
    if (isPay) {
      yield put(payBookingAction(data.id));
    }
  } catch (error) {
    yield put(checkoutBookingFailure(error));
  }
}

function* getPreCheckoutBookingSaga({ bookingId }) {
  try {
    const { data } = yield select(state => state.realtimeBookings);
    let preCheckoutBooking = data[bookingId];
    if (!preCheckoutBooking) {
      preCheckoutBooking = yield call(getBooking, bookingId);
    }
    const {
      startTime,
      actualEndTime,
      expectedEndTime,
      numberOfCustomer,
      roomId,
      packageTypeId,
      discountValue,
      discountUnit,
      checkins,
    } = preCheckoutBooking;
    const requestParams = {
      roomId,
      packageTypeId,
      startTime: moment(checkins && checkins[0] ? checkins[0].startTime : startTime).toISOString(),
      endTime: preCheckoutBooking.package.isHour
        ? moment().toISOString()
        : actualEndTime || expectedEndTime,
      numberOfCustomer,
      discountValue,
      discountUnit,
    };
    const response = yield call(apiWrapper, { isShowProgress: true }, checkPricing, requestParams);
    if (response) {
      yield put(getPreCheckoutSuccess(bookingId, { ...preCheckoutBooking, ...response }));
    } else {
      yield put(getPreCheckoutFailure(response));
    }
  } catch (error) {
    yield put(getPreCheckoutFailure(error));
  }
}

function* payBookingSaga({ bookingId }) {
  try {
    const response = yield call(apiWrapper, { isShowProgress: true }, payBooking, bookingId, {});
    if (response) {
      yield put(payBookingSuccess(bookingId, response));
    } else {
      yield put(payBookingFailure(bookingId, response));
    }
  } catch (error) {
    yield put(payBookingFailure(error));
  }
}

function* updateBookingStatusSaga({ bookingId, data }) {
  try {
    const response = yield call(
      apiWrapper,
      { isShowProgress: true },
      updateBookingStatus,
      bookingId,
      data
    );
    if (response) {
      const convertData = convertResponseData('EDIT', response);
      yield put(BookingsActions.editBookingsSuccess(convertData));
      yield put(updateBookingStatusSuccess(bookingId, response));
      yield put(updateBookingStatusSuccess(bookingId, response));
    } else {
      yield put(updateBookingStatusFailure(bookingId, response));
    }
  } catch (error) {
    yield put(updateBookingStatusFailure(error));
  }
}

function* addServiceForBookingSaga({ bookingId, data }) {
  try {
    const response = yield call(
      apiWrapper,
      { isShowProgress: true, isShowSuccessNoti: true },
      addServiceForBooking,
      data
    );
    if (response) {
      const { bookings } = yield select(state => state.rest);
      const booking = bookings.list[bookingId];
      yield put(
        BookingsActions.editBookingsSuccess({
          ...booking,
          services: [...booking.services, response],
        })
      );
    } else {
      yield put(BookingsActions.editBookingsFailure({ id: bookingId, ...response }));
    }
  } catch (error) {
    yield put(BookingsActions.editBookingsFailure(error));
  }
}

function* addPaymentForBookingSaga({ bookingId, data }) {
  try {
    const response = yield call(
      apiWrapper,
      { isShowProgress: true, isShowSuccessNoti: true },
      addPaymentForBooking,
      data
    );
    if (response) {
      const bookings = yield select(state => state.realtimeBookings.data);
      const booking = bookings[bookingId];
      yield put(
        BookingsActions.editBookingsSuccess({
          ...booking,
          transactions: [...booking.transactions, response],
        })
      );
    } else {
      yield put(BookingsActions.editBookingsFailure({ id: bookingId, ...response }));
    }
  } catch (error) {
    yield put(BookingsActions.editBookingsFailure(error));
  }
}

function createDashboardChannel() {
  return eventChannel(emit => {
    const dashboardHandler = datas => {
      emit(datas);
    };
    const unsubscribe = initSocket(dashboardHandler);
    return () => {
      unsubscribe.close();
    };
  });
}
// reply with a `pong` message by invoking `socket.emit('pong')`

export function* watchDashboardSaga({ data }) {
  dashboardChannel = yield call(createDashboardChannel, data);
  while (true) {
    try {
      const payload = yield take(dashboardChannel);
      yield put(
        getBookingsSuccess({
          ...payload,
          data: {
            ...formattedData(payload.todayBooking).data,
            ...formattedData(payload.pendingBooking).data,
          },
          todayBookingIds: formattedData(payload.todayBooking).ids,
          pendingBookingIds: formattedData(payload.pendingBooking).ids,
        })
      );
    } catch (error) {
      dashboardChannel.close();
    }
  }
}

export function* closeDashboardSaga() {
  try {
    yield call(dashboardChannel.close);
  } catch (error) {
    // tesst
  }
}

export default [
  ...rootCRUDSaga(MODEL, IGNORE_SAGAS, BookingsActions, PRIMARY_KEY),
  takeEvery(BookingsTypes.GET_REALTIME_BOOKINGS_WORK, getBookingsWorkSaga),
  takeEvery(BookingsTypes.CREATE_BOOKING, createBookingSaga),
  takeEvery(BookingsTypes.CHECKIN_BOOKING, checkinBookingSaga),
  takeEvery(BookingsTypes.GET_PRE_CHECKOUT_BOOKING, getPreCheckoutBookingSaga),
  takeEvery(BookingsTypes.CHECKOUT_BOOKING, checkoutBookingSaga),
  takeEvery(BookingsTypes.PAY_BOOKING, payBookingSaga),
  takeEvery(BookingsTypes.UPDATE_BOOKING_STATUS, updateBookingStatusSaga),
  takeEvery(BookingsTypes.ADD_SERVICE_FOR_BOOKING, addServiceForBookingSaga),
  takeEvery(BookingsTypes.ADD_PAYMENT_FOR_BOOKING, addPaymentForBookingSaga),
  takeEvery(BookingsTypes.WATCH_DASHBOARD, watchDashboardSaga),
  takeEvery(BookingsTypes.CLOSE_DASHBOARD, closeDashboardSaga),
];
