import { createSelector } from 'reselect';
import { CHECKIN_STATUS } from '../../configs/localData';

const getCurrentId = state => state.realtimeBookings.currentId;
const getBookingData = state => state.realtimeBookings;
const getFiltersData = state => state.realtimeBookings.filters;

export const getCurrentTabBookings = createSelector(
  [getBookingData, getFiltersData],
  (bookingData, filters) => {
    const { checkinType, userId, roomId, packageTypeId } = filters;
    const { data } = bookingData;
    const currentTabIds =
      bookingData[
        `${checkinType === CHECKIN_STATUS[4].value ? 'pendingBookingIds' : 'todayBookingIds'}`
      ] || [];
    const currentTabData = currentTabIds
      .map(id => data[id])
      .filter(
        item =>
          (!userId || userId === '' || `${item.userId}` === `${userId}`) &&
          (!roomId || roomId === '' || `${item.roomId}` === `${roomId}`) &&
          (!packageTypeId || packageTypeId === '' || `${item.packageTypeId}` === `${packageTypeId}`)
      );

    const results = {};

    results[CHECKIN_STATUS[0].value] = currentTabData.filter(
      item => !item || !item.checkins || !item.checkins[0]
    );
    results[CHECKIN_STATUS[1].value] = currentTabData.filter(
      item => item && item.checkins && item.checkins[0] && !item.checkins[0].endTime
    );
    results[CHECKIN_STATUS[2].value] = currentTabData.filter(
      item => item && item.checkins && item.checkins[0] && item.checkins[0].endTime
    );

    return (
      results[checkinType] || [
        ...results[CHECKIN_STATUS[0].value],
        ...results[CHECKIN_STATUS[1].value],
        ...results[CHECKIN_STATUS[2].value],
      ]
    );
  }
);

export const currentBooking = createSelector(
  [getBookingData, getCurrentId],
  (bookingData, currentId) => {
    const { data } = bookingData;
    return data[currentId] || {};
  }
);
