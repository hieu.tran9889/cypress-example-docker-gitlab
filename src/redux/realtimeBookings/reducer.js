import { makeReducerCreator } from '../../utils/reduxUtils';
import { makeCRUDReducerCreator, INITIAL_CRUD_STATE } from '../crudCreator/reducer';
import { MODEL, IGNORE_ACTIONS, BookingsTypes } from './actions';
import { BOOKINGS_TYPES, CHECKIN_STATUS } from '../../configs/localData/index';

export const INITIAL_STATE = {
  ...INITIAL_CRUD_STATE,
  data: {},
  loading: false,
  filters: {
    currentTab: BOOKINGS_TYPES[0],
    checkinType: CHECKIN_STATUS[0].value,
  },
};

const getBookings = state => ({
  ...state,
  loading: true,
});

const getBookingsSuccess = (state, { data }) => ({
  ...state,
  ...data,
  loading: false,
});

const getBookingsFail = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

const setFilters = (state, { data }) => ({
  ...state,
  filters: { ...state.filters, ...data },
  loading: false,
});

const checkinSuccess = state => ({
  ...state,
  loading: false,
});

const createSuccess = state => ({
  ...state,
  loading: false,
});

const checkoutSuccess = state => ({
  ...state,
  loading: false,
});

const getPreCheckout = (state, { bookingId }) => ({
  ...state,
  currentId: bookingId,
  loading: true,
});

const setCurrent = (state, { data }) => ({
  ...state,
  currentId: data.id,
  loading: true,
});

const getPreCheckoutSuccess = (state, { bookingId, data }) => ({
  ...state,
  data: {
    ...state.data,
    [bookingId]: { ...state.data[bookingId], ...data },
  },
  loading: false,
});

const getPreCheckoutFailure = (state, { bookingId }) => ({
  ...state,
  currentId: bookingId,
  loading: false,
});

const reducer = makeReducerCreator(INITIAL_STATE, {
  ...makeCRUDReducerCreator(MODEL, IGNORE_ACTIONS),
  [BookingsTypes.GET_REALTIME_BOOKINGS_WORK_SUCCESS]: getBookingsSuccess,
  [BookingsTypes.GET_REALTIME_BOOKINGS_WORK_FAIL]: getBookingsFail,
  [BookingsTypes.GET_REALTIME_BOOKINGS_WORK]: getBookings,
  [BookingsTypes.SET_BOOKING_FILTER]: setFilters,
  [BookingsTypes.CHECKIN_BOOKING_SUCCESS]: checkinSuccess,
  [BookingsTypes.CHECKOUT_BOOKING_SUCCESS]: checkoutSuccess,
  [BookingsTypes.CREATE_BOOKING_SUCCESS]: createSuccess,
  [BookingsTypes.SET_CURRENT_BOOKINGS]: setCurrent,
  [BookingsTypes.GET_BY_ID_BOOKINGS]: setCurrent,
  [BookingsTypes.SET_CURRENT_BOOKINGS]: setFilters,
  [BookingsTypes.GET_PRE_CHECKOUT_BOOKING]: getPreCheckout,
  [BookingsTypes.GET_PRE_CHECKOUT_BOOKING_SUCCESS]: getPreCheckoutSuccess,
  [BookingsTypes.GET_PRE_CHECKOUT_BOOKING_FAIL]: getPreCheckoutFailure,
});

export default reducer;
