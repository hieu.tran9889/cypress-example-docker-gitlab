import { call, put, takeEvery } from 'redux-saga/effects';
import {
  ConfigTypes,
  getConfig as getConfigAction,
  getConfigSuccess,
  getConfigFailure,
} from './actions';
import { apiWrapper } from '../../utils/reduxUtils';
import { getConfig } from '../../api/configs';
import { watchDashboard } from '../realtimeBookings/actions';
import { formattedRESTData } from '../../utils/tools';

export function* getConfigSaga() {
  try {
    const response = yield call(
      apiWrapper,
      { isShowProgress: false, isShowSuccessNoti: false },
      getConfig
    );
    if (response) {
      const results = {};
      Object.keys(response).forEach(key => {
        results[key] = formattedRESTData(response[key]);
      });
      yield put(getConfigSuccess(results));
    } else {
      yield put(getConfigFailure(response));
    }
  } catch (error) {
    yield put(getConfigFailure(error));
  }
}

export function* initAppSaga() {
  try {
    yield put(getConfigAction());
    yield put(watchDashboard());
  } catch (error) {
    yield put(getConfigFailure(error));
  }
}
export default [
  takeEvery(ConfigTypes.GET_CONFIG, getConfigSaga),
  takeEvery(ConfigTypes.INIT_APP, initAppSaga),
];
