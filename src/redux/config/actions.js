import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const ConfigTypes = makeConstantCreator(
  'INIT_APP',
  'GET_CONFIG',
  'GET_CONFIG_SUCCESS',
  'GET_CONFIG_FAILURE',
  'TOGGLE_COLLAPSED'
);
export const toggleCollapsedAction = () => makeActionCreator(ConfigTypes.TOGGLE_COLLAPSED);
export const initApp = () => makeActionCreator(ConfigTypes.INIT_APP);
export const getConfig = data => makeActionCreator(ConfigTypes.GET_CONFIG, { data });
export const getConfigSuccess = data => makeActionCreator(ConfigTypes.GET_CONFIG_SUCCESS, { data });
export const getConfigFailure = error =>
  makeActionCreator(ConfigTypes.GET_CONFIG_FAILURE, { error });
