import { createSelector } from 'reselect';
import _ from 'lodash';

const getRawConfigName = (state, name) => state.config[name];
const getRawPackages = state => state.config.packages;
const getRawPackageTypes = state => state.config.packageTypes;
const getRawRooms = state => state.config.rooms;

export const getConfigByName = createSelector(
  [getRawConfigName],
  config => {
    return config?.ids?.map(id => config?.data[id]) || [];
  }
);

export const getPackagesGroupByRoom = createSelector(
  [getRawPackages, getRawPackageTypes, getRawRooms],
  (packages, rawPackageTypes, rawRooms) => {
    const packagesList = packages?.ids?.map(id => packages?.data[id]) || [];
    const packageTypes = rawPackageTypes?.ids?.map(id => rawPackageTypes?.data[id]) || [];
    const rooms = rawRooms?.ids?.map(id => rawRooms?.data[id]) || [];

    const packageFormated = _.sortBy(
      packagesList.map(data => ({
        ...data,
        room: rooms[data.roomId],
        packageType: packageTypes.find(e => e.id === data.packageTypeId),
      })),
      'price'
    );
    const packageFormatedGroupByRoom = _.groupBy(packageFormated, 'roomId');
    return packageFormatedGroupByRoom;
  }
);

export const getPackageTypes = createSelector(
  [getRawPackageTypes],
  config => {
    return config?.ids?.map(id => config?.data[id]) || [];
  }
);
export const getRooms = createSelector(
  [getRawRooms],
  rooms => rooms?.ids?.map(id => rooms?.data[id]) || []
);
