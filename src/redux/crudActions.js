// import crud action
import locations from './locations/actions';
import reservations from './reservations/actions';
import transactions from './transactions/actions';
import checkins from './checkins/actions';
import bookings from './bookings/actions';
import customers from './customers/actions';
import transactionTypes from './transactionTypes/actions';
import packages from './packages/actions';
import packageTypes from './packageTypes/actions';
import rooms from './rooms/actions';

import users from './users/actions';

export default {
  // actions here
  locations,
  reservations,
  transactions,
  checkins,
  bookings,
  customers,
  transactionTypes,
  packages,
  packageTypes,
  rooms,
  users,
};
