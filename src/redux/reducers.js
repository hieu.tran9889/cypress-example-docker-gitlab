import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { auth } from './auth/reducer';
import modal from './modal/reducer';
// import here
import locations from './locations/reducer';
import reports from './reports/reducer';
import reservations from './reservations/reducer';
import transactions from './transactions/reducer';
import checkins from './checkins/reducer';
import bookings from './bookings/reducer';
import realtimeBookings from './realtimeBookings/reducer';
import customers from './customers/reducer';
import transactionTypes from './transactionTypes/reducer';
import packages from './packages/reducer';
import packageTypes from './packageTypes/reducer';
import rooms from './rooms/reducer';

import users from './users/reducer';
import reference from './referenceData/reducer';
import emails from './emails/reducer';
import config from './config/reducer';

export default history =>
  combineReducers({
    router: connectRouter(history),
    auth,
    modal,
    config,
    // add reducer here
    reports,
    realtimeBookings,
    locations,
    reservations,
    transactions,
    checkins,
    bookings,
    customers,
    transactionTypes,
    packages,
    packageTypes,
    rooms,

    users,
    reference,
    emails,
  });
