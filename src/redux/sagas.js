import { all } from 'redux-saga/effects';
import authSaga from './auth/sagas';
import usersSagas from './users/sagas';
import referenceSagas from './referenceData/sagas';
import emailSagas from './emails/sagas';
import configSagas from './config/sagas';
import packageTypesSagas from './packageTypes/sagas';
import roomsSagas from './rooms/sagas';
import packagesSagas from './packages/sagas';
import transactionTypesSagas from './transactionTypes/sagas';
import customersSagas from './customers/sagas';
import bookingsSagas from './bookings/sagas';
import realtimeBookingsSagas from './realtimeBookings/sagas';
import checkinsSagas from './checkins/sagas';
import transactionsSagas from './transactions/sagas';
import reservationsSagas from './reservations/sagas';
import locationsSagas from './locations/sagas';
import reportsSagas from './reports/sagas';
import modalSagas from './modal/sagas';

export default function* root() {
  yield all([
    ...modalSagas,
    ...reportsSagas,
    ...realtimeBookingsSagas,
    ...locationsSagas,
    ...reservationsSagas,
    ...transactionsSagas,
    ...checkinsSagas,
    ...bookingsSagas,
    ...customersSagas,
    ...transactionTypesSagas,
    ...packagesSagas,
    ...packageTypesSagas,
    ...roomsSagas,
    ...configSagas,
    ...usersSagas,
    ...authSaga,
    ...referenceSagas,
    ...emailSagas,
  ]);
}
