import { takeEvery, put, call } from 'redux-saga/effects';
import { apiWrapper } from '../../utils/reduxUtils';
import {
  getCustomersReportsSuccess,
  getCustomersReportsFailure,
  CustomerTypes,
  getPaymentReportsSuccess,
  getPaymentReportsFailure,
  getCustomersSummariesSuccess,
  getCustomersSummariesFailure,
} from './actions';
import { getCustomerReports, getPaymentReports, getCustomerSummaries } from '../../api/reports';

function* getCustomersReportsSaga() {
  try {
    const response = yield call(apiWrapper, { isShowProgress: false }, getCustomerReports);
    if (response) {
      yield put(getCustomersReportsSuccess(response));
    } else {
      yield put(getCustomersReportsFailure(response));
    }
  } catch (error) {
    yield put(getCustomersReportsFailure(error));
  }
}

function* getPaymentReportsSaga({ data }) {
  try {
    const response = yield call(apiWrapper, { isShowProgress: false }, getPaymentReports, data);
    if (response) {
      yield put(getPaymentReportsSuccess(response));
    } else {
      yield put(getPaymentReportsFailure(response));
    }
  } catch (error) {
    yield put(getPaymentReportsFailure(error));
  }
}

function* getCustomerSammariesSaga({ id }) {
  try {
    const response = yield call(apiWrapper, { isShowProgress: false }, getCustomerSummaries, id);
    if (response) {
      yield put(getCustomersSummariesSuccess(response));
    } else {
      yield put(getCustomersSummariesFailure(response));
    }
  } catch (error) {
    yield put(getCustomersSummariesFailure(error));
  }
}

export default [
  takeEvery(CustomerTypes.GET_CUSTOMERS_REPORTS, getCustomersReportsSaga),
  takeEvery(CustomerTypes.GET_PAYMENTS_REPORTS, getPaymentReportsSaga),
  takeEvery(CustomerTypes.GET_CUSTOMERS_SUMMARIES, getCustomerSammariesSaga),
];
