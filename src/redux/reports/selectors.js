import { createSelector } from 'reselect';

const getTransactionFilter = state => state.transactions.filter;

export const getFilterStartTime = createSelector(
  [getTransactionFilter],
  filter => {
    const { createdAt } = filter;
    return createdAt && createdAt.$gte;
  }
);

export const getFilterEndTime = createSelector(
  [getTransactionFilter],
  filter => {
    const { createdAt } = filter;
    return createdAt && createdAt.$lte;
  }
);
