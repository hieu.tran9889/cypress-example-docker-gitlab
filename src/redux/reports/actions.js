import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const CustomerTypes = makeConstantCreator(
  'GET_CUSTOMERS_REPORTS',
  'GET_CUSTOMERS_REPORTS_SUCCESS',
  'GET_CUSTOMERS_REPORTS_FAILURE',
  'GET_PAYMENTS_REPORTS',
  'GET_PAYMENTS_REPORTS_SUCCESS',
  'GET_PAYMENTS_REPORTS_FAILURE',
  'GET_CUSTOMERS_SUMMARIES',
  'GET_CUSTOMERS_SUMMARIES_SUCCESS',
  'GET_CUSTOMERS_SUMMARIES_FAILURE',
);
export const getCustomersReports = () => makeActionCreator(CustomerTypes.GET_CUSTOMERS_REPORTS);
export const getCustomersReportsSuccess = data =>
  makeActionCreator(CustomerTypes.GET_CUSTOMERS_REPORTS_SUCCESS, { data });
export const getCustomersReportsFailure = error =>
  makeActionCreator(CustomerTypes.GET_CUSTOMERS_REPORTS_FAILURE, { error });

export const getPaymentReports = data =>
  makeActionCreator(CustomerTypes.GET_PAYMENTS_REPORTS, { data });
export const getPaymentReportsSuccess = data =>
  makeActionCreator(CustomerTypes.GET_PAYMENTS_REPORTS_SUCCESS, { data });
export const getPaymentReportsFailure = error =>
  makeActionCreator(CustomerTypes.GET_PAYMENTS_REPORTS_FAILURE, { error });

export const getCustomersSummaries = id => makeActionCreator(CustomerTypes.GET_CUSTOMERS_SUMMARIES, { id });
export const getCustomersSummariesSuccess = data => makeActionCreator(CustomerTypes.GET_CUSTOMERS_SUMMARIES_SUCCESS, { data });
export const getCustomersSummariesFailure = error => makeActionCreator(CustomerTypes.GET_CUSTOMERS_SUMMARIES_FAILURE, { error });
