import { CustomerTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const initialState = {
  customerReports: {},
  paymentReports: {},
  customerSummaries: {},
  error: null,
};

const getCustomersReportsFailure = (state, action) => ({
  ...state,
  error: action.error,
});

const getCustomersReportsSuccess = (state, { data }) => ({
  ...state,
  customerReports: data,
});

const getPaymentReportsFailure = (state, action) => ({
  ...state,
  error: action.error,
});

const getPaymentReportsSuccess = (state, { data }) => ({
  ...state,
  paymentReports: data,
});

const getCustomersSummariesSuccess = (state, { data }) => ({
  ...state,
  customerSummaries: data,
})
const getCustomersSummariesFailure = (state, action) => ({
  ...state,
  error: action.error,
})

export default makeReducerCreator(initialState, {
  [CustomerTypes.GET_CUSTOMERS_REPORTS_FAILURE]: getCustomersReportsFailure,
  [CustomerTypes.GET_CUSTOMERS_REPORTS_SUCCESS]: getCustomersReportsSuccess,
  [CustomerTypes.GET_PAYMENTS_REPORTS_FAILURE]: getPaymentReportsFailure,
  [CustomerTypes.GET_PAYMENTS_REPORTS_SUCCESS]: getPaymentReportsSuccess,
  [CustomerTypes.GET_CUSTOMERS_SUMMARIES_SUCCESS]: getCustomersSummariesSuccess,
  [CustomerTypes.GET_CUSTOMERS_SUMMARIES_FAILURE]: getCustomersSummariesFailure,
});
