import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { t } from 'i18next';
import { withNamespaces } from 'react-i18next';
import { flatMap, map } from 'lodash';
import PrivateLayout from '../../layout/PrivateLayout';
import Locations from '../../pages/Locations';
import Reservations from '../../pages/Reservations';
import Transactions from '../../pages/Transactions';
import Checkins from '../../pages/Checkins';
import Bookings from '../../pages/Bookings';
import Customers from '../../pages/Customers';
import TransactionTypes from '../../pages/TransactionTypes';
import Packages from '../../pages/Packages';
import PackageTypes from '../../pages/PackageTypes';
import Rooms from '../../pages/Rooms';

import Users from '../../pages/Users';
import Home from '../../pages/Dashboard';
import Settings from '../../pages/Settings';
import { initApp as initAppAction } from '../../redux/config/actions';
import TransactionList from '../../pages/Transactions/List';
import EditCustomers from '../../pages/Customers/Edit';

const routes = [
  {
    path: '/locations',
    routes: [
      {
        path: '/',
        component: Locations.List,
      },
      {
        path: '/create',
        component: Locations.Create,
      },
      {
        path: '/:id/edit',
        component: Locations.Edit,
      },
    ],
  },
  {
    path: '/reservations',
    routes: [
      {
        path: '/',
        component: Reservations,
      },
      {
        path: '/create',
        component: Reservations.Create,
      },
      {
        path: '/:id/edit',
        component: Reservations.Edit,
      },
    ],
  },

  {
    path: '/transactions',
    routes: [
      {
        path: '/',
        component: TransactionList,
      },
      {
        path: '/create',
        component: Transactions.Create,
      },
      {
        path: '/:id/edit',
        component: Transactions.Edit,
      },
    ],
  },

  {
    path: '/checkins',
    routes: [
      {
        path: '/',
        component: Checkins.List,
      },
      {
        path: '/create',
        component: Checkins.Create,
      },
      {
        path: '/:id/edit',
        component: Checkins.Edit,
      },
    ],
  },

  {
    path: '/bookings',
    routes: [
      {
        path: '/',
        component: Bookings.List,
      },
      {
        path: '/create',
        component: Bookings.Create,
      },
      {
        path: '/:id/edit',
        component: Bookings.Edit,
      },
    ],
  },

  {
    path: '/customers',
    routes: [
      {
        path: '/',
        component: Customers.List,
      },
      {
        path: '/create',
        component: Customers.Create,
      },
      {
        path: '/:id/edit',
        component: EditCustomers,
      },
    ],
  },

  {
    path: '/transactionTypes',
    routes: [
      {
        path: '/',
        component: TransactionTypes.List,
      },
      {
        path: '/create',
        component: TransactionTypes.Create,
      },
      {
        path: '/:id/edit',
        component: TransactionTypes.Edit,
      },
    ],
  },

  {
    path: '/packages',
    routes: [
      {
        path: '/',
        component: Packages.List,
      },
      {
        path: '/create',
        component: Packages.Create,
      },
      {
        path: '/:id/edit',
        component: Packages.Edit,
      },
    ],
  },

  {
    path: '/packageTypes',
    routes: [
      {
        path: '/',
        component: PackageTypes.List,
      },
      {
        path: '/create',
        component: PackageTypes.Create,
      },
      {
        path: '/:id/edit',
        component: PackageTypes.Edit,
      },
    ],
  },

  {
    path: '/rooms',
    routes: [
      {
        path: '/',
        component: Rooms.List,
      },
      {
        path: '/create',
        component: Rooms.Create,
      },
      {
        path: '/:id/edit',
        component: Rooms.Edit,
      },
    ],
  },

  {
    path: '/packageTypes',
    routes: [
      {
        path: '/',
        component: PackageTypes.List,
      },
      {
        path: '/create',
        component: PackageTypes.Create,
      },
      {
        path: '/:id/edit',
        component: PackageTypes.Edit,
      },
    ],
  },
  {
    path: '/users',
    routes: [
      {
        path: '/',
        component: Users.List,
      },
      {
        path: '/create',
        component: Users.Create,
      },
      {
        path: '/:id/edit',
        component: Users.Edit,
      },
    ],
  },
  {
    path: '/home',
    component: Home,
    exact: true,
    title: t('dashboard.title'),
  },
  {
    path: '/',
    component: Home,
    exact: true,
    title: t('dashboard.title'),
  },
  {
    path: '/settings/:model',
    component: Settings,
    exact: true,
    title: t('settings.title'),
  },
];

const PrivateRoutes = ({ initApp }) => {
  useEffect(() => {
    initApp();
  }, []);
  return (
    <PrivateLayout>
      <Switch>
        {map(
          flatMap(routes, route => {
            if (route.routes) {
              return map(route.routes, subRoute => ({
                ...subRoute,
                path: route.path + subRoute.path,
                exact: subRoute.path === '/',
              }));
            }
            return route;
          }),
          route => (
            <Route {...route} key={route.path} />
          )
        )}
        <Redirect to="/" />
      </Switch>
    </PrivateLayout>
  );
};

PrivateRoutes.propTypes = {
  initApp: PropTypes.func,
};

export default connect(
  state => ({
    isAuthenticated: state.auth.isAuthenticated,
  }),
  dispatch => ({
    initApp: () => dispatch(initAppAction()),
  })
)(withNamespaces()(PrivateRoutes));
