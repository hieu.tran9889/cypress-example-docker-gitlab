import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { replace } from 'connected-react-router';
import i18next from 'i18next';
import Modal from '../../components/common/Modal';
import Locations from '../../pages/Locations';
import Reservations from '../../pages/Reservations';
import Transactions from '../../pages/Transactions';
import Checkins from '../../pages/Checkins';
import Bookings from '../../pages/Bookings';
import Customers from '../../pages/Customers';
import TransactionTypes from '../../pages/TransactionTypes';
import Packages from '../../pages/Packages';
import PackageTypes from '../../pages/PackageTypes';
import Rooms from '../../pages/Rooms';

import Users from '../../pages/Users';
import CheckinModal from '../../containers/Bookings/CheckinModal';
import CheckoutModal from '../../containers/Bookings/CheckoutModal';
import CreateForIncomePayments from '../../containers/Transactions/Create/CreateForIncome';
import CreateForExpensePayments from '../../containers/Transactions/Create/CreateForExpense';
import ReservationsCreate from '../../containers/Reservations/Create/index';
import BookingsCreate from '../../containers/Bookings/Create';

const modalRoutes = [
  {
    path: '/locations',
    routes: [
      {
        path: '/create',
        component: Locations.Create,
      },
      {
        path: '/edit',
        component: Locations.Edit,
      },
    ],
  },
  {
    path: '/reservations',
    routes: [
      {
        path: '/create',
        component: ReservationsCreate,
      },
      {
        path: '/edit',
        component: Reservations.Edit,
      },
    ],
  },
  {
    path: '/transactions',
    routes: [
      {
        path: '/edit',
        component: Transactions.Edit,
      },
      {
        path: '/createForIncome',
        component: CreateForIncomePayments,
      },
      {
        path: '/createForExpense',
        component: CreateForExpensePayments,
      },
    ],
  },
  {
    path: '/checkins',
    routes: [
      {
        path: '/create',
        component: Checkins.Create,
      },
      {
        path: '/edit',
        component: Checkins.Edit,
      },
    ],
  },

  {
    path: '/bookings',
    routes: [
      {
        path: '/create',
        component: BookingsCreate,
      },
      {
        path: '/edit',
        component: Bookings.Edit,
        modalOptions: {
          width: 755,
        },
      },
      {
        path: '/checkin',
        component: CheckinModal,
        modalOptions: {
          title: i18next.t('button.checkin'),
        },
      },
      {
        path: '/checkout',
        component: CheckoutModal,
      },
    ],
  },

  {
    path: '/customers',
    routes: [
      {
        path: '/create',
        component: Customers.Create,
        modalOptions: {
          width: 755,
        },
      },
      {
        path: '/edit',
        component: Customers.Edit,
      },
    ],
  },

  {
    path: '/transactionTypes',
    routes: [
      {
        path: '/create',
        component: TransactionTypes.Create,
      },
      {
        path: '/edit',
        component: TransactionTypes.Edit,
      },
    ],
  },

  {
    path: '/packages',
    routes: [
      {
        path: '/create',
        component: Packages.Create,
      },
      {
        path: '/edit',
        component: Packages.Edit,
      },
    ],
  },

  {
    path: '/packageTypes',
    routes: [
      {
        path: '/create',
        component: PackageTypes.Create,
      },
      {
        path: '/edit',
        component: PackageTypes.Edit,
      },
    ],
  },

  {
    path: '/rooms',
    routes: [
      {
        path: '/create',
        component: Rooms.Create,
        modalOptions: {
          width: 755,
        },
      },
      {
        path: '/edit',
        component: Rooms.Edit,
        modalOptions: {
          width: 755,
        },
      },
    ],
  },

  {
    path: '/packageTypes',
    routes: [
      {
        path: '/create',
        component: PackageTypes.Create,
      },
      {
        path: '/edit',
        component: PackageTypes.Edit,
      },
    ],
  },
  {
    path: '/users',
    routes: [
      {
        path: '/create',
        component: Users.Create,
        modalOptions: {
          width: 755,
        },
      },
      {
        path: '/edit',
        component: Users.Edit,
      },
    ],
  },
];

const getModalRoute = currentModal => {
  const modalRoute =
    currentModal && modalRoutes.find(route => currentModal.search(route.path) > -1);
  if (modalRoute) {
    return modalRoute.routes.find(route => currentModal.indexOf(route.path) > -1);
  }
  return modalRoute;
};

class ModalRoute extends Component {
  componentDidMount() {
    const { location } = this.props;
    if (location.hash && location.hash !== '#') {
      const modelRoute = location.hash.replace('#', '/');
      this.modal = getModalRoute(modelRoute);
    }
  }

  closeModal = () => {
    const { replaceRoute, location } = this.props;
    replaceRoute(location.pathname);
  };

  render() {
    const { location } = this.props;
    const modelRoute = location.hash.replace('#', '/');
    this.modal = getModalRoute(modelRoute) || this.modal;
    const modalOptions = this.modal && this.modal.modalOptions ? this.modal.modalOptions : {};
    return (
      <Modal
        {...modalOptions}
        visible={!!(location.hash && location.hash !== '#')}
        footer={null}
        onCancel={this.closeModal}
      >
        {this.modal && this.modal.component && (
          <this.modal.component
            showModal
            visibleModal={location.hash && location.hash !== '#'}
            location={location}
          />
        )}
      </Modal>
    );
  }
}

ModalRoute.propTypes = {
  location: PropTypes.object,
  currentModal: PropTypes.string,
  closeModal: PropTypes.func,
  showModal: PropTypes.func,
  replaceRoute: PropTypes.func,
};

const mapStateToProps = state => ({
  location: state.router.location,
});

const mapDispatchToProps = dispatch => ({
  replaceRoute: data => dispatch(replace(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalRoute);
