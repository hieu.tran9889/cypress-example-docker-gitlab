> Cypress + Docker + GitLabCI = ❤️

[![pipeline status](https://gitlab.com/cypress-io/cypress-example-docker-gitlab/badges/master/pipeline.svg)](https://gitlab.com/cypress-io/cypress-example-docker-gitlab/commits/master)

Running your Cypress E2E tests on GitLab CI is very simple. You can either
start with a base image or with an image that already includes Cypress tool.

You can also find GitLab CI (and other CIs) example in [cypress-example-kitchensink](https://github.com/cypress-io/cypress-example-kitchensink#ci-status).

## Base image

You can derive your custom CI image from
[cypress/base](https://hub.docker.com/r/cypress/base/) and install
`cypress`. Here is a typical `.gitlab-ci.yml` file

```yaml
image: cypress/base:10
cypress-e2e:
  script:
    - npm install
    - $(npm bin)/cypress run
```

https://dashboard.cypress.io/ xem key để chay $(npm bin)/cypress run --record --key 7b525175-b127-4285-9480-1b4a1c044af1 --parallel
7b525175-b127-4285-9480-1b4a1c044af1: hieu.tran@enouvo.com
fb4abc97-702e-4c6c-898e-bcc1a5cdcebc: hieutrandn123@gmail.com
